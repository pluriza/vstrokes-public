import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import configureStore from './store';
import initialState from './store/initialState';
import Routes from './routes';
import './index.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {loadState, saveState} from './libs/localStorage';
import './libs/polyfills';

// Google Analytics
export const GA_TRACKING_ID = 'UA-48949610-1';
window.gtag('config', GA_TRACKING_ID);

const localState = loadState();
const state = initialState(localState);
const store = configureStore(state);
store.subscribe(() => saveState(store.getState(), initialState));

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#1abc9c',
    primary2Color: '#1abc9c',
    pickerHeaderColor: '#1abc9c',
    accent1Color:'#1abc9c'
  },
  appBar: {
    height: 50,
  },
});

ReactDOM.render(
  <MuiThemeProvider muiTheme={muiTheme}>
    <Provider store={store}>
      <Routes/>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root')
);
