// This helps when updating specific states like creating new properties
export default function (localState = {}) {
  const { authentication, growler } = localState;
  // If the user is not logged in, show terms modal on page reload
  let isTermsModalOpen = true;
  if (authentication) {
    isTermsModalOpen = !authentication.isLogged;
  }
  if (growler) {
    isTermsModalOpen = growler.status !== 'hidden'
  }
  return {
    entities: {
      loading: false,
      users: {},
      posts: {},
      ...localState.entities,
    },
    authentication: {
      userId: "",
      sessionId: "",
      isLogged: false,
      loggingFail: false,
      loginErrorMessage: '',
      ...authentication,
    },
    albums: {
      albums: [],
      created: false,
      albumActive: {},
      picturesAll: [],
      ...localState.albums,
    },
    user: {
      userId: '',
      profile: {},
      username: '',
      profileImage: '',
      coverImage:'',
      description: '',
      albums: [],
      ...localState.user,
    },
    room: {
      loading: false,
      active: false,
      ...localState.room,
    },
    post: {
      postIds: {},
      postsUser: [],
      ...localState.post,
    },
    modals: {
      isLoginModalOpen: false,
      isResetPasswordModalOpen: false,
      isUploadPictureModalOpen: false,
      isGetStrokesModalOpen: false,
      isStrokeMeModalOpen: false,
      isDescriptionMeModalOpen: false,
      isAlbumsModalOpen: false,
      isPayInfoToken: false,
      isDetailAlbumModalOpen: false,
      isUploadPicturePostModalOpen: false,
      ...localState.modals,
      isTermsModalOpen,
    },
    growler: {
      text: "",
      icon: "",
      growlerType: "",
      status: "hidden",
      ...growler,
    },
    stream: {
      isStreamOnline: false,
      ...localState.stream,
    },
    // TODO This should not be here
    token: {
      total: 0,
      count: 0,
      withdrawable: 0,
      purchases: [],
      ...localState.token,
      buyInfo: {
        total: 0,
        count: 0,
        loading: false,
        digest: '',
        token: '',
        ...(localState.token ? localState.token.buyInfo : {}),
      },
      withdrawalMessage: null,
    },
    chat: {
      id: '',
      content: [],
      ...localState.chat,
    },
    rooms: {
      loading: false,
      data: [],
      total: 0,
      limit: 1,
      ...localState.rooms,
      filters: {
        ...(localState.rooms ? localState.rooms.filters : { page: 1 })
      },
    },
    tags: {
      trending: [],
      all: [],
      ...localState.tags,
    },
    snaps: {
      loading: false,
      rows: [],
      total: 0,
      limit: 1,
      ...localState.snaps,
      filters: {
        ...(localState.snaps ? localState.snaps.filters : { page: 1 }),
      }
    }
  }
}
