import 'image-capture/lib/imagecapture';

const {
  navigator,
  RTCPeerConnection, mozRTCPeerConnection, webkitRTCPeerConnection,
  RTCIceCandidate, mozRTCIceCandidate, webkitRTCIceCandidate,
  RTCSessionDescription, mozRTCSessionDescription, webkitRTCSessionDescription
} = window;

window.RTCPeerConnection = RTCPeerConnection || mozRTCPeerConnection || webkitRTCPeerConnection;
window.RTCIceCandidate = RTCIceCandidate || mozRTCIceCandidate || webkitRTCIceCandidate;
window.RTCSessionDescription = RTCSessionDescription || mozRTCSessionDescription || webkitRTCSessionDescription;


if (navigator.mediaDevices === undefined) {
  navigator.mediaDevices = {};
}
const { mediaDevices, getUserMedia: navGetUserMedia, webkitGetUserMedia, mozGetUserMedia } = navigator;

if (mediaDevices.getUserMedia === undefined) {
  mediaDevices.getUserMedia = constraints => {
    const getUserMedia = navGetUserMedia || webkitGetUserMedia || mozGetUserMedia;

    if (!getUserMedia) {
      return Promise.reject(new Error('getUserMedia is not implemented in this browser'))
    }

    return new Promise((resolve, reject) => getUserMedia.call(navigator, constraints, resolve, reject));
  }
}