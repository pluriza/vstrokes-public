const {
  navigator: {mediaDevices},
  URL: {createObjectURL},
  WebSocket,
  RTCPeerConnection,
  RTCSessionDescription,
  RTCIceCandidate,
  ImageCapture,
} = window;

const SDP_URL = 'wss://stream.vinegarstrokes.com/webrtc-session.json';

class WebRTC {

  constraints = {
    audio: true,
    video: true,
  };
  stream = null;
  capture = null;
  streamURL = "";
  connection = null;
  peerConnection = null;

  streamInfo = {
    applicationName: "webrtc",
    streamName: null,
    sessionId: "[empty]"
  };

  listener = null;
  endStreamListener = null;

  constructor(streamName, constraints) {
    this.streamInfo.streamName = streamName;
    if (constraints) {
      this.constraints = constraints;
    }
  }

  setListener = callback => {
    this.listener = callback;
  };

  setEndStreamListener = callback => {
    this.endStreamListener = callback;
  };

  initForStreaming = async () => {
    // Capture devices
    this.stream = await mediaDevices.getUserMedia(this.constraints);
    const videoTracks = this.stream.getVideoTracks();
    if (videoTracks.length > 0) {
      this.capture = new ImageCapture(this.stream.getVideoTracks()[0]);
      // Setup connection
      this.connection = new WebSocket(SDP_URL);
      this.connection.binaryType = 'arraybuffer';
      this.connection.addEventListener('open', this.handleOpenStreamConnection);
      this.connection.addEventListener('message', this.handleMessageStreamConnection);
      this.connection.addEventListener('close', this.handleCloseConnection);
      this.connection.addEventListener('error', this.handleErrorStreamConnection);

      this.streamURL = createObjectURL(this.stream);
      if (this.listener) {
        this.listener(this.streamURL);
      }
    } else {
      if (this.endStreamListener) {
        this.endStreamListener();
      }
    }
  };

  closeStream = () => {
    console.log('WebRTC.closeStream', this.stream);
    if (this.stream) {
      const tracks = this.stream.getTracks();
      tracks.forEach(track => track.stop());
    }
    this.stop();
  };

  handleOpenStreamConnection = event => {
    console.log('WebRTC.handleOpenStreamConnection', JSON.stringify(event));
    this.peerConnection = new RTCPeerConnection({iceServers: []});
    this.peerConnection.addEventListener('icecandidate', this.handleIceCandidatePeerConnection);
    if (!this.peerConnection.addTrack) {
      this.peerConnection.addStream(this.stream);
    } else {
      this.stream.getTracks().forEach(track => this.peerConnection.addTrack(track, this.stream));
    }
    this.peerConnection.createOffer(this.gotDescription, error => console.log('WebRTC.handleOpenStreamConnection createOffer', error));
  };

  handleIceCandidatePeerConnection = event => {
    console.log('WebRTC.handleIceCandidatePeerConnection:', JSON.stringify(event));
    if (event.candidate !== null) {
      console.log('WebRTC IceCandidate:', JSON.stringify(event.candidate));
    }
  };

  gotDescription = description => {
    // Make configurable
    const enhanceData = {
      audioBitrate: 64,
      videoBitrate: 180,
      videoFrameRate: 23.97,
    };
    description.sdp = this.enhanceSDP(description.sdp, enhanceData);
    console.log('gotDescription: ' + JSON.stringify({sdp: description}));

    this.peerConnection.setLocalDescription(description, () => {
      const data = {
        direction: "publish",
        command:"sendOffer",
        streamInfo: this.streamInfo,
        sdp: description,
        userData: { param1: "value1" },
      };
      this.connection.send(JSON.stringify(data));
    }, () => {
      console.log('set description error')
    });
  };

  enhanceSDP(sdpStr, enhanceData) {
    const sdpLines = sdpStr.split(/\r\n/);
    let sdpSection = 'header';
    let hitMID = false;
    let sdpStrRet = '';

    console.log('WebRTC.enhanceSDP', sdpLines);

    sdpLines.forEach(sdpLine => {
      if (sdpLine.length <= 0){
        return;
      }

      sdpStrRet += sdpLine;

      if (sdpLine.indexOf("m=audio") === 0) {
        sdpSection = 'audio';
        hitMID = false;
      } else if (sdpLine.indexOf("m=video") === 0) {
        sdpSection = 'video';
        hitMID = false;
      } else if (sdpLine.indexOf("a=rtpmap") === 0) {
        sdpSection = 'bandwidth';
        hitMID = false;
      }

      if (sdpLine.indexOf("a=mid:") === 0 || sdpLine.indexOf("a=rtpmap") === 0) {
        if (!hitMID) {
          if ('audio'.localeCompare(sdpSection) === 0) {
            if (enhanceData.audioBitrate !== undefined) {
              sdpStrRet += '\r\nb=CT:' + (enhanceData.audioBitrate);
              sdpStrRet += '\r\nb=AS:' + (enhanceData.audioBitrate);
            }
            hitMID = true;
          } else if ('video'.localeCompare(sdpSection) === 0) {
            if (enhanceData.videoBitrate !== undefined) {
              sdpStrRet += '\r\nb=CT:' + (enhanceData.videoBitrate);
              sdpStrRet += '\r\nb=AS:' + (enhanceData.videoBitrate);
              if (enhanceData.videoFrameRate !== undefined) {
                sdpStrRet += '\r\na=framerate:' + enhanceData.videoFrameRate;
              }
            }
            hitMID = true;
          } else if ('bandwidth'.localeCompare(sdpSection) === 0) {
            let rtpmapID;
            rtpmapID = WebRTC.getRTPMapID(sdpLine);
            if (rtpmapID !== null) {
              const match = rtpmapID[2].toLowerCase();
              if (('vp9'.localeCompare(match) === 0) || ('vp8'.localeCompare(match) === 0) || ('h264'.localeCompare(match) === 0) || ('red'.localeCompare(match) === 0) || ('ulpfec'.localeCompare(match) === 0) || ('rtx'.localeCompare(match) === 0)) {
                if (enhanceData.videoBitrate !== undefined) {
                  sdpStrRet += '\r\na=fmtp:' + rtpmapID[1] + ' x-google-min-bitrate=' + (enhanceData.videoBitrate) + ';x-google-max-bitrate=' + (enhanceData.videoBitrate);
                }
              }

              if (('opus'.localeCompare(match) === 0) || ('isac'.localeCompare(match) === 0) || ('g722'.localeCompare(match) === 0) || ('pcmu'.localeCompare(match) === 0) || ('pcma'.localeCompare(match) === 0) || ('cn'.localeCompare(match) === 0)) {
                if (enhanceData.audioBitrate !== undefined) {
                  sdpStrRet += '\r\na=fmtp:' + rtpmapID[1] + ' x-google-min-bitrate=' + (enhanceData.audioBitrate) + ';x-google-max-bitrate=' + (enhanceData.audioBitrate);
                }
              }
            }
          }
        }
      }
      sdpStrRet += '\r\n';
    });
    return sdpStrRet;
  }

  static getRTPMapID(line) {
    const findId = new RegExp('a=rtpmap:(\\d+) (\\w+)/(\\d+)');
    const found = line.match(findId);
    return (found && found.length >= 3) ? found : null;
  };

  handleMessageStreamConnection = event => {
    const msgJSON = JSON.parse(event.data);
    console.log('WebRTC.handleMessageStreamConnection', msgJSON);

    const msgStatus = Number(msgJSON['status']);
    console.log('WebRTC.handleMessageStreamConnection', msgJSON['command']);

    if (msgStatus !== 200) {
      console.log('WebRTC.handleMessageStreamConnection', msgJSON['statusDescription']);
      this.stop();
    } else {
      let sdpData = msgJSON['sdp'];
      if (sdpData !== undefined) {
        console.log('sdp: ', msgJSON['sdp']);

        this.peerConnection.setRemoteDescription(new RTCSessionDescription(sdpData), function () {
          //peerConnection.createAnswer(gotDescription, errorHandler);
        }, error => console.log('WebRTC.handleMessageStreamConnection setRemoteDescription', error));
      }

      const iceCandidates = msgJSON['iceCandidates'];
      console.log('iceCandidates: ', iceCandidates);
      if (iceCandidates !== undefined) {
        iceCandidates.forEach((iceCandidate, index) => {
          console.log('iceCandidates: ', index, iceCandidate);

          this.peerConnection.addIceCandidate(new RTCIceCandidate(iceCandidates[index]));
        });
      }
    }

    if (this.connection !== null) {
      this.connection.close();
    }
    this.connection = null;
  };

  handleCloseConnection = event => {
    console.log('WebRTC.handleCloseConnection', JSON.stringify(event));
  };

  handleErrorStreamConnection = event => {
    console.log('WebRTC.handleErrorConnection', JSON.stringify(event));
    this.stop();
  };

  stop = () => {
    if (this.peerConnection !== null) {
      this.peerConnection.close();
    }
    this.peerConnection = null;

    if (this.connection !== null) {
      this.connection.close();
    }
    this.connection = null;

    this.listener = null;
  };

  takeSnapshow = async () => {
    if (this.capture) {
      const bitmap = await this.capture.grabFrame();
      const canvas = document.createElement('canvas');
      canvas.width = bitmap.width;
      canvas.height = bitmap.height;
      canvas.getContext('2d').drawImage(bitmap, 0, 0);
      return await (new Promise(resolve => canvas.toBlob(resolve)));
    } else {
      return null;
    }
  };

  initForPlayback = () => {
    this.connection = new WebSocket(SDP_URL);
    this.connection.binaryType = 'arraybuffer';
    this.connection.addEventListener('open', this.handleOpenPlaybackConnection);
    this.connection.addEventListener('message', this.handleMessagePlaybackConnection);
    this.connection.addEventListener('close', this.handleCloseConnection);
    this.connection.addEventListener('error', this.handleErrorPlayConnection);
  };

  handleOpenPlaybackConnection = event => {
    console.log('WebRTC.handleOpenPlaybackConnection', event, JSON.stringify(event));
    this.peerConnection = new RTCPeerConnection({iceServers: []});
    this.peerConnection.addEventListener('icecandidate', this.handleIceCandidatePeerConnection);
    this.peerConnection.addEventListener('iceconnectionstatechange', () => {
      if (this.peerConnection) {
        console.log('handleOpenPlaybackConnection:iceconnectionstatechange', this.peerConnection.iceConnectionState);
      } else {
        console.log('handleOpenPlaybackConnection:iceconnectionstatechange', this.peerConnection);
      }
    });
    // this.peerConnection.ontrack = event => this.handleTrackPeerConnection(event);
    this.peerConnection.addEventListener('addstream', this.handleAddStreamPeerConnection);

    console.log('WebRTC.handleOpenPlaybackConnection', this.streamInfo);
    const data = {
      direction: 'play',
      command: 'getOffer',
      streamInfo: this.streamInfo,
      userData: { param1: "value1" },
    };
    this.connection.send(JSON.stringify(data));
  };

  handleTrackPeerConnection = event => {
    console.log('WebRTC.handleTrackPeerConnection', JSON.stringify(event));
    console.log('WebRTC.handleTrackPeerConnection kind', event.track.kind, 'stream', event.streams);
    this.streamURL = createObjectURL(event.streams[0]);
    if (this.listener) {
      this.listener(this.streamURL);
    }
  };

  handleAddStreamPeerConnection = event => {
    console.log('WebRTC.handleAddStreamPeerConnection', JSON.stringify(event));
    console.log('WebRTC.handleAddStreamPeerConnection stream', event.stream);
    this.streamURL = createObjectURL(event.stream);
    if (this.listener) {
      this.listener(this.streamURL);
    }
  };

  handleMessagePlaybackConnection = event => {
    const msgJSON = JSON.parse(event.data);
    console.log('WebRTC.handleMessagePlaybackConnection', msgJSON);

    const msgStatus = Number(msgJSON['status']);
    const msgCommand = msgJSON['command'];
    console.log('WebRTC.handleMessagePlaybackConnection command', msgCommand);

    if (msgStatus === 514) {
      console.log('WebRTC.handleMessagePlaybackConnection repeater stream not ready');
      // TODO sendGetOffer repeater retry
      this.stopPlay();
    } else if (msgStatus !== 200) {
      console.log('WebRTC.handleMessagePlaybackConnection status', msgJSON['statusDescription']);
      // TODO Reload
      const data = {
        direction: 'play',
        command: 'getOffer',
        streamInfo: this.streamInfo,
        userData: { param1: "value1" },
      };
      this.connection.send(JSON.stringify(data));
    } else {
      const streamInfoResponse = msgJSON['streamInfo'];

      if (streamInfoResponse) {
        this.streamInfo.sessionId = streamInfoResponse.sessionId;
      }

      const sdpData = msgJSON['sdp'];
      if (sdpData) {
        console.log('WebRTC.handleMessagePlaybackConnection sdp', sdpData);
        this.peerConnection.setRemoteDescription(new RTCSessionDescription(sdpData), () => {
          this.peerConnection.createAnswer(this.gotPlayDescription, error => console.log('WebRTC.handleMessagePlaybackConnection createAnswer', error));
        }, error => console.log('WebRTC.handleMessagePlaybackConnection setRemoteDescription', error));
      }

      const iceCandidates = msgJSON['iceCandidates'];
      console.log('iceCandidates: ', iceCandidates);
      if (iceCandidates) {
        iceCandidates.forEach((iceCandidate, index) => {
          console.log('iceCandidates: ', index, iceCandidate);

          this.peerConnection.addIceCandidate(new RTCIceCandidate(iceCandidates[index]));
        });
      }
    }

    if ('sendResponse'.localeCompare(msgCommand) === 0) {
      if (this.connection !== null) {
        this.connection.close();
      }
      this.connection = null;
    }
  };

  gotPlayDescription = description => {
    console.log('gotPlayDescription', description);
    this.peerConnection.setLocalDescription(description, () => {
      console.log('sendAnswer');

      const data = {
        direction: 'play',
        command: 'sendResponse',
        streamInfo: this.streamInfo,
        sdp: description,
        userData: { param1: "value1" },
      };
      this.connection.send(JSON.stringify(data));

    }, function() {
      console.log('set description error')
    });
  };

  handleErrorPlayConnection = event => {
    console.log('WebRTC.handleErrorPlayConnection', JSON.stringify(event));
  };

  stopPlay = () => {
    this.stop();

    this.streamURL = "";
  };

}

export default WebRTC;