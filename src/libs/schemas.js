import { schema } from "normalizr";

const usersSchema = new schema.Entity("users");
const postsSchema = [new schema.Entity("posts")];

export { usersSchema, postsSchema };
