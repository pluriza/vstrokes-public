import openSocket from 'socket.io-client';

const socket = openSocket(process.env.REACT_APP_SOCKET_URL);

function subscribeToChatProfile(cb) {
  socket.on('get message', data => cb(null, data));
}

function emitToChatProfile(content) {
  socket.emit('new message', content);
}

function subscribeToLiveChanges(cb) {
  socket.on('live changed', (room, roomId) => cb(null, room, roomId));
}

function emitLiveChange(room, roomId) {
  socket.emit('live change', room, roomId);
}

function subscribeToLiveEnding(cb) {
  socket.on('live ended', room => cb(null, room));
}

function emitLiveEnd(room) {
  socket.emit('live end', room);
}

function emitToRoomUsernameChatProfile(username) {
  socket.emit('room', username);
}

export {
  subscribeToChatProfile, emitToChatProfile,
  subscribeToLiveChanges, emitLiveChange,
  subscribeToLiveEnding, emitLiveEnd,
  emitToRoomUsernameChatProfile,
};