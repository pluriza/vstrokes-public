const loadState = () => {
  try {
    const serielizedState = localStorage.getItem('state');
    if (serielizedState === null) return undefined;
    return JSON.parse(serielizedState);
  } catch (error) {
    return undefined;
  }
};

const saveState = (state, initialState) => {
  try {
    const authState = {
      ...initialState,
      modals: {
        ...initialState.modals,
        isTermsModalOpen: state.modals.isTermsModalOpen,
      },
      entities: {
        ...state.entities,
      },
      authentication: {
        ...state.authentication,
      },
      token: {
        ...state.token,
      },
      chat: {
        ...state.chat,
      },
    };
    const serielizedState = JSON.stringify(authState);
    localStorage.setItem('state', serielizedState);
  } catch (error) {
    // Ignore for now
  }
};

export { loadState, saveState };
