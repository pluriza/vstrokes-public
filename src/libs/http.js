import axios from "axios";
import { mergeAll } from "ramda";

const generateHeaders = () => {
  return {
    Accept: "application/json",
    "Content-Type": "application/json;charset=UTF-8"
  };
};

const request = (method, url, optionalHeaders, axiosConfig = {}) => {
  const options = mergeAll([
    {
      method,
      url,
      baseURL: process.env.REACT_APP_SERVER_URL,
      headers: { ...generateHeaders(), ...optionalHeaders },
      responseType: "json"
    },
    axiosConfig
  ]);
  return axios(options);
};

export default {
  BASE_URL: process.env.REACT_APP_SERVER_URL,
  post(
    url,
    { data = {}, params = {}, optionalHeaders = {}, transformResponse = [] }
  ) {
    return request("post", url, optionalHeaders, {
      data,
      params,
      transformResponse
    });
  },
  put(
    url,
    { data = {}, params = {}, optionalHeaders = {}, transformResponse = [] }
  ) {
    return request("put", url, optionalHeaders, {
      data,
      params,
      transformResponse
    });
  },
  get(url, { params = {}, optionalHeaders = {}, transformResponse = [] }) {
    return request("get", url, optionalHeaders, { transformResponse, params });
  },
  delete(url, { optionalHeaders = {} }) {
    return request("delete", url, optionalHeaders, {});
  }
};
