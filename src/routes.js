import React from "react";
import {Router, Route, Switch} from "react-router-dom";
import history from "./libs/history";
import Header from "./containers/Header/HeaderContainer";
import App from "./components/App";
import Tags from './containers/Tags/TagsContainer';
import Snaps from './containers/Snaps/SnapsContainer';
import Streaming from "./containers/Streaming/StreamingContainer";
import SignUp from "./components/SignUp/SignUp";
import Login from "./containers/Login/LoginContainer";
import Profile from "./containers/Profile/ProfileContainer";
import Confirm from "./components/Confirm";
import InicialTerms from "./containers/TermsContainer";
import ResetPassword from "./containers/ResetPassword/ResetPasswordContainer";
import ResetPasswordNotification from "./components/ResetPassword/ResetPasswordNotification";
import SetNewPassword from "./containers/ResetPassword/SetNewPasswordContainer";
import Growler from "./containers/Growler/GrowlerContainer";
import EditProfile from "./containers/Profile/EditProfile/EditProfileContainer";
import GetTokens from './containers/Tokens/GetTokensContainer';
import PayInfoTokens from './containers/Tokens/PayInfoTokensContainer';
import Tokens from './containers/Tokens/TokensContainer';
import Terms from "./components/Terms/Terms";
//import Instruccion from "./components/Instruccion/Instruccion";
import AgeVerification from "./containers/AgeVerification/AgeVerificationContainer";
import GoogleAnalytics from './components/GoogleAnalytics';

const Routes = props => <Router {...props} history={history}>

  <div>
    <Route component={() => {window.scrollTo(0,0); return null;}}/>
    <Header/>
    <Switch>
      <Route exact path="/" component={App}/>
      <Route exact path="/tags" component={Tags}/>
      <Route exact path="/snaps" component={Snaps}/>
      <Route exact path="/signup" component={SignUp}/>
      <Route exact path="/profile" component={Profile}/>
      <Route exact path="/streaming/:username" component={Streaming}/>
      <Route exact path="/profile/edit" component={EditProfile}/>
      <Route exact path="/confirm" component={Confirm}/>
      <Route path="/verification" component={AgeVerification}/>
      {/* <Route exact path="/instruccion" component={Instruccion}/> */}
      <Route path="/legal" component={Terms}/>
      <Route path="/confirm_account/:confirmationToken" component={Profile}/>
      <Route exact path="/reset_password" component={ResetPasswordNotification}/>
      <Route path="/reset_password/:passwordToken" component={SetNewPassword}/>
      <Route path="/tokens" component={Tokens}/>
      <Route exact path="/:username" component={Profile}/>
    </Switch>
    <Login/>
    <InicialTerms/>
    <ResetPassword/>
    <Growler/>
    <GetTokens/>
    <PayInfoTokens/>
    <GoogleAnalytics />
  </div>

</Router>;

export default Routes;
