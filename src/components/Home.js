import React, {Component} from 'react';
import SideMenu from './SideMenu/SideMenu';
import Streams from './Streams/Streams';

class HomeBody extends Component {

  componentDidMount() {
    this.props.getTags();
    this.props.getLiveShows();
  }

  componentWillUnmount() {
    this.props.clearTimeout();
  }

  render() {
    const {
      streams, streamsCount, streamsLimit, streamsLoading,
      trendingTags, allTags, filters,
      setFilter, resetFilters
    } = this.props;
    return (
      <div className="body-container">
        {streamsLoading && <span className="streams-loading">Loading rooms...</span>}
        <SideMenu
          filters={filters}
          tags={allTags}
          updateFilter={setFilter}
          resetFilters={resetFilters}
        />
        <Streams
          streams={streams}
          streamsCount={streamsCount}
          streamsLimit={streamsLimit}
          currentPage={filters.page}
          tags={trendingTags}
          updateFilter={setFilter}
        />
      </div>
    );
  }
}

export default HomeBody;
