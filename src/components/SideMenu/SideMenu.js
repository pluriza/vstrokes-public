import React, { Component } from 'react';
import Filter from './Filter';
import filterList from './FiltersContents';

class SideMenu extends Component {

  render() {
    return (
      <div className="left-container">
        <Filter
          filters={this.props.filters}
          updateFilter={this.props.updateFilter}
          resetFilters={this.props.resetFilters}
          genderList= {filterList.genderList}
          orientationList={filterList.orientationList}
          bodyList={filterList.bodyList}
          hairList={filterList.hairList}
          ageList={filterList.ageList}
          ethnicityList={filterList.ethnicityList}
          regionList={filterList.regionList}
          tagList={this.props.tags}
          showTags={this.props.showTags}
        />
      </div>
    );
  }
}

SideMenu.defaultProps = {
  showTags: true,
  tags: [],
};

export default SideMenu;
