const filterList = {
  genderList: [
    'Male',
    'Female', 
    'Transgender',
    'Couple', 
  ],
  orientationList: [
    'Straight', 
    'Gay', 
    'Bisexual', 
    'Bicurious',
  ],
  bodyList: [
    'Slim/Petite', 
    'Athletic', 
    'Average', 
    'Large', 
  ],
  hairList: [
    'Black', 
    'Brown', 
    'Chestnut', 
    'Blonde',
    'Red',
    'Grey',
    'White',
    'Bald',
  ],
  ageList: [
    '18 - 20', 
    '21 - 25', 
    '26 - 30', 
    '31 - 35', 
    '36 - 40', 
    '41 - 45', 
    '46 - 50', 
    '+50',
  ],
  ethnicityList: [
   'Asian', 
   'Black',
   'Hispanic/Latin',
   'Indian',
   'Mixed',
   'Native American',
   'Caucasian',
  ],
  regionList: [
    'South America',
    'Central America',
    'North America',
    'Asia',
    'Europe', 
    'Eastern Europe',
    'Africa',
  ],
  /*tagList: [

  ]*/
};

export default filterList;