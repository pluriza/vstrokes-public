import React from 'react';
import AutoComplete from 'material-ui/AutoComplete';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';

class Filter extends React.Component {

  state = { searchText: '' };

  componentWillReceiveProps(nextProps) {
    const {tagList, filters: {tag}} = nextProps;
    if (tag) {
      const foundTag = tagList.find(({id}) => id === tag);
      if (foundTag) {
        this.setState(prevState => ({
          searchText: prevState.searchText || foundTag.title
        }));
      }
    } else {
      this.setState({searchText: ''});
    }
  }

  handleChange = (name, value) => {
    const {genderList, updateFilter} = this.props;
    if (name === 'gender' && value) {
      value = genderList.indexOf(value) + 1;
    }
    updateFilter(name, value);
  };

  handleChangeGender = (event, index, value) => this.handleChange("gender", value);
  handleChangeOrientation = (event, index, value) => this.handleChange("orientation", value);
  handleChangeBody = (event, index, value) => this.handleChange("bodyType", value);
  handleChangeHairColor = (event, index, value) => this.handleChange("hairColor", value);
  // handleChangeAge = (event, index, value) => this.handleChange("age", value);
  handleChangeEthnicity = (event, index, value) => this.handleChange("ethnicity", value);
  handleChangeRegion = (event, index, value) => this.handleChange("region", value);

  reset = event => {
    event.preventDefault();
    this.setState({
      searchText: ''
    });
    this.props.resetFilters();
  };

  handleUpdateSearch = searchText => this.setState({ searchText });

  handleNewRequest = (value, index) => {
    const { tagList, updateFilter } = this.props;
    let tag = value;
    if (index >= 0) {
      tag = tagList[index].id
    } else {
      const foundTag = tagList.find(({title}) => title === value);
      if (foundTag) {
        tag = foundTag.id;
      }
    }
    updateFilter("tag", tag);
  };

  buildTagDataSourceItem = (tag) => {
    const {title, count} = tag;
    return {
      text: title,
      value: (
        <MenuItem
          primaryText={`#${title}`}
          secondaryText={count}
        />
      )
    };
  };

  render() {
    const {
      filters: { gender, orientation, bodyType, hairColor,/* age,*/ ethnicity },
      genderList,
      orientationList,
      bodyList,
      hairList,
      // ageList,
      ethnicityList,
      regionList,
      tagList
    } = this.props;
    const selectedGender = gender ? genderList[gender - 1] : null;
    const selectedOrientation = orientation || null;
    const selectedBodyType = bodyType || null;
    const selectedHairColor = hairColor || null;
    const selectedEthnicity = ethnicity || null;
    // const selectedAge = age || null;
    return (
      <div className="filter-container" style={{paddingRight: '10%'}}>
        {/* Now 30 june 2017, i've created the search input and applied the style currently */}
        {this.props.showTags && <div style={{ position: 'relative', display: 'inline-block', width: '100%' }}>
          <FontIcon
            className="material-icons"
            style={{ position: 'absolute', right: 0, bottom: 15, width: 20, height: 20 }}
          >
            search
          </FontIcon>
          <AutoComplete
            floatingLabelText="Search"
            hintText="Search"
            filter={AutoComplete.fuzzyFilter}
            dataSource={tagList.map(this.buildTagDataSourceItem)}
            maxSearchResults={10}
            searchText={this.state.searchText}
            onNewRequest={this.handleNewRequest}
            onUpdateInput={this.handleUpdateSearch}
            textFieldStyle={{ width: '100%' }}
            listStyle={{ width: '100%' }}
            style={{ width: '100%' }}
          />
        </div>}
        <SelectField
          floatingLabelText="Gender"
          value={selectedGender}
          onChange={this.handleChangeGender}
          style={{ width: '100%' }}
        >
          {genderList.map((g, i) =>
            <MenuItem value={g} primaryText={g} key={i} />
          )}
        </SelectField>
        <SelectField
          floatingLabelText="Orientation"
          value={selectedOrientation}
          onChange={this.handleChangeOrientation}
          style={{ width:'100%' }}
        >
          {orientationList.map((g, i) =>
            <MenuItem value={g} primaryText={g} key={i} />
          )}
        </SelectField>
        <SelectField
          floatingLabelText="Body type"
          value={selectedBodyType}
          onChange={this.handleChangeBody}
          style={{ width: '100%' }}
        >
          {bodyList.map((g, i) =>
            <MenuItem value={g} primaryText={g} key={i} />
          )}
        </SelectField>
        <SelectField
          floatingLabelText="Hair color"
          value={selectedHairColor}
          onChange={this.handleChangeHairColor}
          style={{ width: '100%' }}
        >
          {hairList.map((g, i) =>
            <MenuItem value={g} primaryText={g} key={i} />
          )}
        </SelectField>
        {/*<SelectField
          floatingLabelText="Age range"
          value={selectedAge}
          onChange={this.handleChangeAge}
          style={{ width: 150 }}

        >
          {ageList.map((g, i) =>
            <MenuItem value={g} primaryText={g} key={i} />
          )}
        </SelectField>*/}
        <SelectField
          floatingLabelText="Ethnicity"
          value={selectedEthnicity}
          onChange={this.handleChangeEthnicity}
          style={{ width: '100%' }}
        >
          {ethnicityList.map((g, i) =>
            <MenuItem value={g} primaryText={g} key={i} />
          )}
        </SelectField>
        <SelectField
          floatingLabelText="Region"
          onChange={this.handleChangeRegion}
          style={{ width: '100%' }}
        >
          {regionList.map((g, i) =>
            <MenuItem value={g} primaryText={g} key={i} />
          )}
        </SelectField>
        <span className="reset-label" onClick={this.reset}>Clear Filters</span>
      </div>
    );
  }
}

export default Filter;
