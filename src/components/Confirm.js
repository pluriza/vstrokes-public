import React from 'react';

const Confirm = () => {
  return (
    <div className="confirm-content">
      <p className="confirm">
        Please, check your email to confirm your account.
      </p>
    </div>
  );
};

export default Confirm;
