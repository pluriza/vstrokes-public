import React from "react";
import Modal from "react-modal";

const Search = () => {
  return (
    <Modal
      className="search"
      contentLabel="Search-Modal">
      <div className="closeModal"> &times; </div>
      <div className="searchContainer">
        <div className="searchInput">
          <input type="text" className="input" placeholder="Search..."/>
        </div>
        <div className="searchButton">
          <i className="fa fa-search" aria-hidden="true"></i>
        </div>
      </div>
    </Modal>
  );
};

export default Search;




