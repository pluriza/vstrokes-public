import React, {Component} from "react";
import Footer from "./Footer/Footer";
import Home from "../containers/HomeContainer";

class App extends Component {

  render() {
    return (
      <div className="App">
        <div className="grid">
          <Home />
          <Footer/>
        </div>
      </div>
    );
  }
}

export default App;
