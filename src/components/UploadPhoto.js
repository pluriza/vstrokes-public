import React from 'react';
import Modal from 'react-modal';

const UploadPhoto = ({
  isLoginModalOpen,
  toggleLoginModal,
  toggleResetPasswordModal,
}) => {
  const onRequestClose = () => toggleLoginModal();
  const onGoToResetPassword = () => {
    toggleLoginModal();
    toggleResetPasswordModal();
  };
  return (
    <Modal
      className="modalUpload"
      isOpen={isLoginModalOpen}
      contentLabel="Login-Modal"
      onRequestClose={onRequestClose}
    >
    <div className="closeModal" onClick={onRequestClose}> &times; </div>
    <div className="title">Add photo</div>
    <div className="separator"></div>
    <div className="uploadButtons">
      <button className="button"><i className="fa fa-plus" aria-hidden="true">&nbsp;</i>Upload Photo</button>
      <button className="button">Take Photo</button>
    </div>
    <div className="imageArea"></div>
    </Modal>
  );
};

export default UploadPhoto;

