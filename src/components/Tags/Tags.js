import React, { Component } from 'react';
import Footer from '../Footer/Footer'

class Tags extends Component {

  componentDidMount() {
    this.props.getTags();
  }

  renderTag(tag) {
    return (
      <tr key={tag.id}>
        <td>{tag.title}</td>
        <td>{tag.count}</td>
      </tr>
    );
  }

  renderTags() {
    const { tags } = this.props;
    return tags.map(this.renderTag);
  }

  render() {
    const tags = this.renderTags();
    return (
      <div className="tags-container">
        <div className="tags">
          <table className="tags-table">
            <thead>
              <tr>
                <th>Tag</th>
                <th>Active Rooms</th>
              </tr>
            </thead>
            <tbody>{tags}</tbody>
          </table>
        </div>
        <Footer />
      </div>
    );
  }

}

export default Tags;