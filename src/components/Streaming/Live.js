import React, {Component} from 'react';
import LiveInfo from './LiveInfo';
import Tips from './Tips';
import VideoPlayer from "./VideoPlayer";

export default class Live extends Component {

  sendTips = (goalId) => (tips) => () => this.props.sendTips(this.props.roomId, goalId, tips);
  
  render(){
    const { streamKey } = this.props;
    // console.log('Live render streamKey', streamKey);
    const currentGoal = this.props.goals.find(goal => goal.status);
    
    return (
      <div className="live">
        <LiveInfo
          tags={this.props.tags}
          goals={this.props.goals}
        />
        {currentGoal && <Tips sendTips={this.sendTips(currentGoal.id)}/>}
        <div className="live-video">
          <div className="logo" />
          {streamKey && <VideoPlayer streamingKey={streamKey}/>}
        </div>
        <div className="control-video" />
      </div>
    );
  }
};
