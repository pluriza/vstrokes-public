import React, { Component } from 'react';
import WebRTC from "../../libs/webrtc";

class VideoPlayer extends Component {

  state = {
    src: null
  };

  async componentDidMount() {
    try {
      this.webrtc = new WebRTC(this.props.streamingKey);
      this.webrtc.setListener(streamURL => this.setState({src: streamURL}));
      await this.webrtc.initForPlayback();
    } catch (error) {
      console.log('VideoPlayer.componentDidMount error', error);
    }
  }

  componentWillUnmount() {
    if (this.webrtc) {
      this.webrtc.stopPlay();
    }
    this.webrtc = null;
  }

  render() {
    const {src} = this.state;
    if (src) {
      return <video className="video" autoPlay controls src={src}/>;
    } else {
      return (
        <div className="video" style={{minHeight: 243.5, color: '#F0F0F0'}}>
          LOADING...
        </div>
      );
    }
  }

}

export default VideoPlayer;