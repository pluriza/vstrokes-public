import React from 'react';

const LiveInfo = ({tags = [], goals = []}) => {
  const currentGoal = goals?goals.find(goal => goal.status):null;
  const nextGoal = goals.find(goal => currentGoal.index + 1 === goal.index);

  const renderGoal = goal => {
    return `${goal.description} ${goal.tips_current || 0}/${goal.tips_goal || goal.tokens}`;
  };

  return (
      <div className="live-description">
      <div className="tags">{tags.map((tag, i) => <span key={tag.id || i}> #{tag.title || tag} </span>)}</div>
      {goals.length > 0 && <div className="enter-goal" style={{fontWeight: 'bold'}}>Current goal:</div>}
      <div className="goal">
        {goals.length > 0 && renderGoal(currentGoal)}
      </div>
      {goals.length > 0 && nextGoal && <div className="enter-goal">Next goal:</div>}

      {goals.length > 0 && nextGoal && <div className="goal">
        {renderGoal(nextGoal)}
      </div>}
    </div>
  );
};

export default LiveInfo;
