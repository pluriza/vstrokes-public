import React, { Component } from 'react';
import WebRTC from "../../libs/webrtc";

class VideoStream extends Component {

  state = {
    src: null
  };

  async componentDidMount() {
    const constraints = {
      audio: true,
      video: true
    };
    try {
      this.webrtc = new WebRTC(this.props.broadcastKey, constraints);
      this.webrtc.setListener(streamURL => this.setState({src: streamURL}));
      this.webrtc.setEndStreamListener(this.props.endStream);
      this.intervalId = setInterval(async () => {
        const blob = await this.webrtc.takeSnapshow();
        this.props.updateRoomThumbnail(blob);
      }, 1000 * 60 * 2); // every two minutes
      await this.webrtc.initForStreaming();
    } catch (error) {
      console.log('VideoStream.componentDidMount:getUserMedia.error', error);
    }
  }

  componentWillUnmount() {
    if (this.webrtc) {
      this.webrtc.closeStream();
    }
    this.webrtc = null;
    if (this.intervalId) {
      clearTimeout(this.intervalId);
    }
  }

  render() {
    const {src} = this.state;
    return <video className="video" autoPlay muted src={src}/>;
  }

}

export default VideoStream;