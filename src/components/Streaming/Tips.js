import React from 'react';

const Tips = ({sendTips}) => {

    return (
        <div className="tips">
            <div className="tip" style={{cursor: 'default'}}>Tip</div>
            <div className="tip" title="send tips" onClick={sendTips(5)}>5</div>
            <div className="tip" title="send tips" onClick={sendTips(10)}>10</div>
            <div className="tip" title="send tips" onClick={sendTips(25)}>25</div>
            <div className="tip" title="send tips" onClick={sendTips(50)}>50</div>
        </div>
    );
};

export default Tips;
