import React, {Component} from "react";
import Chat2 from "../../containers/Chat2Container"
import Live from "./Live";
import AgeVerificationMessage from "../AgeVerificationMessage";
import history from "../../libs/history";
import {subscribeToLiveChanges, subscribeToLiveEnding} from '../../libs/socket';

class Streaming extends Component {

  constructor(props) {
    super(props);
    this.state = {
      streamKey: '',
      isOtherProfile: props.match.path !== "/profile", // TODO always false
      isMe: props.username === props.match.params.username,
      userOther: {
        username: null
      },
      tags: [],
      goals: []
    };
  }

  componentWillReceiveProps(nextProps) {
    const { goals } = nextProps.room;
    // console.log(goals);
    this.setState(prevState => {
      return {
        isMe: nextProps.username === nextProps.match.params.username,
        goals: [
          ...(goals || [])
        ]
      };
    });
  }

  componentDidMount() {
    const {match, getRoom} = this.props;
    const {username} = match.params;
    getRoom(username).then(({
      value: {
        streamingKey: streamKey,
        Tags: tags,
        RoomGoals: goals
      }
    }) => {
      return this.setState({streamKey, tags, goals})
    }).catch(() => history.replace(`/${username}`));
    this.setState({
      userOther: {
        username: username
      }
    });
    subscribeToLiveChanges((err, room, roomId) => {
      if (err) return;
      this.props.getGoals(roomId);
      // TODO show message of current goal
    });
    subscribeToLiveEnding((err, room) => {
      if (err) return;
      console.log('Streaming live ended', username, room);
      history.replace(`/${username}`);
    });
  }

  render() {
    return (
      <div className="streaming">
        <div className="streamingChat">
          <Chat2
            userOther={this.state.userOther}
            isOtherProfile={this.state.isOtherProfile}
            chats={this.props.chat}
            broadcastStatus={true}
          />
        </div>
        <div className="streamingVideo">
          {this.state.isMe && <AgeVerificationMessage />}
          <Live
            sendTips={this.props.sendTips(this.state.userOther.username, this.props.username)}
            roomId={this.props.room.id}
            streamKey={this.state.streamKey}
            tags={this.state.tags}
            goals={this.state.goals}
          />
        </div>
      </div>
    );
  }
}

export default Streaming;
