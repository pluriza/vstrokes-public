import {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {GA_TRACKING_ID} from '../index';

class GoogleAnalytics extends Component {

  componentWillUpdate({location, history}){
    const {gtag} = window;
    if (location.pathname === this.props.location.pathname) {
      return;
    }
    if (history.action === 'PUSH' && typeof gtag === 'function') {
      gtag('config', GA_TRACKING_ID, {
        page_title: window.document.title,
        page_location: window.location.href,
        page_path: location.pathname,
      });
    }
  }

  render () { return null;}
}

export default withRouter(GoogleAnalytics);