import React from 'react';
import Modal from 'react-modal';

//import icon from '../../../assets/images/CoinsiconGray.png';

class BuyAlbum extends React.Component {
  constructor(props) {
    super(props);
    this.onRequestClose = this.onRequestClose.bind(this);
    this.renderAlbum = this.renderAlbum.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  componentDidMount(){
  }

  onRequestClose = () => {
    this.props.toogleModalDetail();
  }

  renderAlbum () {
    const {album} = this.props;
    if (album.id) {
      return <div className="cover">
      <img className="coverImage" alt="cover" src={album.cover ? album.cover.uri :''}/>
      <div className="coverBlock">
        <i className="fa fa-lock lock" aria-hidden="true"></i>
        <div className="albumTitle">
          {album.title}
          <div className="albumStats">
            {/* <div className="albumTokens">
              0
              <img title="token" className="icon" src={icon} alt="tokens" />
            </div>
            <div className="albumViewers">
              0
              <i className="fa fa-eye" aria-hidden="true"></i>
            </div> */}
          </div>
        </div>
      </div>
    </div>
    }
  }

  onClick(e) {
    e.preventDefault();
    this.props.toogleModalDetail();
    this.props.showDetail();
  }

  render () {
    return (
      <Modal
          isOpen={this.props.isDetailAlbumModalOpen}
          onRequestClose={this.onRequestClose}
          className="modalBuyAlbum"
          contentLabel="Album-Modal-Creation">
          <div className="closeModal" onClick={this.onRequestClose}> &times;  </div>
          <div className="title">Buy this album</div>
          {this.renderAlbum()}
          <button className="buttonBuy" type="button" onClick={this.onClick}>Buy Album</button>
        </Modal>
    );
  }
}

export default BuyAlbum;