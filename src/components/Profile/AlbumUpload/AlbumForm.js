import React from "react";
import TextField from 'material-ui/TextField';

class AlbumForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: ''
    }
  }
  handleChangeTitle = (event, value) => this.props.handleChange("title",value);
  handleChangeToken = (event, value) => this.props.handleChange("tokens",value);
  handleChangeDescription = (event) => this.props.handleChange("description", event.target.value);

  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <div className="photoset">
          Photo set</div>
          <TextField
          required
          style={{
          width: 270
        }}
          hintText="Album's title"
          floatingLabelText="Title"
          onChange={this.handleChangeTitle}/>
        <div className="title-photoset">Description:
        </div>
        <textarea
        required
          className="description-photoset"
          name="description"
          rows="4"
          type="text"
          placeholder="Write a description (optional)"
          onChange={this.handleChangeDescription}></textarea>
        <br/>
        <div className="title-photoset">Tokens</div>
        <div className="question">Charge tokens to view this set?</div>
        <TextField
        required
          style={{
          width: 50
        }}
        type="number"
        min="0"
        name="Cachones"
        onChange={this.handleChangeToken}/>
        
        <br/>
        <button className="cancel-album" onClick={this.props.onRequestClose}>
          Cancel
        </button>
        {
          !this.props.created ? <button className="submit-album" type="submit">
          Submit
        </button> : <span>loader</span>
        }
      </form>
    );
  }
}

export default AlbumForm;
