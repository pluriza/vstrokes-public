import React, {Component} from "react";
import Modal from "react-modal";
import Form from "./AlbumForm"

class CreateAlbumModal extends Component {
  constructor(props) {
    super(props);
    this.onRequestClose = this.onRequestClose.bind(this);
    this.state = {
      title: '',
      description: '',
      tokens: '',
      files: [],
      count: 0,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeFile = this.handleChangeFile.bind(this);
    this.onClickPhoto = this.onClickPhoto.bind(this);
  }

  onRequestClose = () => {
    this.setState({
      count: 0,
      files: [],
      title: '',
      description: '',
    });
    this.props.toogleAlbumsModal();
  };

  handleChange(name, value) {
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    let album = {
      title: this.state.title,
      description: this.state.description,
      tokens: this.state.tokens,
    };
    // console.log('submit')
    // console.log(this.state)
    this.props.createAlbum(album, this.state.files);
    //this.props.createPictureAlbum(picture);
    this.onRequestClose();
  }

  handleChangeFile(e) {
    e.preventDefault();


    // let file = e.target.files[0];
    let files = Array.from(e.target.files);
    // console.log(Array.from(e.target.files))


    Object.keys(files).map((i) => {
      let reader = new FileReader();
      let _file = files[i];
      // console.log(_file);
      reader.onloadend = () => {
        this.setState({
          count: this.state.count + 1,
          files: [...this.state.files, _file],
        });
      };
      reader.readAsDataURL(_file);
      // console.log(this.state)
      return _file;
    });


    // reader.onloadend = () => {
    //   this.setState({
    //     count: 0,
    //     files: file,
    //   });
    // };

    // reader.readAsDataURL(file);

    // console.log(this.state)
  }

  onClickPhoto() {
    document.getElementById('files').click();
  }

  render() {
    return (
      <Modal
        className="modalUploadAlbum"
        isOpen={this.props.isAlbumsModalOpen}
        contentLabel="Album-Modal-Creation"
        onRequestClose={this.onRequestClose}
      >
        <div className="closeModal" onClick={this.onRequestClose}>&times;</div>
        <div className="title">Create a album</div>
        <div className="separator"/>
        <div className="uploadButtons">
          <button className="button" onClick={this.onClickPhoto}>
            <i className="fa fa-plus" aria-hidden="true"/> Create a photo set
          </button>
          <input
            id="files" type="file" accept="image/*" multiple name="files" className="hidden" ref="files"
            onChange={this.handleChangeFile}
          />
          {/*<button className="button">
            <i className="fa fa-plus" aria-hidden="true"/> Create a video
          </button>*/}
        </div>
        <span className="imageCount">You have selected {this.state.count} photos</span>
        <Form
          created={this.props.created}
          createPictureAlbum={this.props.createPictureAlbum}
          handleSubmit={this.handleSubmit}
          handleChange={this.handleChange}
          onRequestClose={this.onRequestClose}
        />
      </Modal>
    );
  }
}

export default CreateAlbumModal;
