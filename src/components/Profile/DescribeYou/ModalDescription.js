import React from "react";
import Modal from "react-modal";
import Form from "./DescribeYouForm"

const ModalDescription = ({isDescriptionMeModalOpen, toggleDescriptionMeModal, postDescriptionMe, description, updateProfile, profile, profileOther, upProfile }) => {
  const onRequestClose = () => toggleDescriptionMeModal();
  return (
    <Modal
      className="modalDescribeYou"
      isOpen={isDescriptionMeModalOpen}
      contentLabel="DescribeYou-Modal"
      onRequestClose={onRequestClose}>
      <div className="closeModal" onClick={onRequestClose}> &times; </div>
      <div className="form-container">
        <Form profile={profile} profileOther={profileOther} upProfile={upProfile} updateProfile={updateProfile} toggleDescriptionMeModal={toggleDescriptionMeModal} postDescriptionMe={postDescriptionMe} description={description}/>
      </div>
    </Modal>
  );
};

export default ModalDescription;