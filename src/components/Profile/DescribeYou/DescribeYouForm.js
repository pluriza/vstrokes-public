import React, {Component} from "react";


class DescribeYouForm extends Component {
    constructor(props){
        super(props);
        var {  profileOther: { shortbio } } = this.props;
        shortbio = shortbio != null ? shortbio : '';
        console.log(shortbio);
        console.log(this.props);

        this.state = {
            counter: 110,
            description: shortbio,
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({
            counter: 110 - e.target.value.length,
            description: e.target.value,
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.postDescriptionMe(this.state.description);
        this.props.upProfile({ shortbio: this.state.description });
        this.props.toggleDescriptionMeModal();
    }

    render() {
        return (
            <form>
                <textarea className="describeYou" rows="2" maxLength="110"
                placeholder="Describe who you are, what you like, hobbies…" onChange={this.handleChange} value={this.state.description}/>
                <div className="caracterCount">{this.state.counter}</div>
                <button className="save" type="submit" onClick={this.handleSubmit}>Save</button>
                <button className="cancel" type="button" onClick={this.props.toggleDescriptionMeModal}>Cancel</button>
            </form>
        );
    }
}

export default DescribeYouForm;