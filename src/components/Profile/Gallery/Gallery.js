import React, {Component} from "react";
import Modal from "react-modal";

class Gallery extends Component {

  constructor(props) {
    super(props);
    const {pictureId, pictures} = props;
    let picture = pictureId;
    let index = -1;
    if (!picture && pictures.length > 0) {
      index = 0;
      picture = pictures[index].id;
    } else if (pictures.length > 0) {
      index = pictures.findIndex(p => p.id === picture);
    }
    this.state = {
      current: picture,
      currentIndex: index
    };
  }

  componentWillReceiveProps(nextProps) {
    const {pictureId, pictures} = nextProps;
    let picture = pictureId;
    let index = -1;
    if (!picture && pictures.length > 0) {
      index = 0;
      picture = pictures[index].id
    } else if (pictures.length > 0) {
      index = pictures.findIndex(p => p.id === picture);
    }
    this.setState(prevState => ({
      current: picture,
      currentIndex: index
    }));
  }

  onRequestClose = () => {
    const {toggleGalleryModal} = this.props;
    if (toggleGalleryModal) toggleGalleryModal();
  };

  prevSlide = event => {
    event.preventDefault();
    this.setState(prevState => {
      const tempPrevIndex = prevState.currentIndex - 1;
      if (-1 < tempPrevIndex) {
        return {
          currentIndex: tempPrevIndex,
          current: this.props.pictures[tempPrevIndex].id
        };
      } else {
        return {
          currentIndex: this.props.pictures.length - 1,
          current: this.props.pictures[this.props.pictures.length - 1].id
        };
      }
    });
  };

  nextSlide = event => {
    event.preventDefault();
    this.setState(prevState => {
      const tempNextIndex = prevState.currentIndex + 1;
      if (this.props.pictures.length > tempNextIndex) {
        return {
          currentIndex: tempNextIndex,
          current: this.props.pictures[tempNextIndex].id
        };
      } else {
        return {
          currentIndex: 0,
          current: this.props.pictures[0].id
        };
      }
    });
  };

  moveToSlide = id => () => {
    const currentIndex = this.props.pictures.findIndex(picture => picture.id === id);
    this.setState({
      current: id,
      currentIndex
    });
  };

  handleOnKeyDown = event => {
    switch (event.keyCode) {
      case 37:
        this.prevSlide(event);
        break;
      case 39:
        this.nextSlide(event);
        break;
      default:
        break;
    }
  };

  render() {
    const {isGalleryModalOpen, pictures} = this.props;
    if (isGalleryModalOpen) {
      document.addEventListener("keydown", this.handleOnKeyDown);
    } else {
      document.removeEventListener("keydown", this.handleOnKeyDown);
    }
    const {
      //current, 
      currentIndex
    } = this.state;
    const currentPicture = pictures[currentIndex];

    // const tempStart = currentIndex - 1;
    // const start = tempStart < 0 ? 0 : tempStart;
    // const tempEnd = currentIndex + 1;
    // const end = tempEnd < pictures.length ? tempEnd : pictures.length - 1;

    // const visiblePictures = [];

    // for (var i = start; i <= end; i++) {
    //   visiblePictures.push(pictures[i]);
    // }

    return (
      <Modal
        className="modalGallery"
        isOpen={isGalleryModalOpen}
        contentLabel="Login-Modal"
        onRequestClose={this.onRequestClose}>
        <div className="closeModal" onClick={this.onRequestClose}> &times; </div>
        <div className="picture">
          {currentPicture && (
            <img src={currentPicture.uri} alt="slide" className="image" key={currentPicture.id}/>
          )}
          <a className="prev" onClick={this.prevSlide}><i className="fa fa-angle-right fa-flip-horizontal"/></a>
          <a className="next" onClick={this.nextSlide}><i className="fa fa-angle-right"/></a>

          {/* <div className="caption-container">
              <p id="caption"></p>
            </div> */}
          {/*visiblePictures.map((picture, index) => (
              <div key={picture.id} className={`column ${picture.id === current ? 'active' : ''}`}>
                <img className="demo" src={picture.uri} onClick={this.moveToSlide(picture.id)} alt="item" style={{width: '100%'}}/>
              </div>
            ))*/}
        </div>
      </Modal>
    );
  }
}

export default Gallery;