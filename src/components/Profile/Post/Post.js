import React from 'react';
import profile from '../../../assets/images/camImages/blonde1.jpg';

const Post = () => {
  return (
    <div className="rectanglePost">
      <div className="user-post ">
        <div className="photo-post-content">
          <img
            src={profile}
            className="photo-post"
            alt="Profile"
          />
        </div>
        <div className="name-post">
          Quodsi Ipsam
          <div className="live-now">7 hrs</div>
        </div>
      </div>
      <div className="post-media-content">
        <img
          src={profile}
          className="post-media"
          alt="Here goes a cam preview"
        />
      </div>
      <ul className="labelPostContent">
        <li>
          <span className="postLabel">Comment</span>
        </li>
        <div className="separator" />
        <li>
          <span className="postLabel">
            <i className="fa fa-thumbs-up icon" aria-hidden="true"></i></span>
        </li>
        <div className="separator" />
        <li>
          <span className="postLabel">Share</span>
        </li>
      </ul>
      <div className="likes"> 5 likes</div>
    </div>
  );
};

export default Post;
