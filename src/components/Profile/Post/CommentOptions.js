import React from 'react';
import moment from 'moment';

const CommentOptions = ({created}) => {

  let time = moment(created);
  return (
        <div className="commentOptions">
            {/* <div className="likeComment">Me gusta</div>
            <div className="separator">·</div>
            <div className="reply">Responder</div> 
            <div className="separator">·</div> */}
            <div className="timeComment">{time.startOf('minute').fromNow()}</div>
        </div>
  );
};

export default CommentOptions;
