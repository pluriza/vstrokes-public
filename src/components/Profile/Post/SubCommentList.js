import React from 'react';
import Comment from './Comment';

const SubCommentList = ({comments}) => {
  return (
    <div className="subCommentList">
      {comments.map((comment, key) => {
        return (
          <div key={comment.id}>
            <Comment
              id={comment.id}
              user={comment.User}
              content={comment.content}
              created={comment.createdAt}
            />
          </div>
        )
      })}
    </div>
  );
};

export default SubCommentList;
