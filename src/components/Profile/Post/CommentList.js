import React from 'react';
import Comment from './Comment';
import SubCommentList from './SubCommentList';

const CommentList = ({parentId, username, comments, deleteComment}) => {
  
  return (
    <div className="CommentList">
      {
        (typeof comments !== 'undefined' && comments !== null ) ?  
            comments.map((comment, key) => {
              return (
                <div key={comment.id}>
                  <Comment
                    id={comment.id}
                    user={comment.User}
                    content={comment.content}
                    created={comment.createdAt}
                  />
                  {comment.Comments !== null
                    ? <SubCommentList comments={comment.Comments}/>
                    : null}
                </div>
              )
            })

      : null }
    </div>
  );
};

export default CommentList;
