import React from 'react';
import { withRouter } from 'react-router-dom';
import CommentList from './CommentList';
import profilePicture from "../../../assets/images/woman_profile_icon.png";

/*const Comments = (comments, id) => {

    return (
        <div className="comments">
            {comments !== null
                ? <CommentList comments={comments.comments} id={id}/>
                : ''}
            <div className="addComment">
                <div className="photo-post-content">
                    <img src={profile} className="photo-post" alt="profile"/>
                </div>
                <div
                    contentEditable="true"
                    className="newComment"
                    placeholder="Make a comment..."></div>
            </div>
        </div>
    );
};*/

import {connect} from "react-redux";
import {createComment, deleteComment} from "../../../actions/post";

class Comments extends React.Component {

  state = {
    content: ''
  }

  handleChange = e => {
    this.setState({content: e.target.value});
  };

  handleSubmit = event => {
    const {isLogged, history, username, id} = this.props;
    if (event.key === 'Enter') {
      event.preventDefault();
      if (!isLogged) {
        history.push('signup');
        return;
      }
      const {content} = this.state;
      this.props.createComment(id, content, username);
      this.setState({content: ''})
    }
  };

  render() {
    const {comments, isLogged, id, username, currentProfilePicture, deleteComment} = this.props;
    return (
      <div className="comments">
        {comments !== null
          ? <CommentList parentId={id} username={username} comments={comments} id={id} deleteComment={deleteComment}/>
          : ''}
        <div className="addComment">
          <div className="photo-post-content">
            <img src={(isLogged && currentProfilePicture) || profilePicture} className="photo-post" alt="profile"/>
          </div>
          <form className="form" onSubmit={this.handleSubmit}>
            <textarea
              rows="1"
              onKeyPress={this.handleSubmit}
              value={this.state.content}
              onChange={this.handleChange}
              className="newComment"
              placeholder="Make a comment..."
            />
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const {entities: {users}, authentication: {userId, isLogged}} = state;
  const user = users[userId];
  const {profileImage: currentProfilePicture} = user || {};
  return {isLogged, currentProfilePicture};
};

const mapDispatchToProps = dispatch => {
  return {
    createComment: (parent_id, content, username) => dispatch(createComment(parent_id, content, username)),
    deleteComment: (parent_id, username, comment_id) => dispatch(deleteComment(parent_id, username, comment_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Comments));
