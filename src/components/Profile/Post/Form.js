import React from "react";
import { trim } from "ramda";
import UploadPicturePost from "../../../containers/Profile/UploadPicture/UploadPicturePostContainer";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      preview: []
    }
    this.postInput = '';
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onChangePreview = this.onChangePreview.bind(this);
  }

  onSubmitHandler (e) {
    e.preventDefault();
    const fixedInputValue = trim(this.postInput.value);
    if (fixedInputValue === "") return;
    return this.props.createPost(fixedInputValue).then(() => (this.postInput.value = ""));
  };

  onChangePreview (preview) {
    this.setState({
      preview: [ ...this.state.preview, preview]
    })
  }

  render() {
    return (
      <div className="rectangleNewPost">
        <ul className="labelPostContent">
          <li>
            <span className="postLabel">Write a message</span>
          </li>
        </ul>
        <form onSubmit={this.onSubmitHandler} className="postForm">
          <textarea
            ref={ref => (this.postInput = ref)}
            rows={2}
            className="rectanglePost"
            placeholder="Say something..."
          />
          {/* TODO: Styles and positions */}
          <div style={
            {
              position: 'absolute'
            }
          }>
          {
            this.state.preview.map((preview, i) => (
                <img src={preview} alt="" key={i} width="20" height="20"/>
            ))
          }
          </div>
          {/* <div className="postOptions">
            <i className="fa fa-picture-o" aria-hidden="true" onClick={()=> this.props.toggleUploadPicturePostModal()} />
            <i className="fa fa-video-camera" aria-hidden="true" />
          </div> */}
          <div className="buttonPost">
            <button type="submit" className="buttonNewPost">
              Post
            </button>
          </div>
          <UploadPicturePost onChangePreview={this.onChangePreview} />
        </form>
      </div>
    );
  }
}

export default Form;
