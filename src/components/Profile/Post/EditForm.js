import React from 'react';

const EditForm = ({ id, content, updatePost }) => {
  let postInput;

  const onSubmitHandler = e => {
    e.preventDefault();
    return updatePost(id, postInput.value);
  };

  return (
    <div className="rectangleNewPost">
      <form onSubmit={onSubmitHandler}>
        <textarea
          ref={ref => (postInput = ref)}
          rows={2}
          defaultValue={content}
          className="rectanglePost"
        />
        <div className="buttonPost">
          <button type="submit" className="buttonNewPost">
            Edit
          </button>
        </div>
      </form>
    </div>
  );
};

export default EditForm;
