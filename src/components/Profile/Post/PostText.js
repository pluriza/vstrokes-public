import React, { Component } from "react";
import moment from 'moment';
import profilePicture from "../../../assets/images/woman_profile_icon.png";
import EditForm from "../../../containers/Profile/Post/EditFormContainer";
import Comments from "./Comments";

class PostText extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isEditable: false
    };

    this.onDeletePost = this.onDeletePost.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }

  componentWillReceiveProps() {
    this.setState({ isEditable: false });
  }

  onDeletePost(e) {
    e.preventDefault();
    const { postId, deletePost } = this.props;
    return deletePost(postId);
  }

  onEdit(e) {
    e.preventDefault();
    this.setState({ isEditable: true });
  }
  render() {
    const { content, postId, isOtherProfile, user } = this.props;
    const { Profile, username } = user;
    let postProfilePicture = profilePicture;
    if (Profile && Profile.Albums.length) {
      const {Albums} = Profile;
      const {Pictures} = Albums[0];
      if (Pictures.length) {
        postProfilePicture = Pictures[0].uri;
      }
    }
    const { isEditable } = this.state;
    return (
      <div className="rectanglePost">
        <div className="user-post ">
          <div className="photo-post-content">
            <img src={postProfilePicture} className="photo-post" alt="Here goes a cam preview" />
          </div>
          <div className="name-post">
            {username}
            <div className="live-now">{moment(this.props.created).startOf('minute').fromNow()}</div>
          </div>
        </div>
        {isOtherProfile
          ? ""
          : <i className="fa fa-angle-down tooltip" aria-hidden="true">
              <span className="tooltiptext">
                <ul>
                  <li onClick={this.onDeletePost}>Delete</li>
                  <div className="menu-separator" />
                  <li onClick={this.onEdit}>Edit</li>
                </ul>
              </span>
            </i>}
        {isEditable
          ? <EditForm id={postId} content={content} />
          : <textarea rows={2} className="textPost" value={content} disabled />}
        <Comments username={username} comments={this.props.comments} id={postId}/>
      </div>
    );
  }
}

export default PostText;
