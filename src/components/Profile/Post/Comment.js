import React from "react";
import profile from "../../../assets/images/woman_profile_icon.png";
import CommentOptions from "./CommentOptions";


class Comment extends React.Component {
  
  delete = () => {
    const { parentId, username, id } = this.props;
    this.props.deleteComment(parentId, username, id)
  }

  render(){
    const { user, content, created } = this.props;
    const {username, Profile } = user;
    const { Albums } = Profile;
    return (
      <div className="comment">
        <div className="delete-comment" onClick={this.delete}>x</div>
        <div className="photo-post-content">
          <img
            src={Albums.length && Albums[0].Pictures.length ? Albums[0].Pictures[0].uri : profile}
            className="photo-post"
            alt="Profile"
          />
        </div>
        <div className="commentContent">
          <span className="userComent">{username}:</span>{content}
        </div>
        <CommentOptions created={created}/>
      </div>
    );
  }
}

export default Comment;
