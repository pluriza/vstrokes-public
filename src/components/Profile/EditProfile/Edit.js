import React, {Component} from "react";
import {Link} from "react-router-dom";
import history from '../../../libs/history';
import EditGeneralForm from "../../../containers/Profile/EditProfile/EditGeneralFormContainer";
import EditContactForm from "../../../containers/Profile/EditProfile/EditContactFormContainer";
import EditDetailsForm from "../../../containers/Profile/EditProfile/EditDetailsFormContainer";

const tabNames = {
  general: 'General info',
  contact: 'Social Media Profile',
  details: 'Details about you'
};

class Edit extends Component {

  state = {
    currentTab: 'general',
    isCurrentTabADirtyWhore: false
  };

  onTabGotDirty = () => this.setState({isCurrentTabADirtyWhore: true});

  onTabGotClean = () => this.setState({isCurrentTabADirtyWhore: false}, history.replace('/profile'));

  openTab = tab => () => {
    const {isCurrentTabADirtyWhore} = this.state;
    if (!isCurrentTabADirtyWhore || window.confirm("If you proceed, your changes will be lost. Leave?")) {
      this.setState(prevState => {
        const state = { isCurrentTabADirtyWhore: false };
        if (tab !== prevState.currentTab) {
          state.currentTab = tab;
        }
        return state;
      });
    }
  };

  renderTab(tab) {
    const { profile, profileOther, email } = this.props;
    switch (tab) {
      case 'general':
        return <EditGeneralForm profile={profile} profileOther={profileOther} onTabGotDirty={this.onTabGotDirty} onTabGotClean={this.onTabGotClean}/>;
      case 'contact':
        return <EditContactForm profile={profile} profileOther={profileOther} email={email} onTabGotDirty={this.onTabGotDirty} onTabGotClean={this.onTabGotClean}/>;
      case 'details':
        return <EditDetailsForm profile={profile} profileOther={profileOther} onTabGotDirty={this.onTabGotDirty} onTabGotClean={this.onTabGotClean}/>;
      default:
        return null;
    }
  }

  renderTabNavButton = tab => {
    const classNames = ['tablinks'];
    if (this.state.currentTab === tab) {
      classNames.push('active');
    }
    return (
      <button
        className={classNames.join(' ')}
        key={tab}
        onClick={this.openTab(tab)}
      >
        {tabNames[tab]}
      </button>
    );
  };

  render() {
    const { currentTab } = this.state;
    return (
      <div className="rectangleAbout">
        <div className="rectangleText">Bio</div>
        <Link to="/profile">
          <i className="fa fa-times" aria-hidden="true"/>
        </Link>
        <div className="separator"/>
        <div className="mainC">
          <div className="secondC">
            <div className="aboutContent left">
              {Object.keys(tabNames).map(this.renderTabNavButton)}
            </div>
            <div className="aboutContent right tabcontent first">
              {this.renderTab(currentTab)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Edit