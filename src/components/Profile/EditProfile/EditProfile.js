import React, { Component } from "react";
import Edit from "./Edit";
import { isEmpty } from "ramda";
import HeaderP from "../../../containers/Profile/HeaderPContainer";
import LeftSideProfile from "../LeftSideProfile";
import ModalDescription from "../../../containers/Profile/ModalDescriptionContainer";

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileOther: {},
      userOther: {},
      isEdit: props.match.path === "/profile/edit"
    };
    this.upProfile = this.upProfile.bind(this);
  }
  componentDidMount() {
    const { profile, getUserProfile } = this.props;
    if (isEmpty(profile)) return getUserProfile().then((data)=>{
      this.setState({
        profileOther: data.value.profile,
        userOther: data.value.user,
      })
    });
  }

  async upProfile(shortbio) {
    const data = await this.props.updateProfile(shortbio);
    this.setState({ profileOther: data.value });
    this.props.history.push("/profile")
  }

  render() {
    const { username, profile } = this.props;
    return (
      <div>
        <HeaderP
          description={this.props.description}
          userOther={this.state.userOther}
          profileOther={this.state.profileOther}
          username={username} profile={profile}
          profileImage={this.props.profileImage}
          coverImage={this.props.coverImage}
        />
        <div style={{display: 'flex', width: 900, margin: '0 auto'}}>
          <LeftSideProfile isEdit={this.state.isEdit} profile={profile} profileOther={this.state.profileOther} />
          <Edit profile={profile} profileOther={this.state.profileOther} email={this.props.email}/>
          <ModalDescription profileOther={this.state.profileOther} upProfile={this.upProfile}/>
        </div>
      </div>
    );
  }
}

export default EditProfile;
