import React, {Component} from "react";

class EditDetailsForm extends Component {

  constructor(props) {
    super(props);
    const { profileOther: {biography} } = props;
    this.state = {
      biography: biography || ''
    };
  }

  componentWillReceiveProps(nextProps) {// console.log(nextProps);
    const { profileOther } = nextProps;
    let { biography } = profileOther;
    this.setState({
      biography: biography || ''
    });
  }

  onChangeBio = ({ target: {value}}) => this.setState({biography: value}, this.props.onTabGotDirty);

  onSubmit = e => {
    e.preventDefault();
    const {updateProfile, onTabGotClean} = this.props;
    const {biography} = this.state;
    updateProfile({biography}).then(onTabGotClean);
  };

  render() {
    const {biography} = this.state;
    return (
      <form onSubmit={this.onSubmit}>
        {" "}
        <div className="bioTitle">Bio</div>
        <textarea
          className="bioContent"
          value={biography}
          onChange={this.onChangeBio}
          rows={2}
        />
        <button>Save Changes</button>
      </form>
    );
  }
}

export default EditDetailsForm;
