import React, { Component } from "react";
import list from "../../SideMenu/FiltersContents";
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import AutoComplete from 'material-ui/AutoComplete';
import countries from '../../Countries'
import languages from '../../Languages'

class EditGeneralForm extends Component {

  constructor(props) {
    super(props);
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
    this.maxDate.setHours(0, 0, 0, 0);
    this.state = {
      // name: '',
      birthdate: this.maxDate,
      gender: 1,
      ethnicity: '',
      orientation: '',
      country: '',
      city: '',
      lenguaje: '',
      bodyType: '',
      hairColor: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    const { profileOther } = nextProps;
    let {
      // name,
      birthdate,
      gender,
      ethnicity,
      orientation,
      country,
      city,
      lenguaje,
      bodyType,
      hairColor,
    } = profileOther;
    // name = name || '';
    birthdate = birthdate ? new Date(birthdate) : this.maxDate;
    gender = gender || 1;
    ethnicity = ethnicity || '';
    orientation = orientation || '';
    country = country || '';
    city = city || '';
    lenguaje = lenguaje || '';
    bodyType = bodyType || '';
    hairColor = hairColor || '';

    this.setState({
      // name,
      birthdate: new Date(birthdate.getUTCFullYear(), birthdate.getUTCMonth(), birthdate.getUTCDate()),
      gender,
      ethnicity,
      orientation,
      bodyType,
      country,
      city,
      lenguaje,
      hairColor,
    });
  }

  onEditProfile = (e) => {
    e.preventDefault();
    const {updateProfile, onTabGotClean} = this.props;
    const {birthdate} = this.state;
    updateProfile({
      ...this.state,
      birthdate: birthdate.toISOString()
    }).then(onTabGotClean);
  };

  handleChangeDate = (event, date) => this.handleChange("birthdate", date);
  handleChangeGender = (event, index, value) => this.handleChange("gender", value);
  handleChangeOrientation = (event, index, value) => this.handleChange("orientation", value);
  handleChangeBody = (event, index, value) => this.handleChange("bodyType", value);
  // handleChangeAge = (event, index, value) => this.handleChange("age", value);
  handleChangeEtnicity = (event, index, value) => this.handleChange("ethnicity", value);
  handleUpdateCountry = searchText => this.handleChange('country', searchText);
  handleNewRequestCountry = (value, index) => {
    const country = index >= 0 ? countries[index] : value;
    this.handleChange("country", country);
  };
  // handleChangeCity = (event, value) => this.handleChange("city", value);
  handleChangeLenguaje = (event, value) => this.handleChange("lenguaje", value);
  handleUpdateLanguage = searchText => this.handleChange('lenguaje', searchText);
  handleNewRequestLanguage = (value, index) => {
    const language = index >= 0 ? languages[index] : value;
    this.handleChange("lenguaje", language);
  };
  // handleChangeName = (event, value) => this.handleChange("name", value);
  handleChangeHairColor = (event, index, value) => this.handleChange("hairColor", value);

  handleChange = (name, value) => {
    this.setState({ [name]: value }, this.props.onTabGotDirty);
  };

  render() {
    return (
      <form onSubmit={this.onEditProfile}>
        {/* <TextField
          hintText="Put your name"
          floatingLabelText="Name"
          fullWidth={true}
          value={this.state.name}
          onChange={this.handleChangeName}
        /> */}
        <DatePicker
          style={{
            width: 150,
            marginRight: 15,
            float: 'left'
          }}
          autoOk={true}
          className="picker"
          floatingLabelText="Birthday"
          hintText="Birthday"
          openToYearSelection={true}
          value={this.state.birthdate}
          maxDate={this.maxDate}
          onChange={this.handleChangeDate}
        />
        <SelectField
          style={{
            width: 150,
            float: 'left'
          }}
          floatingLabelText="Gender"
          value={this.state.gender}
          onChange={this.handleChangeGender}
        >
          {list
            .genderList
            .map((g, i) => <MenuItem value={i + 1} primaryText={g} key={i} />)}
        </SelectField>
        <SelectField
          style={{
            width: 150,
            float: 'left',
            marginRight: 15
          }}
          floatingLabelText="Ethnicity"
          value={this.state.ethnicity}
          onChange={this.handleChangeEtnicity}
        >
          {list
            .ethnicityList
            .map((g, i) => <MenuItem value={g} primaryText={g} key={i} />)}
        </SelectField>
        <AutoComplete
          style={{
            width: 150,
            float: 'left',
            display: 'flex'
          }}
          hintText="Put your country"
          floatingLabelText="Country"
          filter={AutoComplete.fuzzyFilter}
          dataSource={countries}
          maxSearchResults={5}
          searchText={this.state.country}
          onNewRequest={this.handleNewRequestCountry}
          onUpdateInput={this.handleUpdateCountry}
        />
        {/* <TextField
          style={{
            width: 150,
            marginRight:15,
            float: 'left'
          }}
          hintText="Put your city"
          floatingLabelText="City"
          value={this.state.city}
          onChange={this.handleChangeCity}
        /> */}
        <SelectField
          style={{
            width: 150,
            marginRight: 15,
            float: 'left'
          }}
          floatingLabelText="Orientation"
          value={this.state.orientation}
          onChange={this.handleChangeOrientation}
        >
          {list
            .orientationList
            .map((g, i) => <MenuItem value={g} primaryText={g} key={i} />)}
        </SelectField>
        <SelectField
          style={{
            width: 150,
            float: 'left',
          }}
          floatingLabelText="Hair color"
          value={this.state.hairColor}
          onChange={this.handleChangeHairColor}
        >
          {list
            .hairList
            .map((g, i) => <MenuItem value={g} primaryText={g} key={i} />)}
        </SelectField>
        <SelectField
          style={{
            width: 150,
            float: 'left',
            marginRight: 15,
          }}
          floatingLabelText="Body type"
          value={this.state.bodyType}
          onChange={this.handleChangeBody}
        >
          {list
            .bodyList
            .map((g, i) => <MenuItem value={g} primaryText={g} key={i} />)}
        </SelectField>
        <AutoComplete
          style={{
            width: 150,
            float: 'left',
            display: 'flex'
          }}
          hintText="Put your language"
          floatingLabelText="Language"
          filter={AutoComplete.fuzzyFilter}
          dataSource={languages}
          maxSearchResults={5}
          value={this.state.lenguaje}
          onNewRequest={this.handleNewRequestLanguage}
          onUpdateInput={this.handleUpdateLanguage}
        />
        <br style={{ clear: 'both' }} />
        <button type="submit" onClick={this.onEditProfile}>Save Changes</button>
      </form>
    );
  }
}

export default EditGeneralForm;
