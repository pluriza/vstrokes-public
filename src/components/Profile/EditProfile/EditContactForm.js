import React, {Component} from "react";
import TextField from 'material-ui/TextField';

class EditContactForm extends Component {

  constructor(props) {
    super(props);
    this.state = this.setupState(props);
  }

  componentWillReceiveProps(nextProps) {
    this.setState(this.setupState(nextProps));
  }

  setupState = (props) => {
    const {profileOther} = props;
    const {
      /* phone = '', */website, socialLink
    } = profileOther;
    return {
      // phone,
      website: website || 'http://',
      socialLink: socialLink || 'http://'
    };
  };

  makeStringStartWithHttp = (value, prevValue) => {
    value = value.trim();
    let startsHttp = value.match(/^[a-zA-Z]+:\/\//);
    if (prevValue.match(/^[a-zA-Z]+:\/\/$/) && !startsHttp) {
      value = 'http://';
      startsHttp = true;
    }
    if (!value || !startsHttp) {
      value = 'http://' + value;
    }
    return value;
  };

  handleChangeSocial = (event, value = 'http://') => {
    value = this.makeStringStartWithHttp(value, this.state.socialLink);
    this.onChange("socialLink", value);
  };
  handleChangeWeb = (event, value = 'http://') => {
    value = this.makeStringStartWithHttp(value, this.state.website);
    this.onChange("website", value);
  };
  // handleChangePhone = (event, value) => this.onChange("phone", value);

  onChange = (name, value) => this.setState({[name]: value}, this.props.onTabGotDirty);

  onSubmit = e => {
    e.preventDefault();
    const {updateProfile, onTabGotClean} = this.props;
    const {socialLink, website} = this.state;
    const data = {
      socialLink: socialLink.match(/^[a-zA-Z]+:\/\/$/) ? '' : socialLink,
      website: website.match(/^[a-zA-Z]+:\/\/$/) ? '' : website
    };
    updateProfile(data).then(onTabGotClean);
  };

  render() {
    const {socialLink, website/*, phone */} = this.state;
    return (
      <form onSubmit={this.onSubmit}>
        <TextField
          hintText="Put your e-mail"
          floatingLabelText="Email"
          fullWidth={true}
          value={this.props.email}
          disabled
        />
        <TextField
          hintText="Add your social link"
          floatingLabelText="Social link"
          fullWidth={true}
          value={socialLink}
          onChange={this.handleChangeSocial}
        />
        <TextField
          hintText="Add a website"
          floatingLabelText="Website"
          fullWidth={true}
          value={website}
          onChange={this.handleChangeWeb}
        />
        {/*<TextField
          hintText="Put your phone"
          floatingLabelText="Phone"
          type="number"
          fullWidth={true}
          value={phone}
          onChange={this.handleChangePhone}
        />*/}
        <button type="submit">Save Changes</button>
      </form>
    );
  }
}

export default EditContactForm;
