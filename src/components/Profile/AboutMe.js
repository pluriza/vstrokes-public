import React from "react";
import list from "../SideMenu/FiltersContents";
import {Link} from "react-router-dom";
import moment from "moment";

const generateDescription = (data) => {
  let gender;
  if (data) {
    gender = list.genderList[data.gender - 1];
  }
  const { birthdate, orientation, country } = data;
  const age = birthdate !== null ? moment.utc(birthdate).toNow(true) + ' old' : null;
  const countryText = !country ? '' : `from ${country}`;
  return `I am a ${age} ${orientation} ${gender} ${countryText}`;
};

const generateAboutMe = ({biography, socialLink, website, ...data}) => (
  <div>
    {biography && <p>{biography}</p>}
    <p>{generateDescription(data)}</p>
    {socialLink && (
      <p>
        <a target="_blank" rel="noopener noreferrer" href={socialLink} title={socialLink}>
          Social profile
        </a>
      </p>
    )}
    {website && (
      <p>
        <a target="_blank" rel="noopener noreferrer" href={website} title={website}>
          My website
        </a>
      </p>
    )}
  </div>
);

const AboutMe = ({isOtherProfile, profileOther}) => {
  let aboutMe = '';
  if (isOtherProfile) {
    if (!profileOther || !(profileOther.biography || profileOther.birthdate || profileOther.country)) {
      aboutMe = 'Nothing to show';
    } else {
      aboutMe = generateAboutMe(profileOther);
    }
  } else {
    if (!profileOther || !(profileOther.biography || profileOther.birthdate)) {
      aboutMe = (
        <Link to="/profile/edit" className="create-bio">
          <span>+ Create your biography...</span>
        </Link>
      );
    } else {
      aboutMe = generateAboutMe(profileOther);
    }
  }

  return (
    <div className="rectangleAboutMe">
      <div className="rectangleText">About</div>
      {!isOtherProfile && (
        <Link to="/profile/edit">
          <i className="fa fa-cog" aria-hidden="true"/>
        </Link>
      )}
      <div className="rectangleAboutMe2">
        {aboutMe}
      </div>
    </div>
  );
};

export default AboutMe;
