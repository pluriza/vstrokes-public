import React, {Component} from 'react';
import CreateAlbumModal from './AlbumUpload/CreateAlbumModal';
import BuyAlbum from './AlbumUpload/BuyAlbum';
import Gallery from "../../containers/Profile/GalleryContainer";
import icon from '../../assets/images/CoinsiconGray.png';

class Albums extends Component {
  constructor(props) {
    super(props);
    this.toogleModal = this
      .toogleModal
      .bind(this);
    this.toogleModalDetail = this
      .toogleModalDetail
      .bind(this);
    this.openModalBuyDetail = this.openModalBuyDetail.bind(this);
    this.state = {
      isShowDetail: false,
    };
    this.showDetail = this.showDetail.bind(this);
  }

  componentDidMount = () => {
    this
      .props
      .getAlbums(this.props.isMe);
  };

  toogleModal() {
    this
      .props
      .toggleAlbumsModal();
  }

  toogleModalDetail() {
    this.props.toggleDetailAlbumModal();
  }

  openModalBuyDetail(id) {
    this.props.getAlbum(id, this.props.isMe);
    this.toogleModalDetail()
  }

  showDetail() {
    this.setState({
      isShowDetail: !this.state.isShowDetail,
    })
    // this.props.albumActive
  }

  render() {
    let albumDetail;
    if (this.props.albumActive.id) {
      albumDetail =
        <div>
          <div className="albumDetail">
            <div className="albumTitle">{this.props.albumActive.title}</div>
            <div className="albumDescription">{this.props.albumActive.content}</div>
          </div>
          {this
            .props
            .albumActive.Pictures
            .map((r, i) => <div className="pictureRect" key={i} onClick={() => this.props.toggleGalleryModal(r.id)}>
              <div className="albumImgContainer">
                <img title="album" className="albumImg" src={r.uri} alt={i}/>
              </div>
            </div>)
          }
        </div>
    }
    else {
      albumDetail = null
    }

    const albumAll = this
      .props
      .albums
      .map((r, i) => <div className="albumRect" key={i} onClick={() => this.openModalBuyDetail(r.id)}>

        <div className="albumImgContainer">
          <img title="album" className="albumImg" src={r.cover ? r.cover.uri : ''} alt={i}/>
          <div className="albumInfo">
            <i className="fa fa-lock fa-5x lock" aria-hidden="true"/>
            <div className="albumTitle">
              {r.title}
              <div className="albumStats">
                <div className="albumTokens">
                  0
                  <img title="token" className="icon" src={icon} alt="tokens"/>
                </div>
                <div className="albumViewers">
                  0
                  <i className="fa fa-eye" aria-hidden="true"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>);

    return (
      <div className="rectangleAlbum">
        <div className="title">Albums</div>
        {
          !this.state.isShowDetail ?
            <button
              type="button"
              className="buttonAlbum back"
              onClick={this.props.toogleAlbums}
              title="Go back">
              <i className="fa fa-sign-out fa-flip-horizontal fa-lg" aria-hidden="true"/>
            </button> :
            <button
              type="button"
              className="buttonAlbum back"
              onClick={this.showDetail}
              title="close detail">
              <i className="fa fa-sign-out fa-flip-horizontal fa-lg" aria-hidden="true"/>
            </button>
        }

        {!this.props.isOtherProfile?!this.props.created  
          ? <button type="button" className="buttonAlbum" onClick={this.toogleModal}>+ Create Album</button>
          : <span className="buttonAlbum">Creando album...</span>:''}

        <div className="albumContainer "
             style={(!albumAll.length && this.props.loading) ? {maxHeight: 0, animation: 'unset'} : {}}>
          {/* =================NO PHOTOS================= */}
          {/* <div className="noAlbum">No photos to show.</div> */}
          {/* ================ALBUM DETAIL=============== */}
          {
            this.state.isShowDetail ? albumDetail : albumAll
          }
          {/* <div className="albumDetail">
                        <div className="albumTitle">[Album Title]</div>
                        <div className="albumDescription">[Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.]</div>
                    </div>
                    {this
                        .props
                        .albums
                        .map((r, i) => <div className="albumRect" key={i}>

                            <div className="albumImgContainer">
                                <img title="album" className="albumImg" src={r.Pictures} alt={i}/>
                            </div>
                        </div>)} */}
          {/* ================ALBUMS LIST=============== */}
          {/* {this
                        .props
                        .albums
                        .map((r, i) => <div className="albumRect" key={i} onClick={()=>this.openModalBuyDetail(r.id)}>

                            <div className="albumImgContainer">
                                <img title="album" className="albumImg" src={r.Pictures} alt={i}/>
                                <div className="albumInfo">
                                <i className="fa fa-lock fa-5x lock" aria-hidden="true"></i>
                                    <div className="albumTitle">
                                        {r.title}
                                        <div className="albumStats">
                                            <div className="albumTokens">
                                                0
                                                <img title="token" className="icon" src={icon} alt="tokens"/>
                                            </div>
                                            <div className="albumViewers">
                                                0
                                                <i className="fa fa-eye" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>)} */}
        </div>
        <CreateAlbumModal
          created={this.props.created}
          createAlbum={this.props.createAlbum}
          createPictureAlbum={this.props.createPictureAlbum}
          toogleAlbumsModal={this.toogleModal}
          isAlbumsModalOpen={this.props.isAlbumsModalOpen}/>
        <BuyAlbum toogleModalDetail={this.toogleModalDetail}
                  isDetailAlbumModalOpen={this.props.isDetailAlbumModalOpen}
                  album={this.props.albumActive} showDetail={this.showDetail}/>
        <Gallery pictures={this.props.albumActive.Pictures || []}/>
      </div>
    )
  }
}

export default Albums;