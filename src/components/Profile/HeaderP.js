import React, { Component } from "react";
import cover from "../../assets/images/Banner.png";
import profilePicture from "../../assets/images/woman_profile_icon.png";
import HeaderName from "./HeaderName";
import UploadPicture from "../../containers/Profile/UploadPicture/UploadPictureContainer";
import gif from "../../assets/images/loading (1).gif";

class HeaderP extends Component {
  constructor(props) {
    super(props);
    this.toggleModal = this.toggleModal.bind(this);
    this.state = {
      action: 0
    }
  }

  toggleModal(e, action) {
    e.preventDefault();
    this.props.toggleUploadPictureModal();
    this.setState({
      action
    });
  };

  render() {
    const { coverImage, coverImageLoading, profileImage, profileImageLoading } = this.props;
    const coverImageSrc = coverImage ? coverImage : cover;
    const profileImageSrc = profileImageLoading ? gif : (profileImage ? profileImage : profilePicture);
    return (
      <div className="content">
        <div className="mainImage-container">
        {!this.props.isOtherProfile ? 
          <div className="configMainPhoto tooltip">
            <i className="fa fa-cog" aria-hidden="true">
              &nbsp;
            </i>
            <span>Upload Photo</span>
            <div className="configMainPhoto-menu tooltiptext">
              <ul>
                {/*<li>Choose from my photos</li>*/}
                <div className="menu-separator" />
                <li onClick={(evt) => this.toggleModal(evt, 2)}>Upload photo</li>
              </ul>
            </div>
          </div>
          : null
        }
        {coverImageLoading ? 
          <div className="mainImage loading">
            <div className="mainImage-loading-container">
              <img src={gif} className="mainImage-loading" alt="cover"/>
            </div>
          </div>
        :
          <img src={coverImageSrc} className="mainImage" alt="cover" />
        }
          <div className="profilePhoto-container">
            <img src={profileImageSrc} className="profilePhoto" alt="profile" />
            {!this.props.isOtherProfile ? 
            <div className="profilePhoto-hover">
              <div className="hover-content" onClick={(evt) => this.toggleModal(evt, 1)}>
                <i className="fa fa-cog" aria-hidden="true">
                  &nbsp;
              </i>
                <span>Upload Photo</span>
              </div>
            </div>
            : null}
          </div>
        </div>
        <HeaderName 
          userOther={this.props.userOther}
          profileOther={this.props.profileOther} 
          username={this.props.username} 
          profile={this.props.profile} 
          toggleDescriptionMeModal={this.props.toggleDescriptionMeModal}
          description={this.props.description} 
          isOtherProfile={this.props.isOtherProfile}
        />
        <hr className="separator-h2" />
        <UploadPicture action={this.state.action} />
      </div>
    );
  }
}

export default HeaderP;