import React, {Component} from "react";
import Modal from "react-modal";
import gif from "../../../assets/images/loading (1).gif";

class UploadPicture extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: "",
      imagePreviewUrl: "",
      visual: false,
      loading: true
    };
  }

  onClickFile = () => {
    document.getElementById('file').click();
  };

  handleSubmit(e) {
    e.preventDefault();
    const {toggleUploadPictureModal,postUserImageProfile, postUserImageCover} = this.props;
    toggleUploadPictureModal();
    if (this.props.action === 1) {
      postUserImageProfile(this.state.file)
    } else if (this.props.action === 2) {
      postUserImageCover(this.state.file)
    }
    this.setState({visual: false, file: '', imagePreviewUrl: ''});
    console.log(this.props.action)
  }

  handleImageChange(e) {
    e.preventDefault();

    let file = e.target.files[0];
    const image = new Image();
    image.addEventListener('load', async () => {
      const canvas = document.createElement('canvas');
      let { width, height } = image;
      const specWidth = this.props.action === 1 ? 1 : 225;
      const maxWidth = this.props.action === 1 ? 478 : 1280;
      const specHeight = this.props.action === 1 ? 1 : 84;
      const maxHeight = 478;
      if (width / height > specWidth / specHeight) {
        height = height > maxHeight ? maxHeight : height;
        const trueWidth = height * specWidth / specHeight;
        canvas.width = trueWidth;
        canvas.height = height;
        const srcX = (width - trueWidth) / 2;
        canvas.getContext('2d').drawImage(image, srcX, 0, trueWidth, height, 0, 0, trueWidth, height);
      } else {
        width = width > maxWidth ? maxWidth : width;
        const trueHeight = width * specHeight / specWidth;
        canvas.width = width;
        canvas.height = trueHeight;
        const srcY = (height - trueHeight) / 2;
        canvas.getContext('2d').drawImage(image, 0, srcY, width, trueHeight, 0, 0, width, trueHeight);
      }
      const blob = await (new Promise(resolve => canvas.toBlob(resolve)));

      let reader = new FileReader();

      reader.onloadend = () => {
        this.setState({visual: true, file: blob, imagePreviewUrl: reader.result});
      };

      reader.readAsDataURL(blob);
    });
    image.src = URL.createObjectURL(file);
  }

  toogleModal = () => {
    this.setState({visual: false, file: '', imagePreviewUrl: '', loading: true});
    this.props.toggleUploadPictureModal();
  };

  loading = () => {
    if (this.state.loading) {
      setTimeout(() => {
        this.setState({loading: false});
      }, 1000);
    }
  };

  render() {
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    let modal;
    if (imagePreviewUrl) {
      $imagePreview = <img alt="profilePicture" src={imagePreviewUrl}/>;
    } else {
      $imagePreview = <div className="previewText">Please select an Image for Preview</div>;
    }

    if (!this.state.loading) {
      modal = (
        <Modal
          className="modalUploadPicture"
          isOpen={this.props.isUploadPictureModalOpen}
          onRequestClose={this.toogleModal}
          contentLabel="Upload-Picture-Modal"
          onAfterOpen={this.loading}>
          <div className="closeModal" onClick={this.toogleModal}>
            &times;
          </div>
          <div className="title">{this.props.action === 1 ? 'Add profile photo' : 'Add cover photo'}</div>
          <div className="separator"/>
          <div className="previewComponent">
            <form onSubmit={e => this.handleSubmit(e)} style={{width: '100%'}}>
              <div className="uploadButtons">
                {/* <label htmlFor="file"><i className="fa fa-plus" aria-hidden="true">&nbsp;</i>Upload Photo</label> */}
                <button className="button" type="button" onClick={this.onClickFile}>Upload Photo</button>
                <input
                  className="inputfile button hidden"
                  name="file"
                  id="file"
                  type="file"
                  onChange={e => this.handleImageChange(e)}/>
                {/*<button className="button" type="button" onClick={this.toogleModal}>Take Photo</button>*/}
              </div>
            </form>
            <div className="imageArea">
              {$imagePreview}
            </div>
            {this.state.visual
              ? <button
                className="submitButton"
                type="submit"
                onClick={e => this.handleSubmit(e)}>
                Upload Image
              </button>
              : null}
          </div>
        </Modal>
      );
    }
    else {
      modal = (
        <Modal
          className="modalLoading"
          isOpen={this.props.isUploadPictureModalOpen}
          onRequestClose={this.toogleModal}
          contentLabel="Upload-Picture-Modal"
          onAfterOpen={this.loading}>
          <img alt="loading" src={gif} className="loading"/>
        </Modal>
      );
    }

    return modal;

  }
}

export default UploadPicture;
