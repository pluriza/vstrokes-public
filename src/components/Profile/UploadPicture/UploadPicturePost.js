import React, { Component } from "react";
import Modal from "react-modal";

class UploadPicturePost extends Component {
  constructor(props) {
    super(props);
    this.state = { file: "", imagePreviewUrl: "", visual: false };
    this.toogleModal = this.toogleModal.bind(this);
    this.onClickFile = this.onClickFile.bind(this);
  }

  onClickFile() {
    document.getElementById('file').click();
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.toggleUploadPicturePostModal()
    this.props.onChangePreview(this.state.imagePreviewUrl)
    this.setState({
        visual: false,
        file: '',
        imagePreviewUrl: ''
      });
  }

  handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        visual: true,
        file: file,
        imagePreviewUrl: reader.result
      });
    };

    reader.readAsDataURL(file);
  }

  toogleModal() {
    this.setState({
        visual: false,
        file: '',
        imagePreviewUrl: ''
      });
    this.props.toggleUploadPicturePostModal();
  }

  render() {
  
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = <img alt="profilePicture" src={imagePreviewUrl} />;
    } else {
      $imagePreview = (
        <div className="previewText">Please select an Image for Preview</div>
      );
    }

    return (
      <Modal
        className="modalUploadPicture"
        isOpen={this.props.isUploadPicturePostModalOpen}
        contentLabel="Upload-Picture-Modal"
        onRequestClose={this.toogleModal}
      >
      <div className="closeModal" onClick={this.toogleModal}> &times; </div>
      <div className="title">Add photo</div>
      <div className="separator"></div>
        <div className="previewComponent">
          <form onSubmit={e => this.handleSubmit(e)}>
            <div className="uploadButtons">
              {/* <label htmlFor="file"><i className="fa fa-plus" aria-hidden="true">&nbsp;</i>Upload Photo</label> */}
              <button className="button" type="button" onClick={this.onClickFile}>Upload Photo</button> 
              <input
              className="inputfile button hidden"
              name="file" id="file"
              type="file"
              onChange={e => this.handleImageChange(e)}
            />
             {/* <button className="button" type="button" onClick={this.toogleModal}>Take Photo</button> */}
            </div>
          </form>
          <div className="imageArea">
            {$imagePreview}
          </div>
          {this.state.visual ? 
          <button
              className="submitButton"
              type="submit"
              onClick={e => this.handleSubmit(e)}

            >
              Upload Image
            </button>
            : null}
        </div>
      </Modal>
    );
  }
}

export default UploadPicturePost;
