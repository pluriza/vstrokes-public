import React, {Component} from "react";
import Modal from "react-modal";
import TagCreation from "./TagCreation";
import GoalCreation from "./GoalCreation";
// import GoLive from "./GoLive";

class RoomCreation extends Component {

  state = {
    currentStep: 1,
    tags: [],
  };

  onRequestClose = () => {
    this.setState({
      currentStep: 1,
      tags: []
    });
    this.props.toggleRoomCreationModal();
  };

  nextStep = tags => this.setState(prevState => ({
    currentStep: prevState.currentStep + 1,
    tags
  }));
  
  createRoom = goals => {
    this.props.createRoom({
      tags: this.state.tags,
      goals
    }).then(this.onRequestClose);
  };

  renderStep(step) {
    switch (step) {
      case 1:
        return <TagCreation next={this.nextStep} />;
      case 2:
        return <GoalCreation createRoom={this.createRoom} />;
      default:
        return null;
    }
  }

  renderTitle = (step) => {
    return ({
      1: "STEP 1 - Tag your show",
      2: "STEP 2 - Create Tip Goal",
    })[step];
  };

  render() {
    const {
      isCreateRoomModalOpen,
      isCreatingRoom, activeRoom, // broadcastKey
    } = this.props;
    const {currentStep} = this.state;

    return (
      <Modal
        className="modalCreationRoom"
        isOpen={isCreateRoomModalOpen}
        contentLabel="CreateRoom-Modal"
        onRequestClose={this.onRequestClose} >
        <div className="closeModal" onClick={this.onRequestClose}> &times; </div>
        <div className="form-container">
        { isCreatingRoom ?
          <span>Creating room</span>
        :
        activeRoom ||
          <div>
            <div className="form-title-label">{this.renderTitle(currentStep)}</div>
            {this.renderStep(currentStep)}
          </div>
        }
        </div>
      </Modal>
    );
  }
}
/*<GoLive broadcastKey={broadcastKey} onGoLive={this.onRequestClose}/>*/

export default RoomCreation;
