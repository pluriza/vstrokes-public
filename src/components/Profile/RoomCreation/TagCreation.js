import React, { Component } from "react";
import TextField from 'material-ui/TextField';

class FormRoomCreation extends Component {

  state = {
    tags: "",
    chips: [],
    disabled: true
  };

  onTagChange = (event, value) => this.setState({ tags: value.trim() });

  createTags = event => {
    event.preventDefault();
    const {keyCode} = event;
    if (keyCode === 13) {
      const {tags, chips} = this.state;
      if (tags === '' || chips.indexOf(tags) >= 0) {
        return;
      }
      this.setState(({chips}) => ({
        tags: "",
        chips: [
          ...chips,
          tags
        ],
        disabled: chips.length < 3
      }));
    }
    return false;
  };

  deleteTag = number => () => {
    this.setState(({chips}) => ({
      chips: [
        ...chips.slice(0, number),
        ...chips.slice(number + 1)
      ],
      disabled: chips.length < 5
    }));
  };

  next = () => {
    const {next} = this.props;
    const {chips} = this.state;
    if (chips.length >= 4) {
      next(chips);
    }
  };

  render() {
    const { chips, disabled, tags } = this.state;
    let chipsDiv = "You have not added any tags yet";
    if (chips.length > 0) {
      chipsDiv = chips.map((chip, i) => (
        <div className="chip" key={i}>
          #{chip}
          <span className="closebtn" onClick={this.deleteTag(i)}>&times;</span>
        </div>
      ));
    }
    return (
      <form onSubmit={event => event.preventDefault()}>
        <TextField
          hintText="Add tag and hit enter"
          floatingLabelText="Tags"
          maxLength="40"
          fullWidth={true}
          value={tags}
          onChange={this.onTagChange}
          onKeyUp={this.createTags}
        />
        <div className="help-text">Tags help your show get found - Use <strong>at least four</strong> tags that relate to your show</div>
        <div>{chipsDiv}</div>
        <br style={{ clear: 'both' }}/>
        <button className="send-button" type="button" disabled={disabled} onClick={this.next}>
          Next
        </button>
      </form>
    );
  }
}

export default FormRoomCreation;