import React from 'react';

const GoLive = ({broadcastKey, onGoLive}) => (
  <div>
    <h1 className="form-title-label">STEP 3 - GO Live!</h1>
    <div style={{ textAlign: 'justify'}}>
      <div>
        You are now ready to show yourself to the world!
      </div>
      {/*<div>
        We're almost there. To broadcast your show, there are just a few more steps you have to take:
      </div>
      <div>
        There are many tools you can use to broadcast your show, to ensure your
        broadcast is perfect, we recommend you use a free tool called OBS Studio.
      </div>
      <div>
        <a target="_blank" rel="noopener noreferrer" href="https://obsproject.com/download">Click here</a> to
        open a new window where you can download OBS by selecting your Operating System and clicking "Download Installer".
        Install it on your computer and launch it.
      </div>
      <div>
        A popup window should appear asking you to use the Auto-Configuration Wizard, accept it.
        <br />
        Follow the instructions:
        <ul style={{ fontSize: 14 }}>
          <li>In the Usage Information choose "Optimize for streaming, recording is secondary", then click Next</li>
          <li>In the Video Settings leave it as it is or choose your preferences, then click Next</li>
        </ul>
        In the Stream Information set the following:
        <ul style={{ fontSize: 14 }}>
          <li>Stream Type: Select Custom Streaming Server</li>
          <li>Server: <input defaultValue="rtmp://stream.vinegarstrokes.com:1935/live" disabled/></li>
          <li>Stream Key: <input defaultValue={broadcastKey} disabled/></li>
          <li>Uncheck "Estimate bitrate with bandwidth test"</li>
          <li>Click Next</li>
        </ul>
        Wait for the final results and click Apply Settings.
      </div>
      <div>
        You can change these settings anytime by opening the settings window clicking the Settings button in the
        controls panel (bottom right) or clicking the menu File > Settings and the select the Stream section.
      </div>
      <div>
        Add a source for your stream:
        <ul style={{ fontSize: 14 }}>
          <li>On the Sources panel at the bottom, add a Source by clicking the + button and select Video Capture Device, then OK</li>
          <li>Leave these settings as they are and click OK (unless you know what you're doing ;))</li>
          <li>Adjust your camera by resizing and/or moving the red frame</li>
        </ul>
        Finally, click Start Streaming.
      </div>*/}
      <button className="send-button"
        style={{ float: 'none', margin: '25px auto 0' }}
        onClick={onGoLive}
      >
        GO LIVE!
      </button>
    </div>
  </div>
);

export default GoLive;