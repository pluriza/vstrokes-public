import React, {Component} from "react";
import TextField from 'material-ui/TextField';

class GoalCreation extends Component {

  state = {
    goalText: "",
    goalValue: 100,
    goals: [],
    disabled: true
  };
  
  onChangeDescription = (event, value) => this.onChange("goalText", value);
  
  onChangeAmount = (event, value) => this.onChange("goalValue", value);

  onChange = (name, value) => this.setState({[name]: value});

  createGoal = () => {
    const {goalText, goalValue} = this.state;
    if (goalText.length < 1 || goalValue < 1) 
      return;
    this.setState(prevState => ({
      goals: [
        ...prevState.goals, {
          description: prevState.goalText,
          tokens: prevState.goalValue
        }
      ],
      goalText: "",
      goalValue: 100,
      disabled: false
    }));
    document.getElementById("description").focus();
  };

  deleteGoal = index => () => {
    //onClick={this.deleteGoal(i)}
    this.setState(prevState => ({
      goals: [
        ...prevState
          .goals
          .slice(0, index),
        ...prevState
          .goals
          .slice(index + 1)
      ],
      disabled: prevState.goals.length < 2
    }));
  };

  next = () => {
    const {createRoom} = this.props;
    const {goals} = this.state;
    if (goals.length > 0) {
      createRoom(goals)
    }
  };

  render() {
    const {goalText, goalValue, goals, disabled} = this.state;
    return (
      <form>
        <label className="new-goal">Add a goal:</label>
        <div className="new-goal-inputs">
          <TextField
          id="description"
            hintText="Describe your goal"
            floatingLabelText="Description"
            value={goalText}
            onChange={this.onChangeDescription}
          />
          <TextField
            floatingLabelText="Tokens required to complete goal"
            type="number"
            value={goalValue}
            min={1}
            onChange={this.onChangeAmount}
          />
          <div className="add-button">
            <i className="fa fa-plus" aria-hidden="true" onClick={this.createGoal} />
          </div>
        </div>
        <br /><br />
        <div className="table">
          <table>
            <thead style={{ fontSize: 18 }}>
              <tr>
                <th>Goal</th>
                <th style={{ textAlign: 'right' }} >Strokes</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {goals.length > 0 ? goals.map((goal, i) => {
                return (
                  <tr key={i}>
                    <td>{goal.description}</td>
                    <td>{goal.tokens}</td>
                    <td style={{ textAlign: 'center' }}>
                      <i className='fa fa-close' aria-hidden="true" onClick={this.deleteGoal(i)}/>
                    </td>
                  </tr>
                );
              }): <tr><td colSpan={3} style={{textAlign: 'center'}}>Create some goals!</td></tr>}
            </tbody>
          </table>
        </div>
        <button className="send-button" type="button" disabled={disabled} onClick={this.next}>GO LIVE!</button>
      </form>
    );
  }
}

export default GoalCreation;