import React from "react";
import AboutMe from "../../containers/Profile/AboutMeContainer";
import Media from "./Media";
//import Gifts from "./Gifs";
import AddFriend from "../Profile/AddFriend/AddFriend"

/*import img1 from "../../assets/images/camImages/asiangirl1.jpg";
import img2 from "../../assets/images/camImages/blonde1.jpg";
import img3 from "../../assets/images/camImages/blonde2.jpg";
import img4 from "../../assets/images/camImages/brunette1.jpg";
import img5 from "../../assets/images/camImages/brunette2.jpg";
import img6 from "../../assets/images/camImages/brunette3.jpg";
import img7 from "../../assets/images/camImages/guy1.jpg";
import img8 from "../../assets/images/camImages/guy2.jpg";
import img9 from "../../assets/images/camImages/guy3.jpg";
import img10 from "../../assets/images/camImages/guy4.jpg";

import gift1 from "../../assets/images/Cupcake example.png";
import gift2 from "../../assets/images/Movie example.png";
import gift3 from "../../assets/images/Bottle example.png";*/
//import Chat from "../../containers/ChatContainer";
import Chat2 from "../../containers/Chat2Container";

const LeftSideProfile = (props) => {
  const {
    profile, isOtherProfile, /* username, */profileOther, broadcastStatus, userOther, profileImage,
    usernameProfile, isEdit, toogleAlbums, isMe, picturesAll, toggleGalleryModal, chat, isFollowedByUser,
    onFollow, onUnfollow,
  } = props;
  /*const exampleMediaImages = {
    mediaList: [
      {
        image: img1
      }, {
        image: img2
      }, {
        image: img10
      }, {
        image: img3
      }, {
        image: img9
      }, {
        image: img4
      }, {
        image: img8
      }, {
        image: img5
      }, {
        image: img6
      }, {
        image: img7
      }, {
        image: img1
      }, {
        image: img2
      }, {
        image: img10
      }, {
        image: img3
      }, {
        image: img9
      }, {
        image: img4
      }, {
        image: img8
      }, {
        image: img5
      }, {
        image: img6
      }, {
        image: img7
      }, {
        image: img1
      }, {
        image: img2
      }, {
        image: img10
      }, {
        image: img3
      }, {
        image: img9
      }
    ],
    giftsList: [
      {
        image: gift2,
        name: "Movie Night",
        text: "Lorem Ipsun",
        strokes: "75",
        date: "2017-03-10"
      }, {
        image: gift3,
        name: "Beer Bottle",
        text: "Grand Canon",
        strokes: "30",
        date: "2017-02-107"
      }, {
        image: gift1,
        name: "Cupcake",
        text: "Donna Spencer",
        strokes: "40",
        date: "2017-02-9"
      }
    ]
  };*/

  return (
    <div className="leftContent">
      {isOtherProfile && <AddFriend isFollowedByUser={isFollowedByUser} onFollow={onFollow} onUnfollow={onUnfollow} />}
      <AboutMe profile={profile} isOtherProfile={isOtherProfile} profileOther={profileOther}/>
      {/*<Chat/>*/}
      { !isEdit ?
        <div>
          <Chat2
            userOther={userOther}
            isOtherProfile={isOtherProfile}
            chats={chat}
            broadcastStatus={broadcastStatus}
            inProfile={true}
            usernameProfile={usernameProfile}
            profileImage={profileImage}
          />       
          <Media
            isMe={isMe}
            toogleAlbums={toogleAlbums}
            /*mediaList={exampleMediaImages.mediaList}*/
            isOtherProfile={isOtherProfile}
            picturesAll={picturesAll}
            toggleGalleryModal={toggleGalleryModal}
          />
        </div> : null}
      {/* <Gifts giftsList={exampleMediaImages.giftsList} isOtherProfile={isOtherProfile}/> */}
    </div>
  );
};

export default LeftSideProfile;
