import React from "react";
import {Prompt} from 'react-router-dom';
import LiveNow from "./LiveNow";
import NoPost from "./NoPost";
import Tools from "../../containers/Profile/ToolsContainer";
import Form from "../../containers/Profile/Post/FormContainer";
import PostText from "../../containers/Profile/Post/PostTextContainer";

const Feed = ({
  sortedPostsArray,
  username,
  ageVerified,
  isStreamOnline,
  isOtherProfile,
  userPosts,
  openStrokeMeModal,
  getPostsUser,
  postsUser,
  room,
  profileImage,
  scroll,
  addGoal,
  nextGoal,
  getGoals,
  sendTips,
  deleteRoom,
  broadcastKey,
  currentUsername,
  sessionId,
  updateRoomThumbnail,
}) => {
  const postArea = (postsArray, isOtherProfile) => {
    if (typeof postsArray !== 'undefined' && postsArray !== null) {
      if (isOtherProfile) {
        postsArray = postsUser
      }
      if (postsArray.length === 0) {
        return <NoPost/>;
      }
      return postsArray.map(post => <PostText
        isOtherProfile={isOtherProfile}
        key={post.id}
        postId={post.id}
        user={post.User}
        content={post.content}
        comments={post.Comments}
        created={post.createdAt}/>);
    } else {
      return null;
    }
  };
  return (
    <div className="rightContent" style={scroll?{height:'140vh'}:{height: 'auto'} }>
      <div className="staticZone">
        {!isOtherProfile && <Prompt when={isStreamOnline} message="Are you sure you want to leave? The stream will end." />}
        {isStreamOnline &&
        <LiveNow
          sendTips={sendTips(username, currentUsername)}
          addGoal={addGoal}
          nextGoal={nextGoal}
          getGoals={getGoals}
          deleteRoom={deleteRoom}
          profileImage={profileImage}
          isOtherProfile={isOtherProfile}
          broadcastKey={!isOtherProfile && broadcastKey}
          username={username}
          ageVerified={ageVerified}
          tags={room.tags}
          goals={room.goals}
          roomId={room.id}
          streamingKey={room.streamingKey}
          updateRoomThumbnail={updateRoomThumbnail(sessionId)}
        />}
        <Tools
          isStreamOnline={isStreamOnline}
          isOtherProfile={isOtherProfile}
          openStrokeMeModal={openStrokeMeModal}
          goals={room.goals || []}
        />
        {!isOtherProfile && <Form/>}
      </div>
      <div className="scrollZone">
        {isOtherProfile
          ? postArea(userPosts, isOtherProfile)
          : postArea(sortedPostsArray)}
      </div>
    </div>
  );
};

export default Feed;
