import React, { Component } from "react";

import RoomCreation from "../../../containers/Profile/RoomCreationContainer";
import RoomClosing from "../../../containers/Profile/RoomClosingContainer";

import history from "../../../libs/history";

import start from "../../../assets/images/Camara off icon.png";
import end from "../../../assets/images/Camera live icon.png";
import coins from "../../../assets/images/Coins icon.png";
import gift from "../../../assets/images/Gift icon.png";
import chatOn from "../../../assets/images/Chat On icon .png";
/* import chatOff from "../../../assets/images/Chat off icon.png"; */

import ToolItem from "./ToolItem";

class Tools extends Component {
  constructor(props) {
    super(props);
    this.state={
      /* isLive: false */
    }
  }

  handleStartLiveShow = e => {
    e.preventDefault();
    //this.setState({ isLive: !this.state.isLive })
    this.props.openRoomCreationModal();
  };

  handleEndLiveShow = e => {
    e.preventDefault();
    //this.setState({ isLive: !this.state.isLive })
    this.props.toggleRoomClosingModal();
  };

  goToMyBalance = e => {
    e.preventDefault();
    history.push("/tokens");
  };

  render() {
    /* const { isLive } = this.state; */
    const { isStreamOnline, isOtherProfile, goals } = this.props;

    const sendMeAGift = <ToolItem
      name="Send me a gift"
      style={{cursor:'no-drop'}}
      image={gift}
      imageAlt="Gift"
      onClick={()=> {}}
    />;

    const startLiveShow = <ToolItem
      name={isStreamOnline ? "End live show" : "Start a live show"}
      image={isStreamOnline ? end : start}
      style={{cursor:'pointer'}}
      textStyle={{color: '#FF4C00', fontWeight: 'bold'}}
      imageAlt="Start a live show now!"
      onClick={!isStreamOnline ? this.handleStartLiveShow : this.handleEndLiveShow}
    />;

    const strokeMe = <ToolItem
      name="Stroke Me"
      image={coins}
      imageAlt="Srtokes"
      onClick={() => {} /*this.props.openStrokeMeModal*/}
    />;

    const myStroke = <ToolItem
    style={{cursor:'pointer'}}
      name="My strokes"
      image={coins}
      imageAlt="Srtokes"
      onClick={this.goToMyBalance}
    />;

    const PrivateShow = <ToolItem
      name="Private Show"
      image={chatOn}
      imageAlt="Private Show"
      onClick={() => {}}
    />;

    const MyGifts = <ToolItem
    style={{cursor:'no-drop'}}
      name="My Gifts"
      image={gift}
      imageAlt="Gifts"
      onClick={() => {}}
    />;

    const goalsContent = goals.reduce((result, goal) => {
      return {
        current: result.current + goal.tips_current,
        total: result.total + (goal.tips_goal || goal.tokens),
      }
    }, {
      current: 0,
      total: 0,
    });

    return (
      <div className="rectangleToolsContainer">
        { isOtherProfile ? sendMeAGift : startLiveShow }
        { isOtherProfile ? strokeMe : myStroke }
        { isOtherProfile ? PrivateShow : MyGifts }
        <div className="rectangleTool" style={isOtherProfile ? {padding: '5px 10px'} : {}} >
          <div className="goalsContent" style={isOtherProfile ? {marginTop: 0} : {}} >
            {goalsContent.current} / {goalsContent.total}
          </div>
          <div className="toolName" >
            {isOtherProfile ? 'Strokes Needed for Goal' : 'My Goal'}
          </div>
        </div>
        <RoomCreation />
        <RoomClosing  />
      </div>
    );
  }
}

export default Tools;
