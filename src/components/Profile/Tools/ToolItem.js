import React from "react";

const ToolItem = ({name, image, imageAlt, onClick, style, textStyle}) => (
  <div className="rectangleTool" onClick={onClick} style={style}>
    <div className="toolImageContent">
      <img src={image} className="toolImage" alt={imageAlt} />
    </div>
    <div className="toolName" style={textStyle}>{name}</div>
  </div>
);

export default ToolItem;
