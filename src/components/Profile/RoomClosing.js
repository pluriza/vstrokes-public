import React from "react";
import Modal from "react-modal";

const RoomClosing = ({ isClosingRoomModalOpen, toggleRoomClosingModal, deleteRoom }) => {
  const onRequestClose = () => toggleRoomClosingModal();
  return (
    <Modal
      className="modalClosingRoom"
      isOpen={isClosingRoomModalOpen}
      contentLabel="ClosingRoom-Modal"
      onRequestClose={onRequestClose} >
      <div className="form-container">
        <div className="message">Are you sure?</div>
        <div>
          <button className="button" onClick={deleteRoom}>Yes</button>
          <button className="button" onClick={onRequestClose}>No</button>
        </div>
      </div>
    </Modal>
  );
};

export default RoomClosing;
