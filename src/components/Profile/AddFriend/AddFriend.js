import React, { Component } from "react";

class AddFriend extends Component {

  state = {
    unfollowText: "FOLLOWING",
  };

  handleUnfollowMouseEnter = () => this.setState({ unfollowText: "UNFOLLOW" });
  handleUnfollowMouseLeave = () => this.setState({ unfollowText: "FOLLOWING" });

  render() {
    const {isFollowedByUser, onFollow, onUnfollow} = this.props;
    return (
      <div className="friend-options">
        <div className="friend-option">
          {isFollowedByUser ?
            <a
              onClick={onUnfollow}
              onMouseEnter={this.handleUnfollowMouseEnter}
              onMouseLeave={this.handleUnfollowMouseLeave}
            >
              {!isFollowedByUser && <i className="fa fa-plus" aria-hidden="true"/>}
              {this.state.unfollowText}
            </a> :
            <a onClick={onFollow}><i className="fa fa-plus" aria-hidden="true"/>FOLLOW</a>
          }
        </div>
        {/*
      <div className="friend-option">
        <a onClick={}><i className="fa fa-comment" aria-hidden="true"/>SEND MESSAGE</a>
      </div>
      */}
      </div>
    );
  };

}

export default AddFriend;