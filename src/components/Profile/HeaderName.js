import React from "react";

const HeaderP = ({ userOther, profileOther, username, profile, toggleDescriptionMeModal, description, isOtherProfile }) => {
  let name, shortbio;
  if(profileOther) {
    name = profileOther.name;
    shortbio = profileOther.shortbio;
  }
  
  if(!name && userOther){
    name = userOther.username
  }

  return (
    <div className="name-description">
      <div className="name">
        {name || username}
      </div>
      <div className="describeYou" placeholder="Describe who you are, what you like, hobbies…">
        {shortbio || '' }
      </div>
      {isOtherProfile?
      null:<div onClick={toggleDescriptionMeModal} className="editButton">
        <i className="fa fa-pencil"/>
      </div>}
    </div>
  );
};

export default HeaderP;
