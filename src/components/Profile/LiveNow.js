import React, {Component} from 'react';
// import {Link} from 'react-router-dom';
import Tips from '../Streaming/Tips';
import AgeVerificationMessage from "../AgeVerificationMessage";
import TextField from 'material-ui/TextField';
import {subscribeToLiveChanges} from '../../libs/socket';
import VideoStream from "../Streaming/VideoStream";
import VideoPlayer from "../Streaming/VideoPlayer";
import profilePicture from "../../assets/images/woman_profile_icon.png";

class LiveNow extends Component {

  state = {
    newGoal: false,
    goalValue: 100,
    goalText: '',
    errorGoalValue: false,
    errorGoalText: false
  };

  componentDidMount() {
    subscribeToLiveChanges((err, room, roomId) => {
      if (err) return;
      this.props.getGoals(roomId);
      // TODO show message of current goal
    });
    const { isOtherProfile } = this.props;
    if (!isOtherProfile) {
      window.addEventListener('beforeunload', this.onBeforeUnload);
      window.addEventListener('unload', this.onUnload);
    }
  }

  componentWillUnmount() {
    const { isOtherProfile, deleteRoom } = this.props;
    console.log('LiveNow.componentWillUnmount isOtherProfile', isOtherProfile);
    if (!isOtherProfile) {
      window.removeEventListener('beforeunload', this.onBeforeUnload);
      window.removeEventListener('unload', this.onUnload);
      deleteRoom();
    }
  }

  onBeforeUnload = event => {
    const message = "Are you sure you want to leave? The stream will end.";
    event.returnValue = message;
    return message;
  };

  onUnload = () => {
    const { isOtherProfile, deleteRoom } = this.props;
    console.log('LiveNow.onUnload isOtherProfile', isOtherProfile);
    if (!isOtherProfile) {
      deleteRoom();
    }
  };

  renderGoal = goal => {
    if (goal === undefined) {
      return '';
    }
    return `${goal.description} ${goal.tips_current || 0}/${goal.tips_goal || goal.tokens}`;
  };

  toggleGoal = () => this.setState(prevState => ({
    newGoal: !prevState.newGoal
  }));

  onChangeDescription = (event, value) => {
    this.setState({
      goalText: value,
      errorGoalText: value.length < 1
    });
  };

  sendTips = (goalId) => {
    return (tip) => {
      return () => {
        this.props.sendTips(this.props.roomId, goalId, tip);
      }
    }
  };

  onChangeAmount = (event, value) => {
    this.setState({
      goalValue: value,
      errorGoalValue: value < 1
    })
  };

  addGoal = async () => {
    const { addGoal } = this.props;
    const {goalText, goalValue, errorGoalText, errorGoalValue} = this.state;
    if (errorGoalText || errorGoalValue) {
      return;
    }
    if (goalText.length < 1) {
      this.setState({errorGoalText: true});
      return;
    }

    await addGoal(this.props.roomId, goalText, goalValue);
    this.setState({goalText: '', goalValue: 100});
    this.toggleGoal();
  };

  nextGoal = (currentGoalId, nextGoalId) => () => {
    this.props.nextGoal(this.props.roomId, currentGoalId, nextGoalId);
  };

  render() {
    const {
      roomId, username, tags, goals, isOtherProfile, profileImage, streamingKey, broadcastKey,
      updateRoomThumbnail, deleteRoom, ageVerified,
    } = this.props;
    const {
      goalText,
      goalValue,
      newGoal,
      errorGoalText,
      errorGoalValue
    } = this.state;
    const currentGoal = goals.find(goal => goal.status);
    const nextGoal = currentGoal ? goals.find(goal => currentGoal.index + 1 === goal.index) : false;
    return (
      <div className="rectangleLiveNow">
        {!isOtherProfile && <AgeVerificationMessage ageVerified={ageVerified} />}
        <div className="user-live">
          <div className="photo-live-content">
            <img src={profileImage ? profileImage : profilePicture} className="photo-live" alt={username}/>
          </div>
          <div className="name-live">
            {username}
            <div className="live-now">
              <div className="circle-live"/>
              Live now
            </div>
          </div>
        </div>
        {isOtherProfile && <Tips sendTips={this.sendTips(currentGoal.id)}/>}
        <div className="live-description">
          <div className="tags">{tags.map((tag, i) => <span key={tag.id || i}>
              &nbsp;#{tag.title || tag}
            </span>)}</div>
          {goals.length > 0 && <div
            className="enter-goal"
            style={{
              fontWeight: 'bold'
            }}>Current goal:</div>}
          <div className="goal">
            {goals.length > 0 && this.renderGoal(currentGoal)}
          </div>
          {goals.length > 0 && nextGoal && <div className="enter-goal">Next goal:</div>}

          {goals.length > 0 && nextGoal && <div className="goal">
            {this.renderGoal(nextGoal)}
            {!isOtherProfile && nextGoal &&
            <button className="select-goal" onClick={this.nextGoal(currentGoal.id, nextGoal.id)}>select</button>}
          </div>}

          {goals.length > 0 && !newGoal && !isOtherProfile && <i
            className="fa fa-plus-square"
            aria-hidden="true"
            title="add goal"
            onClick={this.toggleGoal}/>}
        </div>
        {newGoal && !isOtherProfile && <div className="new-goal">
          <span className="text">Add goal:</span>
          <div className="add-goal">
            <TextField
              id="description"
              hintText="Describe your goal"
              errorText={errorGoalText && 'Required'}
              floatingLabelText="Description"
              value={goalText}
              onChange={this.onChangeDescription}
            />
            <TextField
              floatingLabelText="Tokens"
              errorText={errorGoalValue && 'Required'}
              type="number"
              value={goalValue}
              style={{
                width: 100
              }}
              min={1}
              onChange={this.onChangeAmount}
            />
            <div className="add-button">
              <i className="fa fa-check-square" aria-hidden="true" onClick={this.addGoal}/>
              <i className="fa fa-window-close" aria-hidden="true" onClick={this.toggleGoal}/>
            </div>
          </div>
        </div>}
        <div className="live-video">
          {!isOtherProfile && (
            <div className="preview">
              <div>CAMERA PREVIEW</div>
            </div>
          )}
          {isOtherProfile ?
            (streamingKey && <VideoPlayer streamingKey={streamingKey}/>) :
            <VideoStream
              broadcastKey={broadcastKey}
              updateRoomThumbnail={updateRoomThumbnail(roomId)}
              endStream={deleteRoom}
            />
          }
        </div>
        <div className="info-video">
          {/* <div className="viewers-video">
            <i className="fa fa-eye fa-lg" aria-hidden={true}/>
            0
          </div>
          <div className="likes-video">
            <i className="fa fa-heart" aria-hidden={true}/>
            0 likes
          </div>
          <div className="report-video">Report abuse</div>
          <div className="control-video">
            <Link to={`/streaming/${username}`}>
              <i className="fa fa-columns" aria-hidden={true}/>
            </Link>
          </div> */}
        </div>
      </div>
    );
  }
}

export default LiveNow;
