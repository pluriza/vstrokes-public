import React from 'react';

const Gifts = ({giftsList, isOtherProfile}) => {
  return (
    <div className="rectangleGifts">
      <div className="rectangleText">Gifts</div>
      {isOtherProfile
        ? <div className="contentGift">
            {giftsList.map((c, i) => <div className="gift-box" key={i}>
              <img src={c.image} className="gift-image" alt="Here goes a gif"/>
              <div className="gift-description">
                <div className="gift-name">
                  {c.name}
                </div>
                <div className="gift-strokes">
                  {c.strokes}
                  strokes
                </div>
                <div className="gift-text">
                  {c.text}
                </div>
                <div className="gift-date">
                  {c.date}
                </div>
              </div>
            </div>)}
            <div className="sendAGift">
              <lik>Send a gift</lik>
            </div>
          </div>
        : <div className="noGifts">
          <div className="message">Nothing to show
          </div>
        </div>}
    </div>
  );
};

export default Gifts;
