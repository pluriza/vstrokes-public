import React, {Component} from "react";
import TextField from 'material-ui/TextField';

class FormStrokeMe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tip: '',
            error: false
        }
        this.send = this
            .send
            .bind(this);
        this.onChange = this
            .onChange
            .bind(this);
        this.toggleModals = this
            .toggleModals
            .bind(this);

        this.styles = {
            color: 'red'
        };

    }

    send() {
        if (this.state.tip <= this.props.count) {
            this
                .props
                .toggleStrokeMeModal();
            let countlNew = this.props.count - this.state.tip;
            this
                .props
                .saveCountStrokes(countlNew);
            this.setState({error: false})
        } else {
            this.setState({error: true})
        }
    }

    onChange (e, value) {
        this.setState({tip: value});
    }

    toggleModals() {
        this
            .props
            .toggleStrokeMeModal();
        this
            .props
            .toggleGetStrokesModal();
    }

    render() {
        return (
            <form>
                <div className="balance">Your current balance:<span>{this.props.count}
                        strokes</span>
                </div>
                <div className="message">Also send me a private note:
                    <textarea rows={6} className="input"/>
                </div>
                <TextField
                    style={{width:150}}
                        type="number"
                        min="0"
                        pattern="[0-9]*"
                        max="9999"
                        hintText="# of strokes"
                        floatingLabelText="Tips"
                        value={this.state.tip}
                        onChange={this.onChange}/> 
                        {this.state.error
                        ? <span style={this.styles}>Tip value error</span>
                        : ''}
                <div className="buy" onClick={this.toggleModals}>GET MORE STROKES</div>
                <button className="send-button" type="button" onClick={this.send}>SEND STROKES NOW</button>
            </form>
        );
    }
}

export default FormStrokeMe;