import React from "react";
import Modal from "react-modal";
import Form from "./FormStrokeMe";

const StrokeMe = ({isStrokeMeModalOpen, toggleStrokeMeModal, toggleGetStrokesModal, count, total, saveCountStrokes,}) => {
  const onRequestClose = () => toggleStrokeMeModal();
  
  return (
    <Modal
      className="modalStrokes"
      isOpen={isStrokeMeModalOpen}
      contentLabel="Login-Modal"
      onRequestClose={onRequestClose}>
      <div className="closeModal" onClick={onRequestClose}> &times; </div>
      <div className="form-container">
        <h1 className="form-title-label">STROKE ME (Send me a tip)</h1>
        <Form
          toggleStrokeMeModal={toggleStrokeMeModal}
          toggleGetStrokesModal={toggleGetStrokesModal}
          count={count}
          total={total}
          saveCountStrokes={saveCountStrokes}
        />
      </div>
    </Modal>
  );
};

export default StrokeMe;
