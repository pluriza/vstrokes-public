import React from "react";
//import moment from "moment";

const NoPost = () => {

  return (
    <div className="rectangleNoPost">
        <div className="rectangleText">NO RECENT POSTS</div>
        <div className="separator"></div>
        <div className="textArea">
            <div className="text">You have not posted anything recently. </div>
        </div>
    </div>
  );
};

export default NoPost;
