import React, {Component} from "react";
import { Link } from 'react-router-dom';
import {path} from "ramda";
import HeaderP from "../../containers/Profile/HeaderPContainer";
import LeftSideProfile from "./LeftSideProfile";
import Feed from "../../containers/Profile/FeedContainer";

import StrokeMe from "../../containers/Profile/StrokeMe/StrokeMeContainer";
import ModalDescription from "../../containers/Profile/ModalDescriptionContainer";
import Albums from "../../containers/Profile/AlbumsContainer";
import {subscribeToLiveEnding} from '../../libs/socket';

import profileIcon from "../../assets/images/woman_profile_icon.png";

const getConfirmationToken = path(["params", "confirmationToken"]);
const getUsername = path(["params", "username"]);

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      posts: [],
      profile: {},
      isOtherProfile: props.match.path !== "/profile",
      profileImage: null,
      profileOther: null,
      userOther: null,
      broadcastStatus: null,
      isShowAlbum: false,
      myPath: props.match.url,
      follows: [],
      isFollowedByUser: false,
    };
    this.upProfile = this.upProfile.bind(this);
    this.toogleAlbums = this.toogleAlbums.bind(this);
  }

  openStrokeMeModal = e => {
    e.preventDefault();
    this.props.toggleStrokeMeModal();
  };

  toogleAlbums = e => {
    e.preventDefault();
    this.setState({
      isShowAlbum: !this.state.isShowAlbum
    });
  };

  componentWillReceiveProps(nextProps) {
    console.log('Profile.cwrp', nextProps, this.state);
    if (nextProps.match.url !== this.state.myPath) {
      window.location.reload();
      // let newState = Object.assign({}, this.state);
      // // make necessary changes to the nextState Object by calling
      // // functions which would change the rendering of the current page
      // this.setState(newState);
    }
  }

  componentDidMount() {
    subscribeToLiveEnding((err, room) => {
      if (err) return;
      console.log('Streaming live ended', room);
      window.location.reload();
    });
    const {
      match,
      confirmUser,
      getUserInfo,
      isLogged,
      history,
      getUserProfile,
      getRoom,
      restoreRoomData,
      getUserImageProfile,
      getUserImageCover,
    } = this.props;
    const confirmationToken = getConfirmationToken(match);

    if (confirmationToken) confirmUser(confirmationToken);
    const username = getUsername(match);

    if (match.path === "/profile") {
      return getUserProfile().then(data => {
        const user = data.value.entities.users[data.value.result];  // What even?

        if (user.broadcastStatus) {
          getRoom(user.username);
        } else {
          restoreRoomData();
        }
        this.setState({
          profileOther: data.value.profile,
          broadcastStatus: user.broadcastStatus,
          userOther: user,
          follows: user.follows,
          isFollowedByUser: false,
        });
        getUserImageProfile();
        getUserImageCover();
        this.props.getPicturesAll(this.props.match.path === "/profile");
      });
    }

    if (username === this.props.username) return history.push("/profile");

    if (username) {
      return getUserInfo(username).then(data => {
        if (this.state.isOtherProfile) {
          this.props.getPostsUser(username);
          this.props.getUserOtherImageProfile(username);
          this.props.getUserOtherImageCover(username);
          this.props.getPicturesAll(this.props.match.path === "/profile");
        }

        const {user, profile, posts} = data.value;

        if (user.broadcastStatus) {
          getRoom(username);
        } else {
          restoreRoomData();
        }
        this.setState({
          profileOther: profile,
          userOther: user,
          broadcastStatus: user.broadcastStatus,
          follows: user.follows,
          isFollowedByUser: user.isFollowedByUser,
        });
        if (profile === null) {
          return this.setState({
            ...this.state,
            posts: posts || [],
            name: user.username
          });
        }
        const {name} = profile;
        this.setState({
          name,
          posts: posts || [],
          profile
        });
      });
    }

    if (!isLogged) return history.push("/");

  }

  async upProfile(shortbio) {
    const data = await this.props.updateProfile(shortbio);
    this.setState({
      profileOther: data.value,
    });
    this.props.history.push("/profile")
  }

  onFollow = () => {
    const {_userId, history, match, onFollowClick, getUserInfo} = this.props;
    const username = getUsername(match);
    onFollowClick(_userId, history).then(result => {
      if (result) {
        getUserInfo(username).then(({ value: { user: { isFollowedByUser } } }) => {
          this.setState({ isFollowedByUser });
        });
      }
    });
  };

  onUnfollow = () => {
    const {_userId, match, onUnfollowClick, getUserInfo} = this.props;
    const username = getUsername(match);
    onUnfollowClick(_userId).then(result => {
      if (result) {
        getUserInfo(username).then(({ value: { user: { isFollowedByUser } } }) => {
          this.setState({ isFollowedByUser });
        });
      }
    });
  };

  render() {
    const {
      props: {username, profile, chat, loadingEntities},
      state: {name, isOtherProfile, posts}
    } = this;

    if (loadingEntities) return null;

    const displayName = name === "" || name === undefined ? username : name;

    return (
      <div className='profile'>
        <HeaderP
          userOther={this.state.userOther}
          description={this.props.description}
          profileOther={this.state.profileOther}
          username={displayName}
          profile={profile}
          profileImage={this.props.profileImage}
          profileImageLoading={this.props.profileImageLoading}
          coverImage={this.props.coverImage}
          coverImageLoading={this.props.coverImageLoading}
          isOtherProfile={isOtherProfile}
        />
        {
          !this.state.isShowAlbum ?
            <div className='profile-content'>
              <LeftSideProfile
                usernameProfile={username}
                profileImage={this.props.profileImage}
                userOther={this.state.userOther}
                broadcastStatus={this.state.broadcastStatus}
                username={displayName}
                isOtherProfile={isOtherProfile}
                profileOther={this.state.profileOther}
                toogleAlbums={this.toogleAlbums}
                isMe={this.props.match.path === "/profile"}
                picturesAll={this.props.picturesAll}
                toggleGalleryModal={this.props.toggleGalleryModal}
                chat={chat}
                isFollowedByUser={this.state.isFollowedByUser}
                onFollow={this.onFollow}
                onUnfollow={this.onUnfollow}
              />
              <Feed
                profileImage={this.props.profileImage}
                postsUser={this.props.postsUser}
                isOtherProfile={isOtherProfile}
                userPosts={posts}
                openStrokeMeModal={this.openStrokeMeModal}
              />
              <StrokeMe/>
              <ModalDescription
                profileOther={this.state.profileOther}
                upProfile={this.upProfile}
              />
            </div> :
            <Albums
              isOtherProfile={isOtherProfile}
              toogleAlbums={this.toogleAlbums} isMe={this.props.match.path === "/profile"}
              toggleGalleryModal={this.props.toggleGalleryModal}
            />
        }
        <div className="followers">
          <div className="followers-title">
            Users I follow
          </div>
          <ul>
            {this.state.follows.map(({username, profilePicture}) => (
              <li className='follower' key={username}>
                <img className='follower-img' src={profilePicture || profileIcon} alt="username"/>
                <Link className='follower-user' to={`/${username}`}>{username}</Link>
              </li>
            ))}
            {this.state.follows.length < 1 && (
              <li className="follower empty">
                No users found
              </li>
            )}
          </ul>
        </div>
        {/*

            <li>A</li>
            <li>B</li>
            <li>A</li>
            <li>B</li>
            <li>A</li>
            <li>B</li>
            <li>A</li>
            <li>B</li>
            <li>A</li>
            <li>B</li>
          </ul>
        </div>
        */}
      </div>
    );
  }
}

export default Profile;
