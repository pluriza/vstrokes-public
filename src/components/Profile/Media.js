import React from 'react';
import Gallery from "../../containers/Profile/GalleryContainer";

const Media = ({mediaList, isOtherProfile, toogleAlbums, picturesAll, toggleGalleryModal}) => {
  return (
    <div className="rectangleMedia">
      <div className="rectangleText">Photos/Videos
        <button type="button" className="album-button" onClick={toogleAlbums}>
          <i className="fa fa-object-group fa-lg" aria-hidden="true" title="Go to albums"></i>
        </button>
      </div>
        {picturesAll.length > 0
        ? <div className="contentMedia">
            {picturesAll.slice(0, 9).map((d, i) => {
                return (
                  <div className="media-box" key={i} onClick={() => toggleGalleryModal(d.id)}>
                  <img src={d.uri} className="media-image" alt="Here goes a media preview"/>
                </div>
                );
              })
            }
          </div>
        : <div className="noPhoto">
          <div className="message">Nothing to show
          </div>
        </div>}
        <Gallery pictures={picturesAll} />
    </div>
  );
};

export default Media;
