import React from "react";
import Modal from "react-modal";
import logo from "../assets/images/Logo.png";

const Terms = ({isTermsModalOpen, toggleTermsModal}) => {
  const onClickAgree = e => {
    e.preventDefault();
    toggleTermsModal();
  };

  const onClickExit = e => {
    e.preventDefault();
    window.location = "https://www.google.com";
  };
  return (
    <Modal
      className="modalTerms"
      isOpen={isTermsModalOpen}
      contentLabel="Terms-Modal">
      <div className="headerTerms">
        <div className="logoContainer">
          <img className="logo" src={logo} alt="Vstrokes"/>
        </div>
        <div className="titleTerms">
          YOU MUST BE OVER 18 AND AGREE TO THE TERMS BELOW BEFORE CONTINUING:
        </div>
      </div>
      <div className="textContent">
        TO ENTER, YOU MUST BE AT LEAST 18 AND AGREE WITH ALL OF THE TERMS BELOW{" "}
        <br/>
        <br/>
        This website contains sexually explicit material in the form of information,
        links, commentary, videos, photos, and opinions, (collectively, the "Sexually
        Explicit Material"). You must be at least 18 years of age to access this site,
        OR have attained the age of majority in the country, and in each and every
        jurisdiction and/or community from which you are accessing this site. If you are
        not 18, or have not yet attained the age of majority, you may not enter this
        site. Further, if Sexually Explicit Material offends you, you may not enter this
        site.<br/>
        <br/>
        Permission to enter this site is only granted to those consenting adults who
        affirm under oath and subject to penalties of perjury under title 28 U.S.C. §
        1746 and other applicable statutes and laws that the following statements are
        all true and correct:<br/>
        <br/>
        I am an adult who has attained the age of majority in the jurisdiction from
        which I am accessing this site;<br/>
        <br/>
        I will not expose any of the material on this site to minors or to others who
        might find it offensive;<br/>
        <br/>
        I have voluntarily chosen to use this site, and any content I access is for my
        own personal use only;<br/>
        <br/>
        It is my express wish to view and/or receive the Sexually Explicit Material
        offered from and within this site;<br/>
        <br/>
        I beleive that sexual acts which consenting adults engage in are neither
        offensive nor obscene;<br/>
        <br/>
        I have confirmed that accessing, watching, listening to, downloading, sharing,
        and/or creating Sexually Explicit Material does not violate the standards of any
        community, village, town, city, state, department, province, territory, or
        country where I will be accessing or contributing said Sexually Explicit
        Material;<br/>
        <br/>
        I am solely responsible for any false disclosures or legal ramifications of
        viewing, reading downloading, uploading, or creating any material appearing on
        this site, and I understand that providing a false declaration under the
        penalties of perjury is a criminal offense;<br/>
        <br/>
        I agree that neither this website nor its owners or affiliates will be held
        responsible for any legal ramifications arising from any fraudulent entry into
        or use of this website;
        <br/>
        <br/>
        I understand and agree that my entry into and use of this website is governed by
        the site’s Terms and Conditions which I have reviewed in entirety and accepted,
        and I agree to be bound by them;<br/>
        <br/>
        The content found on this site is intended to be used by consenting adults in a
        sexual context for education, commentary, entertainment, and/or aid;<br/>
        <br/>
        Everyone accessing this site with you is over the age of 18 and/or has attained
        the age of majority in their local jurisdiction, and has consented to being
        photographed and/or filmed, and furthermore, all aforementioned persons believe
        it is within their rights to engage in sexually explicit acts, and as consenting
        adults, watch others do the same;<br/>
        <br/>
        I agree that this Warning and Affirmation constitutes a legally binding
        agreement between me and the website and is governed by the Electronic
        Signatures in Global and National Commerce Act (commonly known as the “E-Sign
        Act”), 15 U.S.C. § 7000, et seq. By choosing to click below and enter the site,
        I am indicating my agreement to be bound by the above and the Terms and
        Conditions of the site, and I affirmatively adopt the signature line below as my
        signature and the manifestation of my consent to be bound by these terms.<br/>
        <br/>
        If you disagree with any of the terms laid out above, you do NOT have permission
        to enter and should leave immediately by clicking “EXIT” below. On the other
        hand, if you are in agreement with all of the terms above and would like to
        enter the site, simply click on “I AGREE” below.<br/>
        <br/>
      </div>
      <div className="message">
        THIS SITE ACTIVELY COOPERATES WITH LAW ENFORCEMENT IN ALL INSTANCES OF SUSPECTED
        ILLEGAL USE OF THE SERVICE, ESPECIALLY IN THE CASE OF UNDERAGE USAGE OF THE
        SERVICE.
      </div>

      <div className="buttons">
        <button onClick={onClickExit} className="exit">
          EXIT
        </button>
        <button onClick={onClickAgree} className="agree">
          I AGREE
        </button>
      </div>

      <hr className="modal-separator"/>
    </Modal>
  );
};

export default Terms;
