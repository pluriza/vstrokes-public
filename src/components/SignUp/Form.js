import React, {Component} from 'react';
import TextField from 'material-ui/TextField';

class SignUpForm extends Component {

  state = {
    username: '',
    email: '',
    password: '',
    error: false,
    errorMessage: ''
  };

  componentWillReceiveProps({error, errorMessage}) {
    if (error && !this.state.error) {
      this.setState({error, errorMessage});
    }
  }

  onSignUp = e => {
    e.preventDefault();
    const {createUser} = this.props;
    const {username, email, password} = this.state;
    localStorage.setItem('vspassword', password);
    if (username.length < 4) {
      return this.setState({
        error: true,
        errorMessage: "Your username must be at least 4 characters long."
      });
    }
    if (password.length < 8) {
      return this.setState({
        error: true,
        errorMessage: "Your password must be at least 8 characters long."
      });
    }
    return createUser({username, email, password});
  };

  handleChangePassword = (event, value) => this.handleChange("password", value);
  handleChangeMail = (event, value) => this.handleChange("email", value.trim());
  handleChangeUsername = (event, value) => this.handleChange("username", value.trim());

  handleChange(name, value) {
    this.setState({[name]: value, error: false});
  }

  render() {
    const {username, email, password, error, errorMessage} = this.state;
    return (
      <div>
        <form onSubmit={this.onSignUp}>
          <TextField
            required
            style={{width: '80%'}}
            hintText="Choose a username (Minimum 4 characters)"
            floatingLabelText="Username"
            fullWidth={true}
            value={username}
            onChange={this.handleChangeUsername}
          />
          <TextField
            required
            style={{width: '80%'}}
            type="email"
            hintText="Put your email"
            floatingLabelText="E-mail"
            fullWidth={true}
            value={email}
            onChange={this.handleChangeMail}
          />
          <TextField
            required
            style={{width: '80%'}}
            type="password"
            hintText="Choose a new password (Minimum 8 characters)"
            floatingLabelText="Password"
            fullWidth={true}
            value={password}
            onChange={this.handleChangePassword}
          />
          <div style={{color: 'red', fontSize: 14}}>
            {error && errorMessage}
          </div>
          <button type="submit" className="signup-form-button">
            SIGN UP
          </button>
        </form>
        <div className="signup-to-login">
          Already have an account?{' '}
          <span onClick={this.props.toggleLoginModal} style={{cursor:'pointer'}} className="orange-label">
            Login
          </span>
        </div>
      </div>
    );
  }
}

export default SignUpForm;
