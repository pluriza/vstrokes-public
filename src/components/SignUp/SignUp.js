import React from 'react';
import Form from '../../containers/SignUp/FormContainer';
import screenImg from '../../assets/images/PcIcon.png';
import cameraImg from '../../assets/images/CamIcon.png';
import dialogImg from '../../assets/images/BallonsIcon.png';
import pictureImg from '../../assets/images/ImagesIcon.png';
import Footer from '../Footer/Footer';

const SignUp = () => {
  const advertisements = [
    {
      image: screenImg,
      alt: 'PC Image',
      message: 'Join The Largest Adult Social Network in the World for Free!',
    },
    {
      image: cameraImg,
      alt: 'Cam Image',
      message: 'Create your own live shows and make extra money on the side.',
    },
    {
      image: dialogImg,
      alt: 'Dialog Image',
      message:
        'Meet and interact with people just like you from all over the world',
    },
    {
      image: pictureImg,
      alt: 'Picture Image',
      message: 'Share and enjoy tons of adult photos and videos.',
    },
  ];
  return (
    <div>
      <div className="signup-page-container">
        <div>
          <h1 className="form-title-label">
            SIGN-UP FOR A FREE ACCOUNT WITH VINEGAR STROKES!
          </h1>
        </div>
        <div className="signup-left-container">
          <h2 className="create-account-orange-title">
            CREATE YOUR FREE ACCOUNT
          </h2>
          <Form />
        </div>
        <div className="signup-right-container">
          <div className="signup-right-container-title-container">
            <span className="signup-right-container-title">
              Join The Largest Adult Social Network in the World for Free!
            </span>
          </div>
          <div className="advertisements">
            {advertisements.map((ad, index) =>
              <div
                className="signup-right-container-content-container"
                key={index}
              >
                <img
                  className="signup-right-container-image"
                  src={ad.image}
                  alt={ad.alt}
                />
                <p className="signup-right-container-p">
                  {ad.message}
                </p>
              </div>
            )}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default SignUp;
