import React,{ Component } from "react";
import history from "../../libs/history";
import TextField from 'material-ui/TextField';

class LoginForm extends Component {

  state = {
    usernameInput: '',
    passwordInput: ''
  };

  handleChangeUser = (event, value) => this.onChange("usernameInput", value);
  handleChangePass = (event, value) => this.onChange("passwordInput", value);
  
  onChange = (name, value) => this.setState({ [name]: value });

  onLogin = e => {
    const {usernameInput, passwordInput} = this.state;
    e.preventDefault();
    return this.props.createSession({username: usernameInput, password: passwordInput}).then(() => {
      this.props.toggleLoginModal();
      history.push("/profile");
    });
  };

  render() {
    const {loggingFail, loginErrorMessage} = this.props;
    const {usernameInput, passwordInput} = this.state;
    return (
      <form onSubmit={this.onLogin}>
        {loggingFail && <span className="errorLogin">*{loginErrorMessage}*</span>}
        <TextField
        required
          hintText="Your username"
          floatingLabelText="Username"
          fullWidth={true}
          value={usernameInput}
          onChange={this.handleChangeUser}/>
        <TextField
        required
          hintText="Password"
          floatingLabelText="Password"
          type="password"
          fullWidth={true}
          value={passwordInput}
          onChange={this.handleChangePass}/>
        <br/>
        <button className="orange-button" type="submit">
          LOGIN
        </button>
      </form>
    );
  }
}

export default LoginForm;
