import React from "react";
import Modal from "react-modal";
import {Link} from "react-router-dom";
import Form from "../../containers/Login/FormContainer";

const Login = ({isLoginModalOpen, toggleLoginModal, toggleResetPasswordModal}) => {
  const onRequestClose = () => toggleLoginModal();
  const onGoToResetPassword = () => {
    toggleLoginModal();
    toggleResetPasswordModal();
  };
  return (
    <Modal
      className="modal"
      isOpen={isLoginModalOpen}
      contentLabel="Login-Modal"
      onRequestClose={onRequestClose}>
      <div className="closeModal" onClick={onRequestClose}> &times; </div>
      <div className="form-container">
        <h1 className="form-title-label">LOGIN TO VINEGAR STROKES</h1>
        <Form/>
        <div>
          Don’t have an account?{" "}
          <Link onClick={onRequestClose} to="/signup" className="orange-label">
            {" "}Sign-up now!
          </Link>
        </div>
        <hr className="modal-separator"/>
        <a onClick={onGoToResetPassword} className="orange-label">
          {" "}Forgot your password?
        </a>
      </div>
    </Modal>
  );
};

export default Login;
