import React from 'react';
import {Link} from 'react-router-dom';
//Component
import SocialMedia from './SocialMedia';

const Footer = () => {

  return (
    <div className="footer">
      <div className="footer-container">
        <div className="footer-col">
          <span className="footer-title">
            Lastest news
          </span>
          <span className="footer-link">
            <Link target="_blank" rel="noopener noreferrer" to="http://i.vinegarstrokes.com/">
              Vinegar Strokes Blog
            </Link>
          </span>
          {/*<span className="footer-link">
            Community
          </span>*/}
        </div>
        {/*<div className="footer-col">
          <span className="footer-title">
            Advertise & Marketing
          </span>
          <span className="footer-link">
            Advertise With Us
          </span>
          <span className="footer-link">
            Media Kit
          </span>
        </div>
        <div className="footer-col">
          <span className="footer-title">
            Customer Support
          </span>
          <span className="footer-link">
            Help Centre
          </span>
          <span className="footer-link">
            Contact Us
          </span>
          <span className="footer-link">
            FAQ
          </span>
          <span className="footer-link">
            Billing Support
          </span>
        </div>*/}
        <div className="footer-col">
          <span className="footer-title">
            Legal
          </span>
          <Link className="footer-link" to="/legal/2257">2257 Statement</Link>
          <Link className="footer-link" to="/legal/terms">Terms & Conditions</Link>
          <Link className="footer-link" to="/legal/privacy">Privacy Policy</Link>
          <Link className="footer-link" to="/legal/copyright">Copyright</Link>
          <Link className="footer-link" to="/legal/community">Community Guidelines</Link>
        </div>
        <div className="footer-col">
          <span className="footer-title">
            Contact Us
          </span>
          <span className="footer-text">
            Follow us on our social networks.
          </span>
          <SocialMedia/>
        </div>
      </div>
      <span className="footer-disclaimer">
        <Link className="footer-link" to="/legal/2257">
          18 U.S.C. 2257 Record Keeping Requirements Compliance Statement
        </Link>
        <br/>
        VinegarStrokes.com 422 Richards St. Suite 170 Vancouver, BC Canada V6B 2Z4
        <br/>
        © Copyright VinegarStrokes.com 2015 - {(new Date()).getUTCFullYear()}. All Rights Reserved.
      </span>
    </div>
  );
};

export default Footer;
