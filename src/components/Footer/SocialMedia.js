import React from 'react';
import facebook from '../../assets/images/FbIcon.png';
import twitter from '../../assets/images/TwIcon.png';
import instagram from '../../assets/images/InIcon.png';
import vimeo from '../../assets/images/VimeoIcon.png';

const SocialMedia = () => {
  return (
    <div className="footer-contact-icon-container">
      <img src={facebook} className="icon-image" alt="Facebook" />
      <img src={twitter} className="icon-image" alt="Twitter" />
      <img src={instagram} className="icon-image" alt="Instagram" />
      <img src={vimeo} className="icon-image" alt="Vimeo" />
    </div>
  );
};

export default SocialMedia;
