import React from 'react';
import { withRouter } from 'react-router-dom';
import Message2 from '../Chat/Message2';
import {subscribeToChatProfile, emitToChatProfile, emitToRoomUsernameChatProfile} from '../../libs/socket';

class Chat2 extends React.Component {

  constructor(props) {
    super(props);
    let messages;
    if (this.props.chats) {
      messages = [...this.props.chats.content];
    }
    if (this.props.inProfile) {
      this.props.clearMessages();
    }
    if (messages.length === 0) {
      messages.push({
        user: {
          image: '',
          name: '',
        },
        content: 'Welcome to Vinegar Strokes streaming page....'
      });
    }
    this.state = {
      messages: messages,
      usernameRoom: null,
    };
    this.sendChat = this.sendChat.bind(this);
    this.toggleChat = this.toggleChat.bind(this);

  }

  componentDidMount() {
    subscribeToChatProfile((err, newMessage) => {
      if (err) return;
      console.log('newMessage', newMessage);
      this.setState({
        messages: [...this.state.messages, newMessage]
      });
      this.props.getMessages({
        id: this.props.userId,
        content: this.state.messages
      })
    });
  }

  sendChat(content) {
    let data = {
      room: this.state.usernameRoom,
      content: content
    };
    emitToChatProfile(data);
  }

  sendMessage = (e) => {
    const {isLogged, history, username, userImage} = this.props;

    if (e.keyCode === 13) {
      if (e.target.value === '') {
        return;
      }
      //console.log(this.props);
      if (!isLogged) {
        history.push('/signup');
        return;
      }

      const newMessage = {
        user: {
          name: username,
          image: userImage,
        },
        content: e.target.value
      };
      this.sendChat(newMessage);
      document.getElementsByName('chatInput')[0].value = '';
    }
  };

  toggleChat() {
  }

  componentWillReceiveProps(nextProps) {
    // if((nextProps.userOther && nextProps.usernameProfile)|| (nextProps.userOther===null && nextProps.usernameProfile)) {
    const { chats } = nextProps;
    if (chats) {
      const messages = [...chats.content];
      if (messages.length === 0) {
        messages.push({
          user: {
            image: '',
            name: '',
          },
          content: 'Welcome to Vinegar Strokes streaming page....'
        });
      }
      this.setState({messages});
    }

    if (nextProps.userOther) {
      const {userOther: {username}} = nextProps;
      console.debug('USERNAME USEROTHER', username);
      this.setState({
        usernameRoom: username
      });
      //if (this.props.isOtherProfile) {
        emitToRoomUsernameChatProfile(username)
      //}
      //console.log('USERNAME ROOM', username)
      //emitToRoomUsernameChatProfile(username)
    }
    else if (nextProps.usernameProfile) {
      const {usernameProfile} = nextProps;
      console.debug('USERNAME USER', usernameProfile);
      this.setState({
        usernameRoom: usernameProfile
      });

      if (!this.props.isOtherProfile) {
        emitToRoomUsernameChatProfile(usernameProfile)
      }
      //console.log('USERNAME ROOM', usernameProfile)
      //emitToRoomUsernameChatProfile(usernameProfile)
    }

    // } 

    // console.debug('NEXT PROPS', nextProps);
    // console.debug('PROFILE IMAGE', this.props.profileImage)
  }

  // componentDidUpdate(prevProps, prevState) {
  //   console.log('isOtherProfile', this.props.isOtherProfile)
  //   console.log('USERNAME ROOM', this.state.usernameRoom)
  //   if(this.props.isOtherProfile) {
  //     emitToRoomUsernameChatProfile(this.state.usernameRoom)
  //   } 
  // }

  renderMessages = messages => messages.map((m, ix) => <Message2 key={ix} {...m}/>);

  render() {
    const {inProfile, isStreamOnline} = this.props;
    const style = {};
    if (inProfile) {
      style.height = 400;
    }
    return (
      <div className="rectangleChat">
        <div className="rectangleText">Live Chat ({isStreamOnline ? 'ON' : 'OFF'})</div>
        {/* {isStreamOnline ? <i className="fa fa-cog fa-fw" aria-hidden="true"/> : ''}
        {isStreamOnline ? <i className="fa fa-chevron-up" aria-hidden="true" onClick={this.toggleChat}/> : ''} */}
        <div className="separator"/>
        {isStreamOnline !== true ?
          <div className="noChat">
            <div className="message">It´s available when you start a live show</div>
          </div>
          :
          <div className="contentChat" style={style}>
            <div className="chat">
              <div>{this.renderMessages(this.state.messages)}</div>
            </div>
            <input
              type="text"
              name="chatInput"
              className="chatInput"
              placeholder="Escribe un comentario..."
              onKeyUp={this.sendMessage}
            />
          </div>
        }
        <div className="config" id="config"/>
      </div>
    );
  }
}

export default withRouter(Chat2);