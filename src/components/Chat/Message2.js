import React from 'react';
const Message2 = ({user, content, error}) => (
    
  <div className="message">
    {user.image && <img className="image" src={user.image} alt="User Profile"/>}
    <div className="body">
      {user.name && <div className="user">{user.name}</div>}
      <div className="content" style={error?{color: 'red'}:{}}>{content}</div>
    </div>
  </div>
);

export default Message2;