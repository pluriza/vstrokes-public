import React from 'react';
import ReactChatView from '../../../node_modules/react-chatview'
import Message from '../Chat/Message';

import img1 from '../../assets/images/camImages/asiangirl1.jpg';

export default class Chat extends React.Component {
  
  constructor (props) {
    super(props);
    this.state = {
      messages: [
        {
          user: {},
          content: 'Welcome to Vstroke streaming page....'
        }
      ]
    };
  }

  loadMoreHistory = () => {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  sendMessage = (e) => {
    if(e.keyCode === 13){
      if(e.target.value === ''){
        return ;
      }
      const newMessage = {
        user: {
          name: this.props.username,
          image: img1, // TODO this.props.profile.image ?
        },
        content: e.target.value
      };
      this.setState(({messages}) => {
        return {
          messages: [...messages, newMessage]
        };
      })
      document.getElementsByName('chatInput')[0].value = '';
    }
  }

  renderMessages = messages => messages.map((m, ix) => <Message key={ix} content={m.content} user={m.user}/>);

  render(){
    return(
    <div className="rectangleChat">
      <div className="rectangleText">Live Chat (On)</div>
      <i className="fa fa-cog fa-fw" aria-hidden="true"></i>
      <i className="fa fa-chevron-up" aria-hidden="true"></i>
      <div className="separator"></div>
      <div className="contentChat">
        <ReactChatView 
          className="chat"
          flipped={false}
          scrollLoadThreshold={0}
          onInfiniteLoad={this.loadMoreHistory}
        >
          {this.renderMessages(this.state.messages)}
        </ReactChatView>
        <input
          type="text"
          name="chatInput"
          className="chatInput"
          placeholder="Escribe un comentario..."
          onKeyUp={this.sendMessage.bind(this)}
        />
      </div>
      <div className="config" id="config"></div>
  </div>);
  }
}
