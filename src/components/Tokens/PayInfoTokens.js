import React, {Component} from "react";
import Modal from "react-modal";
// import AutoComplete from 'material-ui/AutoComplete';
// import TextField from 'material-ui/TextField';
// import history from "../../libs/history";
// import countries from '../Countries'

const {
  REACT_APP_CCBILL_FLEXFORM_URL: FLEXFORM_URL,
  REACT_APP_CCBILL_CLIENT_SUBACCOUNT: CLIENT_SUBACCOUNT
} = process.env;

const BuyButton = ({loading, className, children, url}) => (
  loading ?
    <div className={className}>{children}</div> :
    <a className={className} href={url}>{children}</a>
);

class PayInfoTokens extends Component {

  state = {
    name: '',
    email: '',
    country: '',
  };

  handleChangeName = (event, value) => this.handleChange("name", value);
  handleChangeMail = (event, value) => this.handleChange("email", value);
  handleChangeCountry = (event, value) => this.handleChange("country", value);

  handleChange(name, value) {
    this.setState({[name]: value});
  }

  onRequestClose = () => this.props.togglePayInfoToken();

  onCancel = () => {
    const {toggleGetStrokesModal, togglePayInfoToken} = this.props;
    toggleGetStrokesModal();
    togglePayInfoToken();
  };

  /*redirect = (e) => {
    const {count, total, buyTokens, togglePayInfoToken} = this.props;
    e.preventDefault();
    this.props.buyTokens(count, total.toFixed(2), 'CCBILLDATA')
    this.props.togglePayInfoToken();
    history.push("/tokens");
  };*/

  render() {
    const {isPayInfoToken, count, total, digest, loading, vsToken} = this.props;
    const price = total.toFixed(2);
    const params = [
      ['clientSubacc', CLIENT_SUBACCOUNT],
      ['initialPrice', price],
      ['initialPeriod', 2],
      ['currencyCode', 840],
      ['formDigest', digest],
      ['vsToken', vsToken]
    ];
    const url = `${FLEXFORM_URL}?${params.map(item => item.join('=')).join('&')}`;
    return (
      <Modal
        className="modalPayInfoTokens"
        isOpen={isPayInfoToken}
        contentLabel="Login-Modal"
        onRequestClose={this.onRequestClose}>
        <div className="closeModal" onClick={this.onRequestClose}>
          &times;
        </div>
        <div className="form-container">
          <div className="form-title-label">Payment Info</div>
          <div className="info">
            {count} tokens for ${price} total
          </div>
          <div className="payInfoForm">
            {/*<div className="formContainer">
              <TextField
                name='username'
                required
                style={{
                  width: 250
                }}
                hintText="Put your name"
                floatingLabelText="Name"
                fullWidth={true}
                maxLength={100}
                value={this.state.name}
                onChange={this.handleChangeName}
              />
              <TextField
                name='useremail'
                required
                style={{
                  width: 250,
                  display: 'block'
                }}
                type="email"
                hintText="Put your email"
                floatingLabelText="E-mail"
                fullWidth={true}
                value={this.state.email}
                onChange={this.handleChangeMail}
              />
              <AutoComplete
                name='usercountry'
                required
                floatingLabelText="Country"
                filter={AutoComplete.fuzzyFilter}
                dataSource={countries}
                maxSearchResults={5}
                value={this.state.country}
                onChange={this.handleChangeCountry}
              />
            </div>*/}
            <BuyButton className='orange-button right' loading={loading} url={url}>PAY WITH CCBILL</BuyButton>
            <button type="button" className="orange-button" onClick={this.onCancel}>Cancel</button>
          </div>
        </div>
      </Modal>
    );
  }
}

export default PayInfoTokens;