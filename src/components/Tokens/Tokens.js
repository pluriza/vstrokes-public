import React, { Component } from "react";
import moment from 'moment';
import { TextField, RaisedButton } from 'material-ui';
import Footer from "../Footer/Footer";

const {
  REACT_APP_CCBILL_FLEXFORM_URL: FLEXFORM_URL,
  REACT_APP_CCBILL_CLIENT_SUBACCOUNT: CLIENT_SUBACCOUNT
} = process.env;

class Tokens extends Component {

  state = {
    tokens: 100,
  };

  componentDidMount() {
    this.props.getTokens();
    this.props.getPurchases();
  }

  componentWillReceiveProps(nextProps) {
    console.log(this.props, nextProps);
    if (this.props.withdrawalPending && !nextProps.withdrawalPending) {
      window.location.reload();
    }
  }

  componentWillUnmount() {
    this.props.clearPurchases();
  }

  renderPurchase = purchase => {
    const {
      id, status, digest, userIP, tokenCount, value, transactionId, createdAt, balance,
      type,
    } = purchase;
    const price = Number(value).toFixed(2);
    const params = [
      ['clientSubacc', CLIENT_SUBACCOUNT],
      ['initialPrice', price],
      ['initialPeriod', 2],
      ['currencyCode', 840],
      ['formDigest', digest],
      ['vsToken', id]
    ];
    const url = `${FLEXFORM_URL}?${params.map(item => item.join('=')).join('&')}`;
    return (
      <tr key={id}>
        <td title={createdAt} >{moment(createdAt).fromNow()}</td>
        {/*
        <td title={timestamp} >{timestamp && moment(timestamp).fromNow()}</td>
        <td title={updatedAt} >{moment(updatedAt).fromNow()}</td>
        */}
        <td>
          {type === 'payment' ?
            <span>
              Order {transactionId} for ${value} from IP "{userIP}" with
              status {status === 'pending' ?
                <a href={url} title='If you have not paid, click here to pay with CCBill'>{status}</a>
                :
                status
              }.
            </span>
            :
            <span>Payment request for ${value} from IP {userIP} with status {status}.</span>
          }
        </td>
        <td>{tokenCount}</td>
        <td>{balance}</td>
      </tr>
    );
  };

  renderPurchases() {
    const { purchases } = this.props;
    if (purchases.length > 0) {
      return purchases.map(this.renderPurchase);
    } else {
      return (
        <tr style={{textAlign: 'center'}}>
          <td colSpan={8}>
            You haven't bought anything yet.
          </td>
        </tr>
      );
    }
  }

  handleTokenSelection = ({ target: { value } }) => {
    value = Number(value);
    if (value < 100-9) {
      return;
    }
    if (value > this.props.withdrawable+6) {
      return;
    }
    this.setState({ tokens: value })
  };

  requestPayment = () => this.props.requestPayment(this.state.tokens);

  render() {
    return (
      <div>
        <div className="tokens">
          <div className="myStrokes">
            My Strokes
            <div className="strokes">{this.props.count} Strokes</div>
          </div>
          <div className="balance">
            Cash Balance
            <div className="cash">$ {Number(this.props.withdrawable * 0.0399).toFixed(2)}</div>
            <p style={{textAlign: 'right'}}>This amount corresponds to the cashable tokens ({this.props.withdrawable})</p>
          </div>
          {/*<div className="payment">
            <div className="paymentTitle">Add a payment method</div>
            <p><b>Credit or Debit Cards </b><br/> Vinegar Strokes accepts all major credit and debit cards.
            <br/><br/><span>+ Add a card</span>
            </p>
          </div>*/}
          <div className="payment">
            <div className="paymentTitle">Request a payment</div>
            <p>Turn those strokes you’ve earned into cash! See what you’ll need to start cashing out your tokens today.</p>
            <TextField
              type="number"
              floatingLabelText="Withdraw tokens"
              errorText={this.props.withdrawalMessage}
              value={this.state.tokens}
              onChange={this.handleTokenSelection}
              disabled={this.props.withdrawalPending}
            />
            <br />
            <RaisedButton
              secondary
              label={`Request a payment for $${Number(this.state.tokens * 0.0399).toFixed(2)}`}
              onClick={this.requestPayment}
              disabled={this.props.withdrawalPending}
            />
          </div>
          <div className="purchases">
            <div className="purchasesTitle">Strokes Statement</div>
            <table className="purchases-table">
              <thead>
              <tr>
                <th>Date</th>
                {/*
                <th>Timestamp</th>
                <th>Updated</th>
                */}
                <th>Description</th>
                <th>Tokens</th>
                <th>Balance</th>
              </tr>
              </thead>
              <tbody>{this.renderPurchases()}</tbody>
            </table>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Tokens;