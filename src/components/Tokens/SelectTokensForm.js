import React, {Component} from "react";
import Stroke from './Stroke';

const items = [
  {
    id: '1',
    cant: 100,
    price: 10.99,
    offert: 0.11
  }, {
    id: '2',
    cant: 200,
    price: 20.99,
    offert: 0.105
  }, {
    id: '3',
    cant: 500,
    price: 44.99,
    offert: 0.09
  }, {
    id: '4',
    cant: 750,
    price: 62.99,
    offert: 0.085
  }, {
    id: '5',
    cant: 1000,
    price: 79.99,
    offert: 0.08
  }
];

class SelectTokensForm extends Component {

  state = {
    count: 0,
    total: 0
  };

  handleChange = id => (event, isInputChecked) => {
    const { price, cant } = items.find(item => item.id === id);
    if (isInputChecked) {
      this.setState(prevState => ({
        count: prevState.count + cant,
        total: price + prevState.total
      }));
    } else {
      this.setState(prevState => ({
        count: prevState.count - cant,
        total: prevState.total - price
      }));
    }
  };

  redirectTo = () => {
    const {count, total} = this.state;
    if (total <= 0) {
      return;
    }
    const {
      toggleGetStrokesModal, saveBuyTokensInfo, togglePayInfoToken
    } = this.props;
    toggleGetStrokesModal();
    saveBuyTokensInfo(count, total);
    togglePayInfoToken();
    //history.push("/tokens");
  };

  render() {
    return (
      <form>
        {items.map(item => (
          <Stroke stroke={item} key={item.id} onChange={this.handleChange}/>
        ))}
        <div className="totalPrice">${this.state.total.toFixed(2)} <span>Total</span></div>
        <button
          className="orange-button"
          disabled={this.state.total <= 0}
          type="button"
          onClick={this.redirectTo}
        >
          CONFIRM
        </button>
      </form>
    );
  }
}

export default SelectTokensForm;