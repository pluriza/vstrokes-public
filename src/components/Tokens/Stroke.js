import React from "react";
import Checkbox from 'material-ui/Checkbox';

const Stroke = ({stroke, onChange}) => {
  return (
    <Checkbox
      className = "tokenOption"
      label={
        <div>
          <div className="token">{stroke.cant} strokes</div>
          <div className="price">
            <div className="value">${stroke.price}</div>
            {/*<div className="info">(aprox. ${stroke.offert}/token)</div>*/}
          </div>
        </div>
      }
      onCheck={onChange(stroke.id)}
      iconStyle={{marginTop: 'auto', marginBottom: 'auto'}}
    />
  );
};

export default Stroke;