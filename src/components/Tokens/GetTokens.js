import React from "react";
import Modal from "react-modal";
import Form from "./SelectTokensForm";

const GetTokens = ({isGetStrokesModalOpen, toggleGetStrokesModal, saveBuyTokensInfo, togglePayInfoToken }) => {
  const onRequestClose = () => toggleGetStrokesModal();
  return (
    <Modal
      className="modalGetTokens"
      isOpen={isGetStrokesModalOpen}
      contentLabel="Login-Modal"
      onRequestClose={onRequestClose}>
      <div className="closeModal" onClick={onRequestClose}> &times; </div>
      <div className="form-container">
        <h1 className="form-title-label">
          Select the package you want to purchase.
        </h1>
        <Form
          togglePayInfoToken={togglePayInfoToken}
          toggleGetStrokesModal={toggleGetStrokesModal}
          saveBuyTokensInfo={saveBuyTokensInfo}
        />
      </div>
    </Modal>
  );
};

export default GetTokens;