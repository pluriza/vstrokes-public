import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import Stream from './Stream';
import {Previous, Page, Next} from '../Paging';

class Streams extends Component {

  handleClickTag = tagId => () => {
    this.props.updateFilter('tag', tagId);
  };

  renderStream = stream => <Stream stream={stream} key={stream.username}/>;

  renderTag = tag => (
    <div className="tag" key={tag.id}>
      <span className='title' onClick={this.handleClickTag(tag.id)}>{tag.title}</span>
      <span className='middot'>&middot;</span>
    </div>
  );

  goToPage = page => () => {
    const {updateFilter} = this.props;
    updateFilter('page', page);
  };

  render() {
    const {streams, streamsCount, streamsLimit, currentPage, tags} = this.props;
    const totalPages = Math.ceil(streamsCount / streamsLimit);
    const pages = (new Array(totalPages)).fill(1);
    return (
      <div className="cams-container">
        <div className="trending-tags">
          <Link to="tags">Trending tags</Link>{tags.map(this.renderTag)}
        </div>
        <br/>
        <div>
          {streams.length > 0 ? streams.map(this.renderStream) : <div className="no-rooms">There are currently no live shows</div>}
        </div>
        <div className="moreshow-container">
          <Previous currentPage={currentPage} noMoreTitle='No more shows' goTo={this.goToPage}/>
          {pages.map((_, i) => <Page key={`page${i}`} page={i + 1} currentPage={currentPage} goTo={this.goToPage}/>)}
          <Next currentPage={currentPage} totalPages={totalPages} noMoreTitle='No more shows' goTo={this.goToPage}/>
        </div>
      </div>
    );
  };
}
export default Streams;
