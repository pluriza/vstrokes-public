import React from 'react';
import {Link} from "react-router-dom";

import img from '../../assets/images/Usergreen.png';

const Stream = ({stream}) => {

  const { Profile, username, Rooms } = stream;
  const room = Rooms[0];
  const { Albums } = Profile;

  let image = img;
  if (Albums.length > 0) {
    const { Pictures } = Albums[0];
    if (Pictures.length > 0) {
      const { uri } = Pictures[0];
      image = uri;
    }
  }

  return (
    <Link to={`/${username}`}>
      <div className="image-box">
        <img src={room.thumbnail || image} className="cam-image" alt={username} />
      </div>
    </Link>
  );
};

export default Stream;
