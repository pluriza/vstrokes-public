import React, {Component} from "react";
import Footer from "../Footer/Footer";
import TermsContent from './TermsContent';
import {Router, Route, NavLink} from 'react-router-dom';

class Terms extends Component {

    render() {
        let general = <div>
            <div className="titleSection">Terms &amp; Conditions</div>
            <div className="section">
                Welcome to VinegarStrokes.com (“Vinegar Strokes”), owned, produced and operated
                by Vinegar Strokes Incorporated. VinegarStrokes.com is a social network for
                people curious about exploring their sexuality. Unless explicitly stated
                otherwise, any current, updated, or new products and services (“Products and
                Services”), including the addition of new properties, shall be subject to these
                General Terms and Conditions of Use (“Terms and Conditions”).
                <br/><br/>
                In addition, there are a variety of special products and services offered
                through VinegarStrokes.com that have separate registration procedures and
                separate terms and conditions, terms of service, user agreements, or similar
                legal agreements. When you are using any service or product on Vinegar Strokes
                that does not have a separate legal agreement, the Terms and Conditions set
                forth here will apply. Vinegar Strokes Incorporated also may supplement the
                Terms and Conditions with posted guidelines or rules applicable to specific
                areas of Vinegar Strokes. In addition, Vinegar Strokes Incorporated may also
                offer other services from time to time that are governed by the terms of service
                of the respective service partners. Vinegar Strokes Incorporated reserves the
                right to amend these Terms and Conditions at any time.
                <br/><br/>By using VinegarStrokes.com you agree to be bound by these Terms and
                    Conditions. Because the Terms and Conditions contain legal obligations, please
                    read them carefully.
                <br/><br/>The Terms and Conditions contain a variety of provisions that are
                    generally applicable to the Products and Services and some provisions that apply
                    to particular Products and Services. You should understand that you will be
                    bound by the entire Terms and Conditions.
                <br/><br/>
                <b>ACCEPTANCE</b><br/><br/>
                By using VinegarStrokes.com, you are agreeing, without limitation or
                qualification, to be bound by, and to comply with, these Terms and Conditions
                and any other posted guidelines or rules applicable to Vinegar Strokes
                Incorporated or any Product or Service. All such guidelines and rules are hereby
                incorporated by reference into the Terms and Conditions.
                <br/><br/>
                <b>CONDITIONS AND RESTRICTIONS ON USE</b>
                <br/><br/>Use of VinegarStrokes.com is subject to compliance with these Terms
                    and Conditions. You shall be authorized to use the Products and Services for
                    personal, non-commercial use only. You acknowledge and agree that Vinegar
                    Strokes Incorporated may terminate your access to any of the Products and
                    Services should you fail to comply with the Terms and Conditions or any other
                    guidelines and rules published by Vinegar Strokes Incorporated. Any such
                    termination shall be at the sole discretion of Vinegar Strokes Incorporated and
                    may occur without prior notice, or any notice. Vinegar Strokes Incorporated
                    further reserves the right to terminate any user’s access to VinegarStrokes.com
                    or to any of the Products and Services for any conduct that Vinegar Strokes
                    Incorporated, at its sole discretion, believes is or may be directly or
                    indirectly harmful to other users, to Vinegar Strokes Incorporated, or to its
                    subsidiaries, affiliates, or business contractors, or to other third parties, or
                    for any conduct that violates any local, provincial, federal or foreign laws or
                    regulations. Vinegar Strokes Incorporated further reserves the right to
                    terminate any user’s access to VinegarStrokes.com or to any of the Products and
                    Services for any reason or for no reason at all, at the sole discretion of
                    Vinegar Strokes Incorporated, without prior notice, or any notice.
                <br/><br/>In order to use the Products and Services, users must have access to
                    the World Wide Web and must navigate the Internet to&nbsp;
                <a href="http://VinegarStrokes.com" target="blank">
                    http://VinegarStrokes.com</a>. Individuals who are less than 18 years of age or
                are not permitted to access mature or adult content under the laws of any
                applicable jurisdiction may not access Vany of the Products and Services. If
                Vinegar Strokes Incorporated learns that anyone under the age of 18 has accessed
                VinegarStrokes.com, Vinegar Strokes Incorporated will terminate their access to
                all Products and Services.
                <br/><br/>Registered sex offenders convicted of sexual violence and/or
                    non-consensual sexual offenses are not permitted to have accounts on any of the
                    Products and Services. Vinegar Strokes Incorporated does not discriminate on the
                    basis of age, gender, race, ethnicity, nationality, religion, sexual orientation
                    or any other protected status.
                <br/><br/>
                <b>REGISTRATION AND PRIVACY</b>
                <br/><br/>Certain Products and Services will require the user to register and
                    provide certain data. In consideration of use of such Products and Services, in
                    registering and providing such data, you represent and warrant that: (a) the
                    information about yourself is true, accurate, current and complete (apart from
                    optional items) as required by various Vinegar Strokes Incorporated registration
                    forms (“Registration Data”) and (b) you will maintain and promptly update the
                    Registration Data to keep it true, accurate, current and complete. If you
                    provide any information that is untrue, inaccurate, not current or incomplete,
                    or Vinegar Strokes Incorporated has reasonable grounds to suspect that such
                    information is untrue, inaccurate, not current or incomplete, Vinegar Strokes
                    Incorporated has the right to suspend or terminate your account and refuse any
                    and all current or future use of the Products and Services.
                <br/><br/>All Vinegar Strokes Incorporated registrations become the exclusive
                    property of Vinegar Strokes Incorporated. Vinegar Strokes Incorporated reserves
                    the right to use and reuse all registration and other personally identifiable
                    user information subject to Vinegar Strokes Privacy Policy. Users may edit,
                    update, alter or obscure their personally identifiable information at any time
                    by following the instructions located in Vinegar Strokes Privacy Policy and on
                    the Products and Services.
                <br/><br/>You acknowledge receipt of the Vinegar Strokes Privacy Policy.
                <br/><br/>
                <b>USER CONDUCT</b>
                <br/><br/>You acknowledge and agree that all information, code, data, text,
                    software, photographs, pictures, graphics, video, chat, messages, files, or
                    other materials (“Content”), whether publicly posted or privately transmitted,
                    are the sole responsibility of the person from which such Content originated.
                    This means that you, and not Vinegar Strokes Incorporated, are entirely
                    responsible for all Content that you upload, post, email or otherwise transmit
                    via the Services. Vinegar Strokes Incorporated does not control the user or
                    third party Content posted via the Products and Services, and, as such, does not
                    guarantee the accuracy, integrity or quality of such user or third party
                    Content. You acknowledge and agree that by using VinegarStrokes.com, you may be
                    exposed to Content that may be deemed offensive, indecent or objectionable.
                    Nevertheless, you agree to use the products and services at your sole risk.
                    Under no circumstances will Vinegar Strokes Incorporated be liable in any way
                    for any user or third party Content, including, but not limited to, for any
                    errors or omissions in any such Content, or for any loss or damage of any kind
                    incurred as a result of the use of any such Content posted, emailed or otherwise
                    transmitted via the Products and Services. As a general matter, Vinegar Strokes
                    Incorporated reserves the right to do so. Vinegar Strokes Incorporated does not
                    guarantee that any screening will be done to your satisfaction or that any
                    screening will be done at all. Vinegar Strokes Incorporated reserves the right
                    to monitor some, all, or no areas of the Products and Services for adherence to
                    these Terms and Conditions or any other rules or guidelines posted by Vinegar
                    Strokes Incorporated.
                <br/><br/>The Products and Services may only be used for the intended purpose
                    for which such Products and Services are being made available.
                <br/><br/>Prohibited Conduct
                <br/><br/>You agree that, while using Vinegar Strokes Products and Services, you will not:

                <br/><br/>
                <li>
                    Personally attack, make fun of, troll, flame, bully, stalk or otherwise harass
                    another member.</li>
                <br/>
                <li>
                    Make criminal accusations against another member in a public forum.
                </li><br/>
                <li>
                    Make or promote any type of racism or hate towards anyone in specific or a group
                    of people, unless in the context of role-playing between consenting parties.
                </li><br/>
                <li>
                    Upload pictures or videos that contain any children under 18 years of age.
                </li><br/>
                <li>
                    Upload, post, email, otherwise transmit, or post links to any Content that
                    promotes having any type of sexual relationship with a minor.
                </li><br/>
                <li>
                    Create a user profile for a business, organization or website.
                </li><br/>
                <li>
                    Take anything any other person has uploaded, posted, or emailed to you,
                    VinegarStrokes.com and re-post such content anywhere outside of
                    VinegarStrokes.com without the express written permission of the person who
                    uploaded, posted or emailed you.
                </li><br/>
                <li>
                    Upload, post, email, or otherwise transmit private conversations between two
                    people in any public forum on the Products and Services without the express
                    written consent of the people who are involved in said private conversation.
                </li><br/>
                <li>
                    Post, directly or indirectly, any personally identifying information about
                    another member without their consent. Personally identifying information can
                    include, but is not limited to, a person’s full name, first name, last name,
                    email address, profession, phone number, address and place of work.
                </li><br/>
                <li>
                    Use the Products and Services to do any academic or corporate research without
                    the express written consent of Vinegar Strokes Incorporated.
                </li><br/>
                <li>
                    Cross-post the same message, be it by one person or multiple people, more than 3
                    times in a day.
                </li><br/>
                <li>
                    Sexualize or publicly fantasize about the following topics: murder, animal
                    abuse, and any activities involving minors. You may have strictly intellectual
                    discussions regarding these topics.
                </li><br/>
                <li>
                    Discuss or post content dealing with actual snuff, necrophilia, cannibalism, and
                    zoophilia/bestiality. You may have strictly intellectual discussions regarding
                    these topics, and may also have discussions and content concerning fantasies in
                    these areas.
                </li><br/>
                <li>
                    Create any event to promote a session with a phone sex operator, professional
                    dominant or professional submissive.
                </li><br/>
                <li>
                    Publicly post the schedule, price list or phone number of a phone sex operator,
                    professional dominant or professional submissive on the Products and Services
                    except in a group led by that person or in a group explicitly created for the
                    purpose of posting such information.
                </li><br/>
                <li>
                    Publicly solicit for clients on behalf, directly or indirectly, of phone sex
                    operators, professional dominants and professional submissives in any group
                    except in a group led by that person or in a group explicitly created for the
                    purpose of posting such solicitations.
                </li><br/>
                <li>
                    Solicit or sell any kind of sex for hire.
                </li><br/>
                <li>
                    Use VinegarStrokes.com for any fraudulent purposes.
                </li><br/>
                <li>
                    Collect or store personally identifying information about any other user(s) for
                    commercial purposes without the express consent of the user(s) or for any
                    unlawful purposes.
                </li><br/>
                <li>
                    Impersonate any person or entity, including, but not limited to, an Vinegar
                    Strokes Incorporated or Vinegar Strokes Company official, employee, consultant,
                    or otherwise, or falsely state or otherwise misrepresent your affiliation with a
                    person or entity.
                </li><br/>
                <li>
                    Employ misleading email addresses or falsify information in the header, footer,
                    return path, or any part of any communication, including emails, transmitted
                    through the Products and Services.
                </li><br/>
                <li>
                    Upload, post, email, otherwise transmit, or post links to any Content that you
                    do not have a right to transmit under any law or regulation or under contractual
                    or fiduciary relationships (such as “inside information”, or proprietary and
                    confidential information learned or disclosed as part of employment
                    relationships or subject to a nondisclosure agreement).
                </li><br/>
                <li>
                    Upload, post, email, or otherwise transmit, or post links to any Content that
                    facilitates computer hacking.
                </li><br/>
                <li>
                    Upload, post, email, otherwise transmit, or post links to any Content that
                    infringes any patent, trademark, service mark, trade secret, copyright or other
                    proprietary rights (“Rights”) of any party, or contributing to inducing or
                    facilitating such infringement. This prohibition shall include, without
                    limitation, the following forms of software piracy:
                </li><br/>
                <li>
                    Making available copyrighted software or other Content that has had the
                    copyright protection removed.
                </li><br/>
                <li>
                    Making available serial numbers for software that can be used to illegally
                    validate or register software.
                </li><br/>
                <li>
                    Making available tools that can be used for no purpose other than for “cracking”
                    software or other copyrighted Content.
                </li><br/>
                <li>
                    Making available any software files for which the user does not own the
                    copyright or have the legal right to make available.
                </li><br/>
                <li>
                    Upload, post, email, otherwise transmit, or post links to any unsolicited or
                    unauthorized advertising, including but not limited to promotional materials,
                    “junk mail,” “spam,” “chain letters,” “pyramid schemes,” or any other form of
                    solicitation, except in those areas that are designated for such purpose.
                </li><br/>
                <li>
                    Upload, post, email, otherwise transmit, or post links to any material that
                    contains software viruses, worms, Trojan horses, time bombs, trap doors or any
                    other computer code, files or programs or repetitive requests for information
                    designed to interrupt, destroy or limit the functionality of any computer
                    software or hardware or telecommunications equipment or to diminish the quality
                    of, interfere with the performance of, or impair the functionality of
                    VinegarStrokes.com or of Vinegar Strokes Incorporated.
                </li><br/>
                <li>
                    Use automated means, including additional computers, software and scripts, to
                    enhance play in Products and Services promotions.
                </li><br/>
                <li>
                    Use automated means, including spiders, robots, crawlers, or the like to
                    download data from any Vinegar Strokes network database.
                </li><br/>
                <li>
                    Use automated means, including bots, to create accounts on VinegarStrokes.com.
                </li><br/>
                <li>
                    Modify, publish, transmit, transfer or sell, reproduce, create derivative works
                    from, distribute, link, display or in any way exploit any Content from any
                    Vinegar Strokes database, including, without limitation, by incorporating data
                    from any Vinegar Strokes database into any e-mail or “white pages” products or
                    serviced, whether browser-based, based on proprietary client-site applications,
                    web-based, or otherwise.
                </li><br/>
                <li>
                    Sell, distribute, or make any commercial or unlawful use of data obtained from
                    any Products and Services database or make any other use of data from any
                    Products and Services database in a manner which could be expected to offend the
                    person for whom the data is relevant.
                </li><br/>
                <li>
                    Create and maintain a Vinegar Strokes profile that stores or hosts content for
                    remote loading by other web pages. (For example, you cannot create a Vinegar
                    Strokes profile, post pictures to the Vinegar Strokes profile, and have other
                    web pages call the Vinegar Strokes profile to retrieve those pictures.)
                </li><br/>
                <li>
                    Interfere with or disrupt VinegarStrokes.com or servers or networks connected to
                    Vinegar Strokes, or disobey any requirements, procedures, policies or
                    regulations of networks connected to any of the Products and Services.
                </li><br/>
                <li>
                    Intentionally or unintentionally violate any applicable local, state, national
                    or international law, including, but not limited to, regulations promulgated by
                    the Canadian or U.S. Securities and Exchange Commission, any rules of any
                    national or other securities exchange, including, without limitation, the New
                    York Stock Exchange, the American Stock Exchange or the NASDAQ, and any
                    regulations having the force of law.
                </li><br/>
                <li>
                    Upload, post, email, otherwise transmit, or post links to any material that is
                    false, misleading, or designed to manipulate any equity, security, or other
                    market.
                </li><br/>
                <li>
                    Disobey any Vinegar Strokes Incorporated employee or representative or interfere
                    with any action by any Vinegar Strokes Incorporated employee or representative
                    to redress any violation of these Terms and Conditions.
                </li><br/>
                <li>
                    Create a new account in order to access VinegarStrokes.com after your account or
                    access has been terminated by Vinegar Strokes Incorporated.
                </li><br/>
                <li>
                    Purchase any goods or services that you are prohibited from purchasing or
                    possessing by any law applicable to you in your jurisdiction. The responsibility
                    for ensuring compliance with all such laws shall be the user’s alone. By
                    submitting an order to purchase goods or services, you represent and warrant
                    that you have the legal right to purchase such goods or services.
                </li><br/>
                <li>
                    Use any software deployed in connection with VinegarStrokes.com to process data
                    as a service to other entities without the express written consent of Vinegar
                    Strokes Incorporated or the party from whom such software may be licensed.
                </li><br/>
                <li>
                    De-compile, disassemble, modify, translate, adapt, reverse engineer, create
                    derivative works from, or sub-license any software deployed in connection with
                    VinegarStrokes.com.
                </li><br/>
                <li>
                    Upload, post, email, otherwise transmit, or post links to any material, or act
                    in any manner that is offensive to the Vinegar Strokes community or the spirit
                    of these Terms and Conditions.
                </li><br/>
                <li>
                    Advertise, offer for sale, or sell any of the following items:
                </li><br/>
                <li>
                    Any firearms, explosives, or weapons.
                </li><br/>
                <li>
                    Any items that are hateful or racially, ethnically or otherwise objectionable,
                    that contain child pornography, or are harmful to minors.
                </li><br/>
                <li>
                    Any controlled substances or pharmaceuticals.
                </li><br/>
                <li>
                    Any counterfeit or stolen items.
                </li><br/>
                <li>
                    Any goods or services that do not, in fact, exist.
                </li><br/>
                <li>
                    Any registered or unregistered securities.
                </li><br/>
                <li>
                    Any items that violate or infringe the rights of other parties.
                </li><br/>
                <li>
                    Any items that you do not have the legal right to sell.
                </li><br/>
                <li>
                    Any items where paying Vinegar Strokes Incorporated any of the required
                    transactional or listing fees would cause Vinegar Strokes Incorporated to
                    violate any law.
                </li><br/><br/>
                You acknowledge and agree that Vinegar Strokes Incorporated and its designee
                shall have the right (but not the obligation), in their sole discretion, to
                refuse to publish, remove, or block access to any Content that is available via
                VinegarStrokes.com at any time, for any reason, or for no reason at all, with or
                without notice. Without limitation, Vinegar Strokes Incorporated and its
                designee shall have the right (but not the obligation), at their sole
                discretion, to refuse to publish, remove, or block access to any Content that
                violates the Terms and Conditions or is otherwise objectionable as determined by
                Vinegar Strokes Incorporated. Vinegar Strokes Incorporated may also terminate
                access to, or membership in VinegarStrokes.com, or any portion thereof, for
                violating these Terms and Conditions. You acknowledge and agree that you must
                evaluate, and bear all risks associated with, the use of any Content, including
                any reliance on the accuracy, completeness, or usefulness of such Content. In
                this regard, you acknowledge that you may not rely on any Content created by or
                obtained through the use of VinegarStrokes.com, including without limitation,
                information posted on message boards.
                <br/><br/>You expressly acknowledge and agree that Vinegar Strokes Incorporated
                    may preserve Content and may also disclose Content if required to do so by law
                    or in the good faith belief that such preservation or disclosure is reasonably
                    necessary to: (a) comply with legal process; (b) enforce the Terms and
                    Conditions; (c) respond to claims that any Content violates the rights of
                    third-parties; or (d) protect the rights, property, or personal safety of
                    Vinegar Strokes Incorporated, its users and the public. You acknowledge and
                    agree that the technical processing and transmission of VinegarStrokes.com,
                    including your Content, may involve (a) transmissions over various networks; and
                    (b) changes to conform and adapt to technical requirements of connecting
                    networks or devices. You further acknowledge and agree that other data collected
                    and maintained by Vinegar Strokes Incorporated with regard to its users may be
                    disclosed in accordance with Vinegar Strokes Privacy Policy
                <br/><br/>
                <b>MEMBER OF THE MEDIA</b>
                <br/><br/>I certify that I am not a member of the media, or that if I am, I am
                    participating on the VinegarStrokes.com solely for my personal enjoyment and not
                    as part of any investigation or gathering of information and that I will not use
                    any such information in any manner without the express written permission of
                    Vinegar Strokes Incorporated. I agree that everything that I observe here will
                    be kept in strict confidence, and that I will do nothing to jeopardize the
                    privacy and identities of any other participant without the expressed written
                    permission of Vinegar Strokes Incorporated.
                <br/><br/>
                <b>LIMITED LICENSE TO CONTENT POSTED ON VinegarStrokes.com
                </b>
                <br/><br/>Vinegar Strokes Incorporated claims no ownership interest in any of
                    the Content (including, without limitation, pictures, videos and writing) posted
                    by you on VinegarStrokes.com, and the copyright to all such Content shall remain
                    with its original owner. By posting Content on VinegarStrokes.com, you warrant
                    and represent that you own the Content posted by you or otherwise have the right
                    to grant the license set forth in this section, and that such Content does not
                    violate the rights of any third party. You agree to pay all royalties and fees
                    owing to any person by reason of any Content you post on Vinegar Strokes.
                <br/><br/>In order to make it possible for Vinegar Strokes Incorporated to
                    provide VinegarStrokes.com, you hereby grant Vinegar Strokes Incorporated a
                    limited, non-exclusive, worldwide, royalty-free license to use, reproduce,
                    modify (for example, re-sizing of photos and/or encoding of audio or video
                    files), transmit, publicly display and distribute any Content posted by you on
                    or through any of the Products and Services, and to sub-license such rights
                    solely as necessary to provide VinegarStrokes.com.
                <br/><br/>
                <b>PAYMENT &amp; REFUNDS</b>
                <br/><br/>To request a refund you have to email hello@VinegarStrokes.com within
                    28 days of the transaction and request a refund. Refunds will be for the full
                    price of the transaction. Vinegar Strokes Incorporated does not refund any
                    interest or transaction fees that might have been charged to you by your
                    financial institution.
                <br/><br/>
                <b>THIRD PARTY MERCHANDISE SALES</b>
                <br/><br/>VinegarStrokes.com contain links to third party websites where Users
                    can purchase goods or services from third party merchants (“Merchants”). By
                    submitting an order to purchase any goods or services from Merchants, Users are
                    obligated to complete such transactions, subject to the individual Merchant’s
                    terms and conditions governing such transactions. Users are prohibited from
                    submitting orders to purchase goods or services where they do not intend to
                    complete such transactions.
                <br/><br/>By submitting an order to purchase goods or services from an
                    individual Merchant, you acknowledge that you are entering into a transaction
                    with that Merchant, and that Vinegar Strokes Incorporated is not party to the
                    transaction.
                <br/><br/>Users are obligated to submit information that is true, accurate,
                    current, and complete. By accepting these Terms and Conditions, you represent
                    and warrant that all such information submitted by you is true, accurate,
                    current, and complete. Users are also required to maintain and update all such
                    information in order to ensure that it remains true, accurate, current, and
                    complete. Each time you update such information, you represent and warrant that
                    such information is true, accurate, current, and complete.
                <br/><br/>Users may not purchase goods or services that they are prohibited from
                    purchasing or possessing by any law applicable to them in their jurisdictions.
                    The responsibility for ensuring compliance with all applicable laws shall be the
                    User’s alone. By submitting an order to purchase goods or services, you
                    represent and warrant that you have the legal right to purchase and possess such
                    goods or services.
                <br/><br/>Vinegar Strokes incorporated has no partnership, joint venture,
                    employer-employee, or franchisor-franchisee relationship with any Merchant
                    accessible through the Products and Services. Vinegar Strokes Incorporated
                    cannot confirm that any particular Merchant is who that Merchant claims to be.
                    Nor can Vinegar Strokes Incorporated confirm the truth or accuracy of any
                    statements made by Merchants or control whether Merchants who post statements on
                    the Products and Services will act in accordance with those statements. Vinegar
                    Strokes Incorporated will not get involved in any dispute between Users of the
                    Service and Merchants who post links on the Products and Services. The ability
                    to include links is provided only as a convenience, and the inclusion of any
                    link by an Artist or User does not imply affiliation, endorsement, or adoption
                    by Vinegar Strokes Incorporated of the linked site or any information contained
                    therein.
                <br/><br/>
                <b>PROFILES</b>
                <br/><br/>Users are invited to post content to their profile (“Profile”) subject
                    to the following limitations: (a) Vinegar Strokes Incorporated may limit the
                    amount of bandwidth dedicated to any account; and (b) Vinegar Strokes
                    Incorporated reserves the right to display advertising on Profile pages. Please
                    consult the Vinegar Strokes Privacy Policy for special terms as they relate to
                    Profiles. Profiles are governed by the entire Terms and Conditions, including,
                    without limitation, Section 5 (User Conduct), and you are encouraged to review
                    these terms carefully. Under no circumstances may Profiles be used to infringe
                    the copyright or any other right of any person or entity. Vinegar Strokes
                    Incorporated reserves the right to modify or discontinue the Profiles service at
                    any time, and you are encouraged to maintain back-up copies of the content you
                    post on your Profile. The Profile service may only be used by people who are at
                    least 18 years old.
                <br/><br/>By registering on VinegarStrokes.com, you represent and warrant that
                    all registration information you submit is truthful and accurate and that you
                    will maintain the accuracy of such information. You further represent and
                    warrant that you are 18 years of age or older and that your use of
                    VinegarStrokes.com will not violate any applicable law. Your membership is for
                    your sole, personal use, and you will not authorize others to use your account,
                    including your Profile. You are solely responsible for all Content published or
                    displayed through your account, and for your interactions with other Users.
                <br/><br/>Please carefully choose the information you post on your Profile and
                    that you provide to other members. Your Profile may not include the following
                    items: telephone numbers and street addresses. Please read Section 4 above, User
                    Conduct, for additional terms and conditions regarding your use of
                    VinegarStrokes.com.
                <br/><br/>The information provided by other users in their Profiles may contain
                    inaccurate, inappropriate or offensive material, products or services for which
                    Vinegar Strokes Incorporated assumes no responsibility or liability.
                <br/><br/>Vinegar Strokes Incorporated reserves the right, at its sole
                    discretion, to reject, refuse to post or remove any Content posted by you, and
                    to restrict, suspend, or terminate your access to all or any part of the Vinegar
                    Strokes Network, including any of the Products and Services, at any time, for
                    any or no reason, with or without prior notice, and without liability.
                <br/><br/>
                <b>DISCLAIMER OF WARRANTIES</b>
                <br/><br/>THE PRODUCTS AND SERVICES ARE PROVIDED “AS IS,” WITH NO WARRANTIES
                    WHATSOEVER. ALL EXPRESS, IMPLIED, AND STATUTORY WARRANTIES, INCLUDING, WITHOUT
                    LIMITATION, THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
                    AND NON-INFRINGEMENT OF PROPRIETARY RIGHTS, ARE EXPRESSLY DISCLAIMED TO THE
                    FULLEST EXTENT PERMITTED BY LAW. TO THE FULLEST EXTENT PERMITTED BY LAW, Vinegar
                    Strokes incorporated DISCLAIMS ANY WARRANTIES FOR THE SECURITY, RELIABILITY,
                    TIMELINESS, AND PERFORMANCE OF THE PRODUCTS AND SERVICES. TO THE FULLEST EXTENT
                    PERMITTED BY LAW, Vinegar Strokes Incorporated DISCLAIMS ANY WARRANTIES FOR
                    OTHER SERVICES OR GOODS RECEIVED THROUGH OR ADVERTISED ON THE PRODUCTS AND
                    SERVICES OR RECEIVED THROUGH ANY LINKS PROVIDED IN THE PRODUCTS AND SERVICES, AS
                    WELL AS FOR ANY INFORMATION OR ADVICE RECEIVED THROUGH THE PRODUCTS AND SERVICES
                    OR THROUGH ANY LINKS PROVIDED IN THE PRODUCTS AND SERVICES. Vinegar Strokes
                    incorporated SIMILARLY DISCLAIMS, TO THE FULLEST EXTENT PERMITTED BY LAW, ANY
                    WARRANTIES FOR ANY INFORMATION OR ADVICE OBTAINED THROUGH THE PRODUCTS AND
                    SERVICES.
                <br/><br/>YOU EXPRESSLY UNDERSTAND AND AGREE THAT Vinegar Strokes Incorporated
                    DISCLAIMS ANY AND ALL RESPONSIBILITY OR LIABILITY FOR THE ACCURACY, CONTENT,
                    COMPLETENESS, LEGALITY, RELIABILITY, OR OPERABILITY OR AVAILABILITY OF
                    INFORMATION OR MATERIAL IN THE PRODUCTS AND SERVICES. Vinegar Strokes
                    Incorporated DISCLAIMS ANY RESPONSIBILITY FOR THE DELETION, FAILURE TO STORE,
                    MISDELIVERY, OR UNTIMELY DELIVERY OF ANY INFORMATION OR MATERIAL. Vinegar
                    Strokes Incorporated DISCLAIMS ANY RESPONSIBILITY OR LIABILITY FOR ANY HARM
                    RESULTING FROM DOWNLOADING OR ACCESSING ANY INFORMATION OR MATERIAL THROUGH THE
                    PRODUCTS AND SERVICES, INCLUDING, WITHOUT LIMITATION, FOR HARM CAUSED BY VIRUSES
                    OR SIMILAR CONTAMINATION OR DESTRUCTIVE FEATURES. Vinegar Strokes Incorporated
                    MAKES NO WARRANTY REGARDING THE RELIABILITY OR ACCESSIBILITY OF MEMBER WEB PAGES
                    OR ANY STORAGE FACILITIES OFFERED BY Vinegar Strokes Incorporated.
                <br/><br/>YOU UNDERSTAND AND AGREE THAT ANY MATERIAL DOWNLOADED OR OTHERWISE
                    OBTAINED THROUGH THE USE OF THE PRODUCTS AND SERVICES IS DONE AT YOUR OWN
                    DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGES TO
                    YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS IN THE DOWNLOAD OF SUCH
                    MATERIAL.
                <br/><br/>Some jurisdictions do not allow the disclaimer of implied warranties.
                    In such jurisdictions, the foregoing disclaimers may not apply to you insofar as
                    they relate to implied warranties.
                <br/><br/>
                <b>LIMITATION OF LIABILITY</b>
                <br/><br/>YOU EXPRESSLY UNDERSTAND AND AGREE THAT UNDER NO CIRCUMSTANCES SHALL
                    Vinegar Strokes Incorporated OR ITS LICENSORS BE LIABLE TO ANY USER ON ACCOUNT
                    OF THAT USER’S USE OR MISUSE OF AND RELIANCE ON THE PRODUCTS AND SERVICES. SUCH
                    LIMITATION OF LIABILITY SHALL APPLY TO PREVENT RECOVERY OF DIRECT, INDIRECT,
                    INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, AND PUNITIVE DAMAGES (EVEN IF
                    Vinegar Strokes Incorporated OR ITS LICENSORS HAVE BEEN ADVISED OF THE
                    POSSIBILITY OF SUCH DAMAGES). SUCH LIMITATION OF LIABILITY SHALL APPLY WHETHER
                    THE DAMAGES ARISE FROM USE OR MISUSE OF AND RELIANCE ON THE PRODUCTS AND
                    SERVICES, FROM INABILITY TO USE THE PRODUCTS AND SERVICES, OR FROM THE
                    INTERRUPTION, SUSPENSION, OR TERMINATION OF THE PRODUCTS AND SERVICES (INCLUDING
                    SUCH DAMAGES INCURRED BY THIRD PARTIES).
                <br/><br/>SUCH LIMITATION SHALL ALSO APPLY WITH RESPECT TO DAMAGES INCURRED BY
                    REASON OF OTHER SERVICES OR GOODS RECEIVED THROUGH OR ADVERTISED ON THE PRODUCTS
                    AND SERVICES OR RECEIVED THROUGH ANY LINKS PROVIDED IN THE PRODUCTS AND
                    SERVICES, AS WELL AS BY REASON OF ANY INFORMATION OR ADVICE RECEIVED THROUGH OR
                    ADVERTISED ON THE PRODUCTS AND SERVICES OR RECEIVED THROUGH ANY LINKS PROVIDED
                    IN THE PRODUCTS AND SERVICES. SUCH LIMITATION SHALL APPLY, WITHOUT LIMITATION,
                    TO THE COSTS OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, LOST PROFITS, OR
                    LOST DATA. SUCH LIMITATION SHALL APPLY WITH RESPECT TO THE PERFORMANCE OR
                    NON-PERFORMANCE OF THE PRODUCTS AND SERVICES OR ANY INFORMATION OR MERCHANDISE
                    THAT APPEARS ON, OR IS LINKED OR RELATED IN ANY WAY TO, VinegarStrokes.com. SUCH
                    LIMITATION SHALL APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY
                    LIMITED REMEDY. SUCH LIMITATION SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY
                    LAW.
                <br/><br/>SUCH LIMITATION OF LIABILITY SHALL ALSO APPLY TO ANY DAMAGE CAUSED BY
                    LOSS OF ACCESS TO, DELETION OF, FAILURE TO STORE, FAILURE TO BACK UP, OR
                    ALTERATION OF PROFILES, ARIST PAGES OR OTHER CONTENT STORED THROUGHOUT
                    VinegarStrokes.com OR ANY OF THE PRODUCTS AND SERVICES.
                <br/><br/>UNDER NO CIRCUMSTANCES SHALL Vinegar Strokes Incorporated OR ITS
                    LICENSORS BE HELD LIABLE FOR ANY DELAY OR FAILURE IN PERFORMANCE RESULTING
                    DIRECTLY OR INDIRECTLY FROM ACTS OF NATURE, FORCES, OR CAUSES BEYOND ITS
                    REASONABLE CONTROL, INCLUDING, WITHOUT LIMITATION, INTERNET FAILURES, COMPUTER
                    EQUIPMENT FAILURES, TELECOMMUNICATION EQUIPMENT FAILURES, OTHER EQUIPMENT
                    FAILURES, ELECTRICAL POWER FAILURES, STRIKES, LABOR DISPUTES, RIOTS,
                    INSURRECTIONS, CIVIL DISTURBANCES, SHORTAGES OF LABOR OR MATERIALS, FIRES,
                    FLOODS, STORMS, EXPLOSIONS, ACTS OF GOD, WAR, GOVERNMENTAL ACTIONS, ORDERS OF
                    DOMESTIC OR FOREIGN COURTS OR TRIBUNALS, NON-PERFORMANCE OF THIRD PARTIES, OR
                    LOSS OF OR FLUCTUATIONS IN HEAT, LIGHT, OR AIR CONDITIONING.
                <br/><br/>In some jurisdictions, limitations of liability are not permitted. In
                    such jurisdictions, the foregoing limitation may not apply to you.
                <br/><br/>
                <b>DISCLAIMER REGARDING THIRD PARTY CONTENT</b>
                <br/><br/>Vinegar Strokes Incorporated offers access to numerous third party web
                    pages and content available over the Internet. In overwhelming majority
                    instances, including, but not limited to, sites contained within a directory of
                    links, Vinegar Strokes Incorporated has no control whatsoever over the content
                    of such sites. In other instances, including, but not limited to, web pages
                    hosted on VinegarStrokes.com or provided to Vinegar Strokes Incorporated by
                    third parties, Vinegar Strokes Incorporated may set guidelines for what may
                    appear on such web pages and may engage in certain screening, but generally
                    exercises no control over the content of such web pages and is unable to police
                    all such content. Third party content accessible through VinegarStrokes.com from
                    such sources is developed by people over whom Vinegar Strokes Incorporated
                    exercises no control. Similarly, content hosted by Vinegar Strokes Incorporated
                    which is posted by third parties, and, with the exception of certain random
                    screening functions, Vinegar Strokes Incorporated cannot and does not screen
                    such web pages before they are made accessible to other users of
                    VinegarStrokes.com. Accordingly, neither Vinegar Strokes incorporated nor its
                    licensors assume any responsibility for the content of any site linked to
                    VinegarStrokes.com or in any web page hosted for a third party within
                    VinegarStrokes.com. In instances where Vinegar Strokes incorporated does review
                    the sites that are displayed in directories, indices, and/or references, or
                    where Vinegar Strokes Incorporated makes available directories, indices, and/or
                    references of sites compiled by third parties, the content located at such sites
                    was developed third parties, and review of such sites by Vinegar Strokes
                    Incorporated, if any, is narrowly limited to that which is necessary for
                    compiling the particular set of links.
                <br/><br/>
                <b>STORAGE</b>
                <br/><br/>Many features of VinegarStrokes.com store information on behalf of
                    users. Examples include, but are not limited to chat, Profiles and postings.
                    Vinegar Strokes Incorporated reserves the right, in its sole discretion, to
                    limit the amount of storage space available per user or to delete materials
                    stored for an excessive period while the user’s account has been inactive.
                <br/><br/>
                <b>ADVERTISERS AND CONTENT LINKED TO THE Vinegar Strokes NETWORK</b>
                <br/><br/>Vinegar Strokes Incorporated and the Vinegar Strokes Companies may
                    provide, or third parties may provide, links to other sites or resources located
                    on the World Wide Web by allowing a user to leave VinegarStrokes.com to access
                    third-party material or by bringing the third party material into this site via
                    “inverse” hyperlinks and framing technology. Vinegar Strokes Incorporated has no
                    control over such sites and resources. You acknowledge and agree that Vinegar
                    Strokes Incorporated is not responsible for the availability of such external
                    sites or resources, and does not endorse and is not responsible or liable for
                    any content, advertising, products, or other materials on or available from such
                    sites or resources. You further acknowledge and agree that Vinegar Strokes shall
                    not be responsible or liable, directly or indirectly, for any damage or loss
                    caused or alleged to be caused by or in connection with use of or reliance on
                    any such content, goods or services available on or through any such site or
                    resource.
                <br/><br/>Your correspondence or business dealings with, or in participation
                    with or in promotions of, advertisers found on or through VinegarStrokes.com,
                    including payment and delivery of related goods or services, and any other
                    terms, conditions, warranties or representations associated with such dealings,
                    are solely between you and such advertiser. You agree that Vinegar Strokes
                    Incorporated shall not be responsible or liable for any loss or damage of any
                    sort incurred as the result of any such dealings or as the result of the
                    presence of such advertisers on VinegarStrokes.com.
                <br/><br/>
                <b>NO LICENSE; INTELLECTUAL PROPERTY OF Vinegar Strokes AND OTHERS</b>
                <br/><br/>Except as expressly provided, nothing within VinegarStrokes.com shall
                    be construed as conferring any license under any of Vinegar Stroke
                    Incorporated’s or any third party’s intellectual property rights, whether by
                    estoppel, implication, waiver, or otherwise. Without limiting the generality of
                    the foregoing, you acknowledge and agree that certain Content available through
                    and used to operate VinegarStrokes.com is protected by copyright, trademark,
                    patent, or other proprietary rights of Vinegar Strokes Incorporated and its
                    affiliates, licensors (including, without limitation, Artists), and service
                    providers. Except as expressly provided to the contrary, you agree not to
                    modify, alter, or deface any of the trademarks, service marks, or other
                    intellectual property made available by Vinegar Strokes Incorporated in
                    connection with the Products and Services. You agree not to hold yourself out as
                    in any way sponsored by, affiliated with, or endorsed by Vinegar Strokes
                    Incorporated, any of Vinegar Strokes affiliates, or any of Vinegar Strokes
                    service providers. You agree not to use any of the trademarks or service marks
                    or other Content accessible through VinegarStrokes.com for any purpose other
                    than the purpose for which such Content is made available to users by Vinegar
                    Strokes incorporated. You agree not to defame or disparage Vinegar Strokes, the
                    trademarks or service marks of Vinegar Strokes, or any aspect of the Products
                    and Services. You agree not to adapt, translate, modify, de-compile,
                    disassemble, or reverse engineer VinegarStrokes.com or any software or programs
                    used in connection with VinegarStrokes.com.
                <br/><br/>In order to maintain the privacy and security of your user profile,
                    you hereby grant Vinegar Strokes Incorporated a limited, non-exclusive,
                    worldwide, royalty-free license to any and all copyrightable content posted by
                    you on or through VinegarStrokes.com. You also grant and assign Vinegar Strokes
                    or its affiliates the right and authority to enforce your DMCA and any and all
                    intellectual property rights against alleged infringers at your request. This
                    provision does not constitute in any way a partnership, joint venture or any
                    other fiduciary relationship between you and Vinegar Strokes Incorporated.
                    Further, Vinegar Strokes Incorporated does not warrant or guarantee that it can
                    or will enforce your intellectual property rights against alleged infringers.
                    Vinegar Strokes Incorporated reserves the right through this limited license to
                    enforce your rights at your request, however, it remains the sole and primary
                    responsibility of each member, the Content creator and owner, to police and
                    enforce your rights against alleged infringers.
                <br/><br/>
                <b>LINKING TO VinegarStrokes.com; USE OF LOGO</b>
                <br/><br/>Vinegar Strokes Incorporated offers a non-assignable,
                    non-transferable, and non-exclusive license to link to VinegarStrokes.com, using
                    Vinegar Strokes logos, subject to the following provisions. Vinegar Strokes
                    logos may be placed on a Web site for the sole purpose of creating a link to
                    VinegarStrokes.com and allowing users of your site to access the Products and
                    Services on VinegarStrokes.com. Vinegar Strokes logos may not be used for any
                    other purpose, including, among other purposes, to suggest sponsorship by, or
                    affiliation with, or endorsement by Vinegar Strokes Incorporated. Vinegar
                    Strokes logos may only be used in accordance with the Vinegar Strokes Trademark
                    Usage Guidelines, and may only be used in the exact size, shape, colors, design,
                    and configuration as found on such web page. Vinegar Strokes logos may not be
                    altered in any manner. Vinegar Strokes logos must appear by themselves, with
                    reasonable spacing (at least the height of the logo) between each side of the
                    applicable logo and other graphic or textual elements. Vinegar Strokes logos may
                    not be used to disparage Vinegar Strokes, its products or services, or in a
                    manner which, in Vinegar Strokes’ reasonable judgment, may diminish or otherwise
                    damage Vinegar Strokes’ good will in Vinegar Strokes logos. By using any such
                    Vinegar Strokes Network logo, you acknowledge that Vinegar Strokes has exclusive
                    rights to the logo, and that all goodwill generated through your use of the logo
                    will inure to the benefit of Vinegar Strokes. If you use Vinegar Strokes logos,
                    you must include appropriate attribution, for example: “Vinegar Strokes® is a
                    registered trademark of Vinegar Strokes” Vinegar Strokes Incorporated reserves
                    the right to revoke this license or to alter its terms from time to time, for
                    any or no reason, with or without notice. Vinegar Strokes Incorporated reserves
                    the right to take action against any use that does not conform to these
                    provisions.
                <br/><br/>
                <b>INDEMNITY AND RELEASE</b>
                <br/><br/>By using VinegarStrokes.com you agree to indemnify Vinegar Strokes
                    Incorporated, and their officers, employees, volunteers and licensors, and hold
                    them harmless from any and all claims and expenses, including attorney’s fees,
                    arising from your use of VinegarStrokes.com, your use of the Products and
                    Services, or your submission of ideas and/or related materials to Vinegar
                    Strokes or from any person’s use of any account or password you maintain with
                    Vinegar Strokes, regardless of whether such use is authorized by you. By using
                    VinegarStrokes.com, using the Products and Services, or submitting any ideas
                    and/or related materials to Vinegar Strokes Incorporated, you are hereby
                    agreeing to release Vinegar Strokes and its parents, subsidiaries, affiliates,
                    officers, employees, volunteers, and licensors from any and all claims, demands,
                    debts, obligations, damages (actual or consequential), costs, and expenses of
                    any kind or nature whatsoever, whether known or unknown, suspected or
                    unsuspected, disclosed or undisclosed, that you may have against them arising
                    out of or in any way related to such disputes and/or to the Products and
                    Services or to any disputes regarding use of ideas and/or related materials
                    submitted to VinegarStrokes.com YOU HEREBY AGREE TO WAIVE ALL LAWS THAT MAY
                    LIMIT THE EFFICACY OF SUCH RELEASES.
                <br/><br/>
                <b>LIMITATION OF ACTIONS</b>
                <br/><br/>You acknowledge and agree that, regardless of any statute or law to
                    the contrary, any claim or cause of action you may have arising out of, or
                    relating to, your use of VinegarStrokes.com or the Products and Services must be
                    filed within one (1) year after such claim or cause of action arises, or forever
                    be barred.
                <br/><br/>
                <b>COPYRIGHT, TRADEMARK, AND PATENT NOTICES</b>
                <br/><br/>All other marks that appear throughout VinegarStrokes.com belong to
                    Vinegar Strokes or the respective owners of such marks, and are protected by
                    Canadian, U.S. and international copyright and trademark laws. Any use of any of
                    the marks appearing throughout the Products and Services without the express
                    written consent of Vinegar Strokes or the owner of the mark, as appropriate, is
                    strictly prohibited. Vinegar Strokes may provide, or third parties may provide,
                    links to other sites or resources located on the World Wide Web by allowing a
                    user to leave Vinegar Strokes to access third-party material or by bringing the
                    third party material into this site via “inverse” hyperlinks and framing
                    technology. Vinegar Strokes have no control over such sites and resources. You
                    acknowledge and agree that Vinegar Strokes are not responsible for the
                    availability of such external sites or resources, and do not endorse and are not
                    responsible or liable for any content, advertising, products, or other materials
                    on or available from such sites or resources. You further acknowledge and agree
                    that Vinegar Strokes shall not be responsible or liable, directly or indirectly,
                    for any damage or loss caused or alleged to be caused by or in connection with
                    use of or reliance on any such content, goods or services available on or
                    through any such site or resource.
                <br/><br/>Your correspondence or business dealings with, or participation in
                    promotions of, advertisers found on or through VinegarStrokes.com, including
                    payment and delivery of related goods or services, and any other terms,
                    conditions, warranties or representations associated with such dealings, are
                    solely between you and such advertiser. You agree that Vinegar Strokes shall not
                    be responsible or liable for any loss or damage of any sort incurred as the
                    result of any such dealings or as the result of the presence of such advertisers
                    on the Product and Services.
                <br/><br/>
                <b>INTELLECTUAL PROPERTY INFRINGEMENT CLAIMS</b>
                <br/><br/>Vinegar Strokes respects the intellectual property of others. It is
                    Vinegar Stroke’s policy to respond expeditiously to claims of copyright and
                    other intellectual property infringement. Vinegar Strokes will promptly process
                    and investigate notices of alleged infringement and will take appropriate
                    actions under the Digital Millennium Copyright Act (“DMCA”) and other applicable
                    intellectual property laws. Upon receipt of notices complying or substantially
                    complying with the DMCA, Vinegar Strokes may act expeditiously to remove or
                    disable access to any material claimed to be infringing or claimed to be the
                    subject of infringing activity and may act expeditiously to remove or disable
                    access to any reference or link to material or activity that is claimed to be
                    infringing. Vinegar Strokes will terminate access for users who are repeat
                    infringers.
                <br/><br/>Notifying Vinegar Strokes of Copyright Infringement: To provide
                    Vinegar Strokes notice of an infringement read&nbsp;
                <a href="http://vinegarstrokes.com/copyright/" target="blank">
                    our copyright policy and take down notice procedure.</a>&nbsp;Please note that
                you may be liable for damages (including costs and attorneys’ fees) if you
                materially misrepresent that an activity is infringing your copyright.
                <br/><br/>
                <b>ARBITRATION, GOVERNING LAW AND FORUM FOR DISPUTES</b>
                <br/><br/>Unless expressly stated to the contrary elsewhere within the Products
                    and Services, all legal issues arising from or related to the use of the
                    Products and Services shall be construed in accordance with, and all questions
                    with respect thereto shall be determined by, the laws of British Columbia,
                    Canada applicable to contracts entered into and wholly to be performed within
                    said province. Any controversy or claim arising out of or relating to these
                    Terms and Conditions or any user’s use of the Products and Services shall be
                    settled by binding arbitration in accordance with the commercial arbitration
                    rules of the Arbitration and Mediation Institute of Canada. Any such controversy
                    or claim shall be arbitrated on an individual basis, and shall not be
                    consolidated in any arbitration with any claim or controversy of any other
                    party. The arbitration shall be conducted in Vancouver, British Columbia, and
                    judgment on the arbitration award may be entered into in any provincial or
                    federal court in British Columbia having jurisdiction thereof. Any party seeking
                    temporary or preliminary injunctive relief may do so in any provincial or
                    federal court in British Columbia having jurisdiction thereof. Except as set
                    forth above, the provincial and federal courts of British Columbia shall be the
                    exclusive forum and venue to resolve disputes arising out of or relating to
                    these Terms and Conditions or any user’s use of the Products and Services. By
                    using the Products and Services and thereby agreeing to these Terms and
                    Conditions, users consent to personal jurisdiction and venue in the provincial
                    and federal courts in British Columbia with respect to all such disputes.
                <br/><br/>
                <b>CHANGES IN TERMS AND CONDITIONS AND CHANGES IN PRODUCTS AND SERVICES</b>
                <br/><br/>Vinegar Strokes Incorporated reserves the right to modify
                    VinegarStrokes.com from time to time, for any reason, and without notice,
                    including the right to terminate the Products and Services. Vinegar Strokes
                    reserves the right to modify these Terms and Conditions from time to time,
                    without notice. Please review these Terms and Conditions from time to time so
                    you will be apprised of any changes.
                <br/><br/>
                <b>MERGER</b>
                <br/><br/>These Terms and Conditions constitute the entire agreement between the
                    parties with respect to the subject matter contained herein and supersede any
                    other agreements, proposals and communications, written or oral, between Vinegar
                    Strokes representations and you with respect to the subject matter hereof;
                    except that any other terms and conditions located on VinegarStrokes.com or in
                    connection with the Products and Services are incorporated herein by reference
                    to the extent they do not conflict with these Terms and Conditions. To the
                    extent that any other terms and conditions or terms of service conflict with
                    these Terms and Conditions, those other provisions shall control with respect to
                    the use of the particular web site and any Products or Services available on or
                    through the web site or the respective Product or Service at which those other
                    provisions may be found.
                <br/><br/>
                <b>NON-WAIVER AND SEPARABILITY</b>
                <br/><br/>Vinegar Strokes failure to exercise any right or provision of these
                    Terms and Conditions shall not constitute a waiver of such right or provision.
                    If a court of competent jurisdiction holds any provision of these Terms and
                    Conditions to be invalid, the parties nevertheless agree that the court should
                    endeavor to give effect to the parties’ intentions as reflected in the
                    provision, and agree that the other provisions of these Terms and Conditions
                    remain in full force and effect.
                <br/><br/>
                <b>RELATIONSHIP OF PARTIES</b>
                <br/><br/>You acknowledge and agree that you and Vinegar Strokes are independent
                    contractors under these Terms and Conditions, and nothing herein shall be
                    construed to create a partnership, joint venture, agency, or employment
                    relationship. Neither party pursuant to these Terms and Conditions has authority
                    to enter into agreements of any kind on behalf of the other and neither party
                    shall be considered the agent of the other.
                <br/><br/>
                <b>NO RESALE, ASSIGNMENT, OR SUB-LICENSING
                </b>
                <br/><br/>You agree not to resell, assign, sub-license, otherwise transfer, or
                    delegate your rights or obligations under these Terms and Conditions without
                    prior express written authorization of Vinegar Strokes Incorporated.
                <br/><br/>
                <b>SUCCESSORS AND ASSIGNS</b>
                <br/><br/>Without in any way limiting the prohibition on your resale,
                    assignment, sub-licensing, or other transfer of rights or obligations, these
                    Terms and Conditions shall be binding upon and inure to the benefit of the
                    parties hereto and their respective heirs, successors, and assigns.
                <br/><br/>
                <b>TERMINATION; SURVIVAL</b>
                <br/><br/>These Terms and Conditions shall continue in effect for as long as you
                    use the Products and Services, unless specifically terminated earlier by Vinegar
                    Strokes. All provisions of these Terms and Conditions which impose obligations
                    continuing in their nature shall survive termination of these Terms and
                    Conditions.
                <br/><br/>
                <b>COMMUNICATIONS WITH USERS</b>
                <br/><br/>You consent to receive communications from Vinegar Strokes concerning
                    your use of VinegarStrokes.com (“Communications”). The Communications may be
                    those that Vinegar Strokes is required to send to you by law concerning the
                    Products and Services (“Required Communications”). The Communications may also
                    be those that Vinegar Strokes sends to you for other reasons. You consent to
                    receive Communications electronically. Vinegar Strokes may provide these
                    Communications to you by sending an email to the email address you provided in
                    connection with your account or by posting the Communication on Vinegar Strokes.
                    You may change the email address to which Vinegar Strokes sends Communications
                    by visiting your account information page.
                <br/><br/>In order to receive Required Communications, you must provide Vinegar
                    Strokes, upon registration with Vinegar Strokes, a valid email address to which
                    Vinegar Strokes may send electronic mail.
                <br/><br/>
                <b>SUBMISSIONS OF IDEAS</b>
                <br/><br/>Vinegar Strokes is always improving its Products and Services and
                    developing new features. If you have ideas regarding improvements or additions
                    to VinegarStrokes.com, we would like to hear them — but any submission will be
                    subject to these Terms and Conditions. UNDER NO CIRCUMSTANCES SHALL ANY
                    DISCLOSURE OF ANY IDEA OR RELATED MATERIALS TO Vinegar Strokes BE SUBJECT TO ANY
                    OBLIGATION OF CONFIDENTIALITY OR EXPECTATION OF COMPENSATION. BY SUBMITTING THE
                    IDEA AND/OR ANY RELATED MATERIAL TO Vinegar Strokes, YOU ARE WAIVING ANY AND ALL
                    RIGHTS THAT YOU MAY HAVE IN THE IDEA OR ANY RELATED MATERIALS AND ARE
                    REPRESENTING AND WARRANTING TO Vinegar Strokes THAT THE IDEA AND/OR RELATED
                    MATERIALS ARE WHOLLY ORIGINAL WITH YOU, THAT NO ONE ELSE HAS ANY RIGHTS IN THE
                    IDEA AND/OR MATERIALS AND THAT VINEGAR STROKES INC IS FREE TO IMPLEMENT THE IDEA
                    AND TO USE THE MATERIALS IF IT SO DESIRES, AS PROVIDED OR AS MODIFIED BY VINEGAR
                    STROKES, WITHOUT OBTAINING PERMISSION OR LICENSE FROM ANY THIRD PARTY.
                <br/><br/>
                <b>EXPORT CONTROLS</b>
                <br/><br/>Certain software, and related documentation or technical information,
                    available through Vinegar Strokes is subject to applicable laws and regulations
                    of the United States pertaining to export controls. By using such software or
                    related documentation or technical information, you represent and warrant that
                    you are not located in, or under the control of, or a national or resident of
                    any embargoed country or any country on the Canadian or U.S. Department of
                    Commerce’s Table of Denial Orders. You agree not to export or re-export such
                    software or related documentation or technical information directly or
                    indirectly to any countries that are subject to Canadian and United States
                    export restrictions.
                <br/><br/>
                <b>VIOLATIONS OF TERMS AND CONDITIONS</b>
                <br/><br/>Should you violate these Terms and Conditions or any other rights of
                    Vinegar Strokes, Vinegar Strokes reserve the right to pursue any and all legal
                    and equitable remedies against you, including, without limitation, terminating
                    any and all user accounts on any and all Vinegar Strokes properties.
                <br/><br/>If you are aware of any violations of these Terms and Conditions,
                    please report using our email:&nbsp;
                <a href="mailto:hello@vinegarstrokes.com">
                    <u>hello@vinegarstrokes.com</u>
                </a>
                <br/><br/>Please read the entire Terms and Conditions, as all Vinegar Strokes
                    properties, Products and Services are offered subject to these Terms and
                    Conditions.
            </div>
        </div>

        let privacy = <div>
            <div className="titleSection">Privacy Policy</div>
            <div className="section">Your privacy is very important to us and we want to be
                transparent about our privacy policies so that you can feel comfortable using
                and sharing information about yourself on Vinegar Strokes.<br/><br/>
                The rule we live by when it comes to making decisions about privacy is:
                <br/><br/>“Treat people’s information like we would want others to treat ours.”
                <br/><br/>If you have any questions or feedback on our privacy policy please
                    don’t hesitate to send us a message to our email:&nbsp;
                <a href="mailto:hello@vinegarstrokes.com">
                    <u>hello@vinegarstrokes.com</u>
                </a>
                <br/><br/>Information You Provide Us
                <br/><br/>
                <b>Personal Information</b>
                <br/>To access Vinegar Strokes you first need to sign-up. When you sign-up, you
                    provide us with a nickname people will use to refer to you, your email address,
                    the gender you associate with, your sexual orientation, your role(s) (sexually
                    speaking), date of birth and location. You can also add additional information
                    about yourself once you have finished signing-up such as a personal description
                    of yourself, what you are looking for, relationships you are in and fetishes you
                    are into and curious about, amongst other things.
                <br/>But don’t stress about it; you can visit your profile at any time to modify
                    your personal information. Nothing is permanent.
                <br/><br/>
                <b>Content</b>
                <br/>Anything posted on your profile can be removed at any time; it is your
                    profile after all. You should think twice though before you start or comment on
                    any discussions. To maintain the integrity of discussions we don’t remove the
                    original post or any comments unless they break one of our guidelines or our
                    Terms of Use.
                <br/>Transaction Information
                <br/>If you choose to use Vinegar Strokes, and we hope you do, you do so through
                    CCiBill. These are the three 3rd party merchant providers we use to handle all
                    credit card transactions for us. You can view their respective privacy policies
                    here.
                <br/><br/>
                <b>Information We Collect When You Interact With Vinegar Strokes</b>
                <br/><br/>Site Activity
                <br/>We log the actions you take on Vinegar Strokes such as adding a friend,
                    joining a group, uploading a picture or attending an event. The logs are used to
                    create an activity feed for those who you are friends with.
                <br/><br/>Logs and Analytics
                <br/>Web servers write to a log file every time you access a page on Vinegar
                    Strokes. In that log file your IP Address, browser information and page you
                    requested is logged. This is the standard information logged by web-servers.
                <br/>On top of that, we use Google Analytics to better understand how people use
                    Vinegar Strokes so that we can improve the experience for you. Google Analytics
                    collects the information sent by your browser as part of a web page request. To
                    get a better understanding of how Google uses the information we collect we
                    recommend you read their Privacy Policy.
                <br/><br/>
                <b>Cookies</b>
                <br/>“C is for cookie and that’s good enough for me.” – Sesame Street’s Cookie
                    Monster.
                <br/><br/>Vinegar Strokes uses cookies for authentication and to store
                    information in between page requests. If you want to learn more about cookies
                    you should read the wikipedia article on HTTP cookies.
                <br/>At anytime, you can delete the cookies that are on you computer.
                    Instructions: Safari, Chrome, Internet Explorer, Firefox, and Opera.
                <br/><br/>
                <b>How We Use Your Information</b>
                <br/>To Contact You
                <br/>We use your email address attached to your profile to contact you and send
                    you email notifications. If you want us to contact you using another email
                    address and or not send you certain email notifications please update your
                    settings on Vinegar Strokes. We do not give sell or share your email address
                    with any third parties. That would be wrong.
                <br/>To Serve Ads
                <br/>We allow our advertisers to target their ads by location and gender. We use
                    your IP address and gender you specified on Vinegar Strokes to do the targeting.
                    We use AdGear’s ad platform to serve ads for us and we do not give advertisers
                    any information about people who have clicked on their ads. We just give them
                    aggregate non-personally-identifiable information. You can view AdGear’s privacy
                    policy here.
                <br/><br/>
                <b>How We Share Information</b>
                <br/><br/>Law and Harm
                <br/>We may disclose your information if we believe that it is reasonably
                    necessary to comply with a law, regulation or legal request; to protect the
                    safety of anyone; to address fraud, security or technical issues; or to protect
                    Vinegar Strokes’ rights or property.
                <br/>Service Providers
                <br/>We engage certain trusted third parties to perform functions and provide
                    services to us. We may share your personal information with these third parties,
                    but only to the extent necessary to perform these functions and provide such
                    services. Our personal information is on Vinegar Strokes as well, we would never
                    use companies that don’t share a similar privacy philosophy as us.
                <br/><br/>Business Transfers
                <br/>In the event that Vinegar Strokes is involved in a bankruptcy, merger,
                    acquisition, reorganization or sale of assets, your information may be sold or
                    transferred as part of that transaction. The promises in this privacy policy
                    will apply to your information as transferred to the new entity. No, we are not
                    for sale and not planning to sell Vinegar Strokes. This note is just one of
                    those necessary evils.
                <br/><br/>
                <b>How You Can View and Modify Your Personal Information</b>
                <br/><br/>Once you login to Vinegar Strokes you can view and edit your profile
                    and personal information we have on file for you.
                <br/><br/>
                <b>Steps We Take To Protect Your Information</b>
                <br/>We Encrypt Your Password
                <br/>Believe it or not, a lot of websites still store passwords in plain text so
                    anyone who has access to the database can see your password. We encrypt your
                    password so we have no way to tell what your password is. Fun tip: To see if any
                    specific site encrypts your password, request your login information through
                    their login form and if they send you back your password then you know they
                    don’t encrypt it.
                <br/>We Use SSL to Login to Vinegar Strokes
                <br/>When you login to Vinegar Strokes it is done over SSL so if someone wants
                    to try to steal your login credentials when you login they wouldn’t be able to
                    sniff it over the network. SSL is the same technology your bank uses when you
                    login to their website.
                <br/>We Protect You From XSS Vulnerabilities
                <br/>We have gone to great lengths to protect all members of Vinegar Strokes
                    from XSS (Cross Site Scripting) vulnerabilities but if you do come across any
                    vulnerabilities please email us ASAP at our email:&nbsp;
                <a href="mailto:hello@vinegarstrokes.com">
                    <u>hello@vinegarstrokes.com</u>
                </a>
                so we can fix the problem right away.
                <br/>If you want to learn more about XSS check out Cross-Site Scripting (XSS) on
                    Wikipedia.
                <br/><br/>
                <b>Surgeon General’s Warning</b>
                <br/>Please be aware that no security measures are perfect or impenetrable. We
                    cannot control the actions of other users with whom you share your information.
                    We cannot ensure that information you share on Vinegar Strokes will not become
                    publicly available. We can’t be responsible for third party circumvention of any
                    privacy settings or security measures on Vinegar Strokes. You can reduce these
                    risks by using common sense security practices such as choosing a strong
                    password, using different passwords for different services, and using up-to-date
                    antivirus software.
                <br/><br/>
                <b>Reporting Violations</b>
                <br/>You should report any privacy and or security violations to us by using our
                    email:&nbsp;
                <a href="mailto:hello@vinegarstrokes.com">
                    <u>hello@vinegarstrokes.com</u>
                </a>
                <br/><br/>
                <b>Changes to this Policy</b>
                <br/>We may revise this Privacy Policy from time to time. The current version of
                    the policy will govern our use of your information and will always be available
                    at&nbsp;
                <a href="http://vinegarstrokes.com/privacy-policy" target="blank">
                    vinegarstrokes.com/privacy-policy.
                </a>&nbsp; By continuing to access or use the Services after those changes
                become effective, you agree to be bound by the revised Privacy Policy.
                <br/><br/>
                <b>Got a Question?</b>
                <br/>By using our email:&nbsp;
                <a href="mailto:hello@vinegarstrokes.com">
                    <u>hello@vinegarstrokes.com</u>
                </a>&nbsp; you can send us a question or concern.
            </div>
        </div>

        let copyright = <div>
            <div className="titleSection">Copyright</div>
            <div className="section">
                <span className="subsection">Intellectual Property and Publicity Rights</span>
                <br/><br/>Vinegar Strokes is a community built on respect and creativity. We
                    ask, rather we beg, that you remember this when you are posting work on Vinegar
                    Strokes. If you make sure that all the works you upload consist of your very own
                    original ideas and are not infringing on the intellectual property or publicity
                    rights of another, you will help us foster the supportive and creative
                    environment that is Vinegar Strokes. AND, besides being counter to all that
                    Vinegar Strokes stands for, stealing other people’s work and passing it off as
                    your own is against the law:
                <br/><br/>There are several international treaties that relate to intellectual
                    property, but the laws are not uniform across the world. However, generally
                    speaking…
                <br/><br/>
                <b>COPYRIGHT</b>&nbsp; law protects the expression of an original idea recorded
                in a tangible form, such as artwork in the form of photographs or paintings and
                literary works in the form of poems or stories.
                <br/><br/>
                <b>TRADEMARK</b>&nbsp; law protects the use of words, symbols, designs or logos
                that identify and distinguish a source of goods.
                <br/><br/>
                <b>PUBLICITY RIGHTS</b>&nbsp; protect an individual’s name, image and likeness.
                Basically this means you can’t use someone else’s identity to your commercial
                advantage, without their consent.
                <br/><br/>
                <b>A NOTE ON FAIR USE:</b>&nbsp; You might be able to incorporate someone else’s
                copyright or trademark into your own work, if you do it in such a way that is
                considered “fair use” or in such a way that qualifies as another permissible
                use. However, please be aware that “fair use” typically applies in limited
                circumstances and isn’t the same as “freedom of speech”. Quite frankly “fair
                use” is a difficult concept, even for the experts, and the scope of “fair use”
                is different in different countries. So even if you think you’ve created a work
                covered by “fair use” or another permissible use, you should talk to an attorney
                prior to using it in connection with the Vinegar Strokes service. While some
                uses might appear “fair” or permissible to you, we ask that you understand that
                Vinegar Strokes must act in accordance with its Notice and Takedown procedure
                when a report has been received. Additionally, certain content may be taken down
                without a specific report being received if we are otherwise alerted or aware of
                potential infringement issues.
                <br/><br/>Ultimately, you take full responsibility for the content you upload
                    and display on Vinegar Strokes. This is reflected in the Vinegar Strokes Terms
                    and Conditions. Use of VinegarStrokes.com indicates continued acceptance of this
                    Agreement.
                <br/><br/>So please do us all a favor and if someone has created or owns the
                    rights to a picture, video, painting, photograph, logo, story, poem or any other
                    work, copyright, trademark or publicity right (regardless of whether it’s an
                    individual or a large global company), obtain consent before you use that work
                    in connection with the Vinegar Strokes service. This will help ensure that you
                    don’t infringe the rights of any third party and help us promote an encouraging
                    and inspired environment at Vinegar Strokes filled with wit, soul, meaning and
                    aesthetic genius.
                <br/><br/>
                <b>REPEAT INFRINGER POLICY:</b>&nbsp; It is Vinegar Strokes policy, in
                appropriate circumstances, to disable and/or terminate the accounts of users who
                repeatedly infringe upon or are repeatedly charged with infringing upon the
                copyrights, trademark rights, other intellectual property rights or publicity
                rights of others.<br/><br/>
                <span className="subsection">Notice and Takedown Procedure – Reports and Complaints</span>
                <br/><br/>Vinegar Strokes respects the intellectual property rights of others
                    and we ask our users to do the same.
                <br/><br/>If you believe that your content has been used in a way that
                    constitutes an infringement of your rights, please notify the Vinegar Strokes
                    designated agent for complaints (contact email below) by sending a Notice and
                    Takedown Report, which must include the following important information:<br/>
                <br/>1. an electronic or physical signature of the person authorised to act on
                    behalf of the owner of the relevant matter;
                <br/>2. a description of the matter claimed to have been infringed;
                <br/>3. a description of where the claimed infringing content is located on the
                    Vinegar Strokes site. URLs should be in the following
                    form:http://www.vinegarstrokes.com/works/
                <br/>4. your address, telephone number, and email address;
                <br/>5. a statement by you that you have a good faith belief that the disputed
                    use is not authorised by the owner, its agent, or the law;
                <br/>6. a statement by you, made under penalty of perjury, that: a. the above
                    information is accurate; and b. you are authorised to act on behalf of the owner
                    of the rights involved.
                <br/><br/>Vinegar Strokes Designated Agent for complaints can be reached through
                    our email:&nbsp;
                <a href="mailto:hello@vinegarstrokes.com">
                    <u>hello@vinegarstrokes.com</u>
                </a>
                <br/><br/>Additionally, in certain circumstances, Vinegar Strokes may suspend or
                    terminate users who in our opinion purposely infringe upon the copyrights,
                    trademarks, publicity rights or other rights of others.<br/><br/>
                <span className="subsection">If Your Work Has Been Included in a Notice and Takedown Report</span>
                <br/><br/>If the Vinegar Strokes Content Team has received a Notice and Takedown
                    Report which specifically notes one or more of your works, the noted works will
                    have been removed. You will not be able to view or update these works.
                <br/><br/>The Notice and Takedown Report that has been received may or may not
                    have also included other works by other artists, and by removing the work, we
                    are not stating that your work does or does not infringe copyright, trademark or
                    publicity rights law.
                <br/><br/>We have a legal obligation to act on reports filed in accordance with
                    our IP/Publicity Rights Policy.
                <br/><br/>While the work may not have been a direct copy of someone else’s work,
                    it may contain elements, logos, or personal likenesses which may infringe on
                    another’s rights.
                <br/><br/>If you believe a report was in error or should not apply to your work,
                    you have the right to lodge a counter-notification.
                <br/><br/>We do apologize that we are legally not able to provide individual
                    copyright, trademark or publicity rights advice, or give personal opinions on
                    these matters.
                <br/><br/>We recommend that you research the relevant copyright, trademark and
                    publicity rights laws and their application to your work on the Internet (a few
                    links below), or consult an I.P. specialist if you are unsure why your specific
                    work may have been included in a Notice and Takedown Report.
                <br/><br/>General Copyright:&nbsp;
                <a href="http://en.wikipedia.org/wiki/Copyright" target="blank">
                    http://en.wikipedia.org/wiki/Copyright</a>
                <br/>General Trademark:&nbsp;
                <a href="http://en.wikipedia.org/wiki/Trademark" target="blank">
                    http://en.wikipedia.org/wiki/Trademark</a>
                <br/>General Publicity Rights:&nbsp;
                <a href="http://en.wikipedia.org/wiki/Personality_rights" target="blank">
                    http://en.wikipedia.org/wiki/Personality_rights</a>
                <br/>Australia:&nbsp;
                <a href="http://www.copyright.org.au/find-an-answer/" target="blank">
                    http://www.copyright.org.au/find-an-answer/</a>
                <br/>United Kingdom:&nbsp;
                <a href="http://www.ipo.gov.uk/copy.htm" target="blank">
                    http://www.ipo.gov.uk/copy.htm</a>
                <br/>United States Copyright:&nbsp;
                <a href="http://www.copyright.gov/" target="blank">
                    http://www.copyright.gov/</a>
                <br/>United States Trademark:&nbsp;
                <a href="http://www.uspto.gov/trademarks/index.jsp" target="blank">
                    http://www.uspto.gov/trademarks/index.jsp</a>
                <br/><br/>
                <span className="subsection">Filing a Counter Notice</span>
                <br/><br/>If you believe that removal of the content is the result of a mistake
                    (for example, that you have authorisation) or misidentification, you can send us
                    a counter notice. Such counter notice must provide the following information:<br/><br/>
                1. an electronic or physical signature of the person authorised to act on behalf
                of the owner of the relevant matter;
                <br/>2. a description of the content which we have removed, including the URL on
                    which the content was located on the Vinegar Strokes site;
                <br/>3. your address, telephone number, and email address;
                <br/>4. a statement by you that you consent to the jurisdiction of Edmonton,
                    Alberta, Canada and that you will accept service of process from the person who
                    provided notification described above or an agent of such person;
                <br/>5. a statement by you that, under penalty of perjury, you have a good faith
                    belief that the material was removed or disabled as a result of mistake or
                    misidentification of the material to be removed or disabled;
                <br/><br/>You can send a counter notice through our contact form and selecting
                    Report from the drop down menu.
                <br/><br/>If we receive your counter notice but your work does not comply with
                    the Vinegar Strokes Terms and Conditions and/or IP/Publicity Rights Policy, we
                    may inform you that we are not be able to reinstate your work. We may also
                    request further information from you in order to determine whether the work can
                    be reinstated.
                <br/><br/>In many circumstances, however, we will forward your counter notice
                    directly to the complainant, which will include your personal contact
                    information. At that time the complainant may take legal court action against
                    you in the United States. If after 14 days the complainant has not taken legal
                    action against you, you may contact us to request that we reinstate your work.
                    If your work otherwise complies with our User Agreement and IP/Publicity Rights
                    Policy, we may reinstate your work at that time.
            </div>
        </div>

        let community = <div>
            <div className="titleSection">Community Guidelines</div>
            <div className="section">Our number one priority is to create a fun and safe
                place for open minded people like you and I, to call home.
                <br/>To make sure Vinegar Strokes is a fun and safe place, we wrote the
                    following list of general community guidelines we would like everyone to
                    respect.
                <br/>Just remember, that your participation on Vinegar Strokes is always subject
                    to our Terms and Conditions.<br/><br/>
                <b>Be Open-Minded and Non-Judgemental</b>
                <br/>People often say to us how they love the fact that Vinegar Strokes’s
                    community is so open-minded and non-judgemental, and we are proud of that.
                <br/>Please help us make sure Vinegar Strokes stays this way.
                <br/><br/>
                <b>No Pictures or Videos of Kids Allowed</b><br/>
                The community does not want to see any pictures or videos of people under the
                age of 18 on Vinegar Strokes. This policy will be strictly enforced.
                <br/><br/>
                <b>You Must Be At Least 18 Years Old</b>
                <br/>To join Vinegar Strokes you need to be at least 18 years old. Each person
                    matures at a different speed and you might feel you are ready to join a site
                    like Vinegar Strokes when you are 17, but you need to wait until your 18th
                    birthday to join.
                <br/><br/>
                <b>Think Twice Before You Post</b>
                <br/>You can remove any content on your profile at any time but you should think
                    twice though before you start or comment on any discussions. To maintain the
                    integrity of discussions we don’t remove the original post or any comments
                    unless they break one of our guidelines or our Terms and Conditions
                <br/><br/>
                <b>Only Post Pictures and Videos You Own or Have The Rights to Post</b><br/>
                You should only upload pictures and videos which you have taken or which you
                were given permission to post on Vinegar Strokes.
                <br/>Has someone taken one of your pictures or videos and uploaded it to Vinegar
                    Strokes without your permission? Check out our Copyright Policy for more
                    information on how to report copyright infringements.
                <br/><br/>
                <b>Private Conversations Should Be Kept Private
                </b>
                <br/>Private conversations between two members should stay private, unless of
                    course, the two people who were having the conversation both agree to it being
                    posted publicly.
                <br/><br/>
                <b>Don’t Out Anyone
                </b><br/>Each person on Vinegar Strokes is comfortable with different pieces of
                    their personal information being revealed. Some people don’t mind you using
                    their full real name and others don’t want you to even use their first name.
                    Please make a conscious effort to respect each person’s comfort level. Also, to
                    protect members of the community, we don’t allow phone numbers or addresses to
                    be posted publicly on Vinegar Strokes unless they are for a business,
                    organization or event.
                <br/><br/>
                <b>Take Everything You Read With A Grain of Salt</b><br/>
                Vinegar Strokes is for entertainment, informational, and educational purposes
                only. What people post is not verified by anyone for its accuracy. What you read
                on Vinegar Strokes should not be construed as actual, professional advice. You
                should always consult a qualified professional.
                <br/><br/>
                <b>Spam and Cross-Posting</b><br/>
                Since nobody likes spam other than spammers, we promise to protect you from it
                to the best of our ability. This includes cross-posting. We will not tolerate
                anyone posting the same message more than 3 times a day anywhere publicly on
                Vinegar Strokes.
                <br/><br/>
                <b>Someone Breaks One Of Our Community Guidelines</b><br/>
                We don’t scan Vinegar Strokes other than for spam or scammers. So if you come
                across something that goes against our community guidelines, it is probably
                because nobody reported it. To report anything that is in violation of our
                community guidelines please use our Contact Form and select Report in the drop
                down box. We will investigate each report and take necessary actions.
                <br/><br/>
                <b>Got Questions or Suggestions?</b><br/>
                If you ever need clarification on any part of the community guidelines or have a
                question, suggestion, a bit of feedback, or a problem with the site, please feel
                free to contact us.
                <br/><br/>
                <b>Changes to the Community Guidelines</b><br/>
                Like Vinegar Strokes itself, the Community Guidelines will evolve over time, so
                make sure to check back often for updates.
            </div>
        </div>

        let statement = <div>
            <div className="titleSection">2257 Statement</div>
            <div className="section">
                <span className="subsection">18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement</span>
                <br/><br/>All models and other persons that appear in any visual depiction of
                    actual sexually explicit conduct appearing or otherwise contained in this
                    Website were over the age of eighteen years at the time of the creation of such
                    depictions.
                <br/><br/>Some visual depictions displayed on this Website are exempt from the
                    provision of 18 U.S.C. section 2257 and 28 C.F.R. 75 because said visual
                    depictions do not consist of depictions of conduct as specifically listed in 18
                    U.S.C section 2256 (2) (A) through (D), but are merely depictions of
                    non-sexually explicit nudity, or are depictions of simulated sexual conduct, or
                    are otherwise exempt because the visual depictions were created prior to July 3,
                    1995.
                <br/><br/>With respect to all visual depictions displayed on this website,
                    whether of actual sexually explicit conduct, simulated sexual content or
                    otherwise, all persons in said visual depictions were at least 18 years of age
                    when said visual depictions were created.
                <br/><br/>The owners and operators of this Website may not be the primary
                    producer (as that term is defined in 18 USC section 2257) of any of the visual
                    content contained in the Website. Please direct questions pertaining to content
                    on this website to:
                <br/><br/>
                <a href="mailto:hello@vinegarstrokes.com">
                    <u>hello@vinegarstrokes.com</u>
                </a>
                <br/><br/>Original records required pursuant to 18 U.S.C. section 2257 and 28
                    C.F.R. 75 for all materials contained in the website are kept by the following
                    Custodian of Records:
                <br/><br/>Vinegar Strokes
                <br/>422 Richards St. Suite 170
                <br/>Vancouver, BC
                <br/>V6B 2Z4</div>
        </div>

        return (
            <Router history={this.props.history}>
                <div>
                    <div className="terms">
                        <div className="title">Legal</div>
                        <div className="subTitle">Everything you need to know, all in one place.</div>
                        <div className="separator"/>
                        <div className="mainC">
                            <div className="secondC">
                                <div className="aboutContent left">
                                    <NavLink to='/legal/2257' className='tablinks' activeClassName='active'>
                                        2257 Statement
                                    </NavLink>
                                    <NavLink to='/legal/terms' className='tablinks' activeClassName='active'>
                                        Terms &amp; Conditions
                                    </NavLink>
                                    <NavLink to='/legal/privacy' className='tablinks' activeClassName='active'>
                                        Privacy Policy
                                    </NavLink>
                                    <NavLink to='/legal/copyright' className='tablinks' activeClassName='active'>
                                        Copyright
                                    </NavLink>
                                    <NavLink to='/legal/community' className='tablinks' activeClassName='active'>
                                        Community
                                    </NavLink>
                                </div>
                                <Route
                                    exact
                                    path='/legal/2257'
                                    render={() => (
                                    <TermsContent>{statement}</TermsContent>
                                )}/>
                                <Route
                                    exact
                                    path='/legal/terms'
                                    render={() => (
                                    <TermsContent>{general}</TermsContent>
                                )}/>
                                <Route
                                    exact
                                    path='/legal/privacy'
                                    render={() => (
                                    <TermsContent>{privacy}</TermsContent>
                                )}/>
                                <Route
                                    exact
                                    path='/legal/copyright'
                                    render={() => (
                                    <TermsContent>{copyright}</TermsContent>
                                )}/>
                                <Route
                                    exact
                                    path='/legal/community'
                                    render={() => (
                                    <TermsContent>{community}</TermsContent>
                                )}/>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>
            </Router>
        );
    }
}
export default Terms;