import React from 'react';

const TermsContent = ({children}) => {
    return (
        <div className='aboutContent right tabcontent first'>
            {children}
        </div>
    );
};

export default TermsContent;
