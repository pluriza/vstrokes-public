import Previous from "../Paging/Previous";
import Next from "../Paging/Next";
import Page from "../Paging/Page";

export {Previous, Page, Next};