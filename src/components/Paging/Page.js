import React from 'react';
import PropTypes from 'prop-types';

const style = {
  paddingRight: 10,
};

const Page = ({page, currentPage, goTo}) => {
  const isCurrentPage = currentPage === page;
  if (isCurrentPage) {
    style.cursor = 'default';
    style.color = '#000000';
  }
  return (
    <span
      className="uppercase-label-inverse"
      style={style}
      onClick={isCurrentPage ? () => {} : goTo(page)}
    >
      {page}
    </span>
  );
};

Page.propTypes = {
  page: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  goTo: PropTypes.func.isRequired,
};

export default Page;