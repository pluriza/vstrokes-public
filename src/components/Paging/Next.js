import React from 'react';
import PropTypes from 'prop-types';

const style = {
  display: 'inline-block',
  width: 9,
  marginRight: -9,
};

const Next = ({currentPage, totalPages, title, noMoreTitle, goTo}) => {
  const isNotLastPage = currentPage < totalPages;
  if (!isNotLastPage) {
    style.cursor = 'default';
    style.color = '#000000';
  }
  return (
    <span
      className="uppercase-label-inverse"
      style={style}
      title={isNotLastPage ? title : noMoreTitle}
      onClick={isNotLastPage && goTo(currentPage + 1)}
    >
      &gt;
    </span>
  );
};

Next.propTypes = {
  currentPage: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  title: PropTypes.string,
  noMoreTitle: PropTypes.string.isRequired,
  goTo: PropTypes.func.isRequired,
};
Next.defaultProps = {
  title: 'Next page',
};

export default Next;