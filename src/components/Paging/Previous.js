import React from 'react';
import PropTypes from 'prop-types';

const style = {
  paddingRight: 10,
};

const Previous = ({currentPage, title, noMoreTitle, goTo}) => {
  const isNotFirstPage = currentPage > 1;
  if (!isNotFirstPage) {
    style.cursor = 'default';
    style.color = '#000000';
  }
  return (
    <span
      className="uppercase-label-inverse"
      style={style}
      title={isNotFirstPage ? title : noMoreTitle}
      onClick={isNotFirstPage && goTo(currentPage - 1)}
    >
      &lt;
    </span>
  );
};

Previous.propTypes = {
  currentPage: PropTypes.number.isRequired,
  title: PropTypes.string,
  noMoreTitle: PropTypes.string.isRequired,
  goTo: PropTypes.func.isRequired,
};
Previous.defaultProps = {
  title: 'Previous page',
};

export default Previous;