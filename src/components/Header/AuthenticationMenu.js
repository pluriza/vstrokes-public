import React from 'react';
import { Link } from 'react-router-dom';
//import searchIcon from '../../assets/images/SearchIcon.png';

const AuthenticatioMenu = ({ toggleLoginModal }) => {
  const openLoginModal = e => {
    e.preventDefault();
    toggleLoginModal();
  };

  return (
    <div className="login-container">
      <Link className="signup-button" to="/signup">
        Create a Free Account
      </Link>
      <div className="separator" />
      <a onClick={openLoginModal} className="uppercase-label login-label">
        LOGIN
      </a>
      {/* <img src={searchIcon} className="searchIcon" alt="search icon" /> */}
      <span className="uppercase-label language-label">EN</span>
    </div>
  );
};

export default AuthenticatioMenu;
