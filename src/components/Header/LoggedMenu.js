import React from "react";
import { Link } from "react-router-dom";
//import searchIcon from '../../assets/images/SearchIcon.png';
import LoggedMenuOptions from "../../containers/Header/LoggedMenuOptionsContainer";

const LoggedMenu = ({ username, profileImage, history }) => {
  return (
    <div className="logedInContent">
      <Link to="/profile">
        <div className="photo-loged-content">
          <img src={profileImage} title={username} className="photo-loged" alt="profile" />
        </div>
      </Link>
      <div className="menu-loged-content">
        <LoggedMenuOptions history={history}/>
      </div>
      {/* <img src={searchIcon} className="searchIcon" alt="search icon" /> */}
    </div>
  );
};

export default LoggedMenu;
