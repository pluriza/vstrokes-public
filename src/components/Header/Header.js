import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import AuthenticationMenu from '../../containers/Header/AuthenticationMenuContainer';
import logo from '../../assets/images/Logo.png';
import LoggedMenu from '../../containers/Header/LoggedMenuContainer';

class Header extends Component {

  openGetStrokesModal = e => {
    e.preventDefault();
    this.props.toggleGetStrokesModal();
  };

  goFilter = (key, value) => e => {
    e.preventDefault();
    const {history, setFilter} = this.props;
    setFilter(key, value);
    history.push('/');
  };

  goHome = e => {
    e.preventDefault();
    const {history, resetFilters} = this.props;
    resetFilters();
    history.push('/');
  };

  render () {
    const {isLogged, history} = this.props;
    const authentication = !isLogged ? <AuthenticationMenu /> : <LoggedMenu history={history}/>;

    return (
      <div>
        <div className="areaHeader"/>
        <div className="header">
          <div className="header-nav">
            <div className="logo-container" onClick={this.goHome}>
              <img src={logo} className="logo" alt="Vinegar Strokes Logo"/>
            </div>
            <ul className="label-nav">
              <li className="uppercase-label" onClick={this.goHome}>
                  TRENDING
              </li>
              <div className="separator"/>
              <li className="uppercase-label" onClick={this.goFilter('gender', 2)}>
                  WOMEN
              </li>
              <div className="separator"/>
              <li className="uppercase-label" onClick={this.goFilter('gender', 1)}>
                  MEN
              </li>
              <div className="separator"/>
              <li className="uppercase-label" onClick={this.goFilter('gender', 3)}>
                  TRANSGENDER
              </li>
              <div className="separator"/>
              <li className="uppercase-label" onClick={this.goFilter('gender', 4)}>
                  MIX AND MATCH
              </li>
              <div className="separator"/>
              <li className="uppercase-label">
                <Link className="header-link" to="/snaps">SNAPS</Link>
              </li>
            </ul>
            {/*<button className="community-button">COMMUNITY</button>*/}
            {
              isLogged ? (
                <button className="strokes-button" onClick={this.openGetStrokesModal}>
                  Get Strokes Here!
                </button>
              ) : null
            }
          </div>
          <div className="header-right-container">
            {authentication}
          </div>
        </div>
      </div>
    );
  }
}
export default Header;
