import React from 'react';
import { Link } from 'react-router-dom';
import history from "../../libs/history";

const LoggedMenuOptions = ({ destroySession }) => {
  const onLogOutHandler = e => {
    e.preventDefault();
    destroySession();
  };

  const goToBalance = e => {
    history.push("/tokens")
  }

  return (
    <div className="menu-loged tooltip">
      <i className="fa fa-bars " aria-hidden="true">
        <span className="tooltiptext">
          <ul>
            <li onClick={goToBalance}>My balance</li>
            <div className="menu-separator" />
            <li><Link to='/verification'>Verify my age</Link></li>
            <li><Link to='/legal/2257'>Legal</Link></li>
            {/*<li>Settings</li>
            <li>FAQ</li>
            <li>Report a Problem</li>*/}
            <li onClick={onLogOutHandler}>Log Out</li>
          </ul>
        </span>
      </i>
    </div>
  );
};

export default LoggedMenuOptions;
