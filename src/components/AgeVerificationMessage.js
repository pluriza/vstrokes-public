import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

const AgeVerificationMessage = ({ageVerified}) => {
  if (ageVerified) {
    return null;
  }
  return (
    <div className="verificationMessage">
      You must submit <Link to="/verification">age verification</Link> to enable the ability to receive tokens while
      broadcasting.
    </div>
  );
};

AgeVerificationMessage.propTypes = {
  ageVerified: PropTypes.bool,
};

AgeVerificationMessage.defaultProps = {
  ageVerified: false,
};

export default AgeVerificationMessage;
