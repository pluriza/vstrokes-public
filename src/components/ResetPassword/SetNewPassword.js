import React,{ Component } from "react";
import history from "../../libs/history";
import Footer from "../Footer/Footer";
import TextField from 'material-ui/TextField';

class SetNewPassword extends Component {
  state = {
    passwordInput:'',
    confirmPasswordInput: '',
    passwordToken: this.props.match.params.passwordToken
  }

  onResetPassword = e => {
    e.preventDefault();
    return this.props.setNewPassword({
      password: this.state.passwordInput,
      confirmPassword: this.state.confirmPasswordInput,
      token: this.state.passwordToken
    }).then(() => history.push("/"));
  };

  handleChangePassword = (event, value) => this.onChange("passwordInput", value);
  handleChangeConfirm = (event, value) => this.onChange("confirmPasswordInput", value);
  
  onChange = (name, value) => this.setState({ [name]: value });

  render(){
    return (
      <div className="reset-page">
        <form className="form-reset" onSubmit={this.onResetPassword}>
          <h2>Set your new password</h2>
          <h3>Choose a password that you can remember easily</h3>
          <TextField
          style={{width:300, display: 'block', transform: 'translateX(25%)'}}
          type="password"
          hintText="Your new password"
          floatingLabelText="New password"
          value={this.state.passwordInput}
          onChange={this.handleChangePassword}/>
          <TextField
          style={{width:300}}
          type="password"
          hintText="Confirm new password"
          floatingLabelText="Confirm password"
          value={this.state.confirmPasswordInput}
          onChange={this.handleChangeConfirm}/>
          <br />
          <button className="orange-button" type="submit">
            CHANGE PASSWORD
          </button>
        </form>
        <div className="other">
        Tips for choosing a password:<br/><br/>
  •   Try to use a mixture of lowercase and uppercase letters <br/>
  •   Try adding some numbers <br/>
  •   Try to combine two different words that are easy to remember <br/>
  •   Avoid using obvious words such as your pet's, wife's, or children's names <br/>
  •   Avoid using common numbers such as birthdays and phone numbers <br/>
        </div>
        <Footer/>
      </div>
    );
  } 
}

export default SetNewPassword;
