import React from 'react';
import Modal from 'react-modal';
import Form from '../../containers/ResetPassword/FormContainer';

const ResetPassword = ({
  toggleResetPasswordModal,
  isResetPasswordModalOpen,
}) => {
  const onRequestClose = () => toggleResetPasswordModal();
  return (
    <Modal
      className="modal"
      isOpen={isResetPasswordModalOpen}
      contentLabel="Reset-Password-Modal"
      onRequestClose={onRequestClose}
    >
    <div className="closeModal" onClick={onRequestClose}> &times; </div>
      <div className="form-container">
        <h1 className="form-title-label">RESET PASSWORD</h1>
        <Form />
      </div>
    </Modal>
  );
};

export default ResetPassword;
