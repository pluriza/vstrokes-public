import React,{ Component } from "react";
import history from "../../libs/history";
import TextField from 'material-ui/TextField';

class Form extends Component {
  state = {
    emailInput: ''
  }

  handleChangeEmail = (event, value) => this.onChange("emailInput", value);
  
  onChange = (name, value) => this.setState({ [name]: value });

  onResetPassword = e => {
    e.preventDefault();
    return this
      .props
      .requestResetPassword({email: this.state.emailInput})
      .then(() => history.push("/"));
  };

  onCancelResetPassword = e => {
    e.preventDefault();
    return this
      .props
      .toggleResetPasswordModal();
  };

  render() {
    return (
      <form onSubmit={this.onResetPassword}>
        <div className="resetMessage">
          Enter the email address you used to sign-up for your account and we'll send you
          instructions to reset the password.
        </div>
          <TextField
          hintText="Enter your full email address here"
          floatingLabelText="Email"
          fullWidth={true}
          value={this.state.emailInput}
          onChange={this.handleChangeEmail}/>
        <div className="buttons">
          <button onClick={this.onCancelResetPassword} className="cancel-button">
            Cancel
          </button>
          <button className="orange-button" type="submit">
            SUBMIT
          </button>
        </div>
      </form>
    );
  }
}

export default Form;
