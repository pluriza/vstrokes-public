import React, { Component } from 'react';
import Footer from '../Footer/Footer'
import Snap from './Snap';
import {Previous, Page, Next} from '../Paging';
import SideMenu from "../SideMenu/SideMenu";

class Snaps extends Component {

  componentDidMount() {
    this.props.getSnaps();
  }

  goToPage = page => () => {
    const {updatePage} = this.props;
    updatePage(page);
  };

  render() {
    const { loading, rows, total, limit, filters, setFilter, resetFilters } = this.props;
    const { page } = filters;
    const snaps = rows.map(snap => <Snap key={snap.id} {...snap} />);
    const totalPages = Math.ceil(total / limit);
    const pages = (new Array(totalPages)).fill(null);
    return (
      <div className="App">
        <div className="grid">
          <div className="body-container">
            {loading && <span className="snaps-loading">Loading snaps...</span>}
            <SideMenu
              filters={filters}
              showTags={false}
              updateFilter={setFilter}
              resetFilters={resetFilters}
            />
            <div className="snaps-container">
              <div>
                {snaps.length > 0 ? snaps : <div className="no-snaps">There are currently no snaps</div>}
              </div>
              <div className="more-container">
                <Previous currentPage={page} noMoreTitle='No more snaps' goTo={this.goToPage}/>
                {pages.map((_, i) => <Page key={i} page={i + 1} currentPage={page} goTo={this.goToPage}/>)}
                <Next currentPage={page} totalPages={totalPages} noMoreTitle='No more snaps' goTo={this.goToPage}/>
              </div>
            </div>
          </div>
          <Footer/>
        </div>
      </div>
    );
  }

}

export default Snaps;