import React from 'react';
import {Link} from "react-router-dom";

const Snap = ({uri, title, username}) => (
  <Link to={`/${username}`}>
    <div className="image-box">
      <img src={uri} className="snap-image" alt={title} title={username} />
    </div>
  </Link>
);

export default Snap;