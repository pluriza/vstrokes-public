import React from "react";
import classNames from "classnames";

const Growler = ({ growler, hideGrowler }) => {
  const growlerClass = classNames("growler", growler.growlerType, {
    "growler--hiding": growler.status === "hide",
    "growler--hidden": growler.status === "hidden"
  });

  return (
    <div
      className={growlerClass}
      onClick={e => {
        e.preventDefault();
        hideGrowler(growler);
      }}
    >
      <span className={`icon ${growler.icon}`} />
      {growler.text}
    </div>
  );
};
export default Growler;
