import React, { Component } from "react";
import moment from "moment";
import TextField from 'material-ui/TextField';

class Agreement extends Component {

  constructor(props) {
    super(props);
    this.state = {
      agreement: '',
      digitalSignature: props.digitalSignature,
      error: false,
      errorMessage: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    const { realName, email, dateOfBirth, history } = this.props;
    if (!realName || !email || !dateOfBirth) {
      history.replace('/verification');
      return;
    }
    this.setState({
      digitalSignature: nextProps.digitalSignature,
      agreement: this.generateAgreementText(realName, email, moment(dateOfBirth).format("YYYY/MM/DD"))
    });
  }

  componentDidMount() {
    const { realName, email, dateOfBirth, history } = this.props;
    if (!realName || !email || !dateOfBirth) {
      history.replace('/verification');
      return;
    }
    this.setState({
      agreement: this.generateAgreementText(realName, email, moment(dateOfBirth).format("DD/MM/YYYY"))
    });
  }

  handleChangeSign = (event, value) => this.setState({ 'digitalSignature': value });

  agree = event => {
    event.preventDefault();
    const { digitalSignature } = this.state;
    if(!digitalSignature) {
      this.setState({error: true, errorMessage: 'You must sign the agreement.'});
      return;
    }
    this.props.set(digitalSignature);
  };

  generateAgreementText(realName, email, birthdate) {
    const currentDate = moment().format("DD/MM/YYYY");
    return (
      <div>
        <p>PREFORMER AGREEMENT: 2257 COMPLIANT AGREEMENT</p>
        <p>This Agreement is entered into as of today’s date between <strong>{realName}</strong> and Vinegar Strokes, operator of vinegarstrokes.com (the “Site”).</p>
        <p><strong>Warrant and Acknowledgement.</strong> I, <strong>{realName}</strong>, (referred to a “I”, “me” or “Performer”) hereby certify and warrant that my date of birth is <strong>{birthdate}</strong> and that I am at least eighteen (18) years of age or the age of majority in my geographic location, whichever is higher (the “Age of Majority”). I further warrant that all identification information previously provided, provided contemporaneously with this Agreement, or to be provided in the future to the Site is true and correct, and is current (not expired), validly issued government identification showing my actual date of birth and a recent, recognizable photo of me. My digital signature at the bottom of this agreement certifies that the above information, and any information provided or to be provided to the Site, is true and correct. I acknowledge and agree that I am entering into this Agreement of my own free will as a consenting adult.</p>
        <p><strong>Tipping.</strong> I acknowledge that I use the interactive chat service provided through the Site. I have requested to participate in a program through which the Site will allow other users of the Site to provide me voluntary “tips” in the form of virtual currency to be credited to my user account with the Site. So long as Performer is in full compliance with the terms of this Agreement, the Site agrees to permit the use of the Site by the Performer. I understand that the Site cannot and does not guarantee that I will actually receive any tips at any time, and that all tipping is optional at the users’ discretion, including whether or not to tip and the amount of the tip; although the Site may from time to time allow me to set a minimum tip amount and schedule chat sessions for which I can require a preset tip amount. I understand and agree that anyone in any jurisdiction will be able to view my use of the Site.</p>
        <p><strong>Payment Instructions.</strong> I acknowledge and agree that it shall be my responsibility to provide payment instructions to be used for disbursal of any amounts that the Site may send to me. I further acknowledge and agree that any changes that I request to such payment instructions may take up to 2 business days to take effect and that any payments I request within less than 2 business days after a requested change to my payment instructions may be sent using the previously provided payment instructions. I hereby authorize the Site to send payments to me in accordance with the payment instructions that are provided for my account.</p>
        <p><strong>Shared Accounts.</strong> In the event that I share an account with any other user, no matter how briefly, at any time and for any reason, I understand that all tokens deposited by users of the Site into such account shall only be paid to the payment details on file for the holder of the account. I understand and agree that any agreement between the accountholder and me for shared compensation with respect to any tips received while sharing such account will be solely between me and the other user. I hereby acknowledge that the Site will bear no responsibility to me in the event that the other user does not compensate me the amount I believe I am owed or at all. I further acknowledge that the Site will not share with me the personal details of the accountholder or where payment is sent for the accountholder for any reason.</p>
        <p><strong>Relationship with Site – No Agency.</strong> I understand and acknowledge that I am not an employee or agent of the Site, its owner company, or any of its affiliates. I understand and agree that I am an independent contractor. This means that I am responsible for paying all withholding, FICA and other applicable taxes on any “tips” received in connection with my use of the Site and the Services (defined below); and the Site does not provide, and I am not entitled to, any workman’s compensation, unemployment insurance, medical benefits, paid time off, or other benefits offered to employees of the Site. Subject to applicable laws, the Site’s Terms & Conditions, and this Agreement, I understand I have sole discretion to determine how, when and where to use the Site and perform the Services (defined below).</p>
        <p><strong>Release.</strong> Performer releases Site, its employees, agents, attorneys, assigns and licensees (the “Releasees") from any and all claims arising out of this Agreement and the Services (defined below) including, without limitation, right of publicity claims, invasion of privacy claims, defamation claims, sexual harassment claims, injuries (both physical and emotional), negligence, intellectual property, claims relating to disease or illness (including STD’s), pregnancy, and all other such claims whether or not listed above. I agree to indemnify and hold harmless the Site, its employees, affiliates and other related workers and entities from any liability arising out of this Agreement or my performance of the Services. I agree that in the event that I appear on camera with any third person, as permitted hereunder, I am doing so at my own discretion and risk and I acknowledge that the Site will not, and is under no obligation to, do any medical testing of such third party. I further release the Releasees from any claim in connection with any payment to my account or the account of a third party on whose account I may appear so long as the Releasees make a good faith effort to deliver payment using the payment instructions on file at the time of payment for the applicable account.</p>
        <p>
          <strong>Waiver of Civil Code Section 1542.</strong> <em>California Civil Code</em> § 1542 provides:
        </p>
        <blockquote><strong>A GENERAL RELEASE DOES NOT EXTEND TO CLAIMS WHICH THE CREDITOR DOES NOT KNOW OR SUSPECT TO EXIST IN HIS OR HER FAVOR AT THE TIME OF EXECUTING THE RELEASE, WHICH IF KNOWN BY HIM OR HER MUST HAVE MATERIALLY AFFECTED HIS SETTLEMENT WITH THE DEBTOR.</strong></blockquote>
        <p>Except as otherwise provided in this Agreement, after consultation with counsel, Performer hereby waives any and all rights and benefits conferred by the provisions of California Civil Code §1542 and any similar law of any state or territory of the United States or other jurisdiction, with respect to all claims within the scope of the releases granted above that are presently unknown or unsuspected. Performer acknowledges that this waiver is a material inducement for entering into this Agreement and that the Site would not have entered into this Agreement in the absence of this waiver.</p>
        <p><strong>Obligations.</strong> I understand that I have no obligation to appear on the Site at any time, whether in exchange for tips or otherwise, and that I shall be in sole control of when I access the Site. I further understand I am in full control of how I use the Site, including ensuring that my use of the Site at all times conforms with applicable local, state and federal law, as well as the Site’s acceptable use policies. I acknowledge and agree that all services provided by me, including but not limited to, live streaming video and similar services (all such material, services and any other material produced by Performer pursuant, in whole or in part, to this Agreement, whether or not of an adult nature, are referred to herein as the “Services”) shall be for the benefit of the Site’s users. I warrant to the Site that my performance of the Services will not violate any laws in the location in which the Services are performed or exhibited. I acknowledge that I am responsible for paying all taxes from monies paid to me by users who choose to provide tips to my user account with the Site. I acknowledge that the Services may be posted on the Site and that anyone over the Age of Majority in his or her location may be able to access and view my Services.</p>
        <p>
          <strong>Rules.</strong> I agree to adhere to the following rules while performing the Services for users of the Site:
        </p>
        <ol>
          <li>I will appear on camera substantially the entire time that I am performing the Services;</li>
          <li>I will not reveal my real name or personal contact information to other users of the Site while performing the Services. I agree that under no circumstances will I communicate with any user of the Site through any means other than through the Site without the express written permission of the Site;</li>
          <li>I will not reveal the location where I provide the Services nor the names, addresses or other information of any of the companies or individuals connected with the Site or my Services;</li>
          <li>I will not promote or discuss any third party or website which is not the Site or which has not been approved in writing by the Site;</li>
          <li>If, at anytime, I feel that someone is requesting that I perform an act in violation of this Agreement, the Site’s acceptable use policies, or applicable law, I will cease to perform the Services for the individual and I will immediately report the incident and that person’s information to the Site;</li>
          <li>I will not hold myself out to anyone as not being the Age of Majority. In the event I believe that someone believes me to be younger than the Age of Majority, I will immediately correct this error by informing the user that I am the Age of Majority;</li>
          <li>I will not tell any user of the Site, nor lead them to believe, that I participate in or simulate any obscene acts, including but not limited to bestiality, necrophilia, child molestation, child pornography, rape, urination, defecation or any other obscene sex acts (the full list of prohibited acts can be found on the Site’s Terms & Conditions); and</li>
          <li>While providing the Services, I agree that I will not appear on camera with any third person without the written permission of the Site, which permission will be contingent on such third party signing a separate performer agreement with the Site.</li>
        </ol>
        <p><strong>Indemnification.</strong> I will indemnify and hold the Site, its agents, affiliated entities, employees, owners and managers, harmless with respect to the provision of the Services and all information I provide to the Site and any of their agents, employees or affiliated entities.</p>
        <p><strong>Rights Granted to the Site.</strong> I hereby grant to the Site the right to distribute my Services through any and all media now existing or hereinafter created including without limitation on the Site. I grant to the Site the perpetual, universal right to record, edit and exploit my Services for purposes of advertising and promoting the Site on which my Services appear and to promote and advertise my Services and to generally promote the Site and its affiliated entities, including, without limitation, other performers who provide similar services. I authorize others to use my name, any and all stage names and aliases, and biography, resume, signature, caricature, voice and likeness (collectively, the “Name and Likeness”) for and in connection with the provision of the Services on websites owned and/or operated by the Site or by third parties, and all advertising (including the Name and Likeness on websites, banner ads, written publications and the like), merchandising, commercial tie-ups, publicity, and other means of exploitation of any and all rights pertaining to the Services and any element thereof. I agree to sign any and all reasonable documentation requested by the Site to perfect the rights granted herein.</p>
        <p>
          <strong>Termination.</strong> This Agreement may be terminated by either party for any reason whatsoever and shall be effective upon delivery of written notice as follows:
          <br /><br />
          To the Site: <em><a href="mailto:hello@vinegarstrokes.com">hello@vinegarstrokes.com</a></em>
          <br /><br />
          To the Performer: <em><strong>{email}</strong></em>
          <br /><br />
          In the event that the Site determines, in the Site’s sole discretion, that I have performed the Services in a way that renders such Services are obscene, defamatory, threatening, in violation of applicable law, in anyway in violation of a third party’s rights or otherwise in violation of this Agreement, I understand and agree that Site shall have the right to immediately terminate this Agreement “for cause” and cancel my account, and in such event I agree to forfeit any tips provided by the Site’s users to my account but not yet claimed.
        </p>
        <p><strong>Severability.</strong> If any provision of this Agreement is found to be unenforceable, the remainder of this Agreement shall be enforced as fully as possible, and the unenforceable provision shall be deemed modified to the limited extent required to permit its enforcement in a manner most closely representing the intention of the parties as expressed herein.</p>
        <p><strong>Jurisdiction/Venue.</strong> Subject in all respect to the arbitration clause below, Performer and the Site irrevocably submit to the jurisdiction of the courts located in Los Angeles County, California, and agree that all disputes arising under this Agreement shall be governed by the laws of the State of California, USA, excluding its conflict of laws provisions. In the event of Performer’s actual, alleged or threatened breach of the Rules provisions of this Agreement (above), the Site shall be entitled to seek injunctive relief against the Performer in any court having competent jurisdiction over the matter.</p>
        <p><strong>Miscellaneous.</strong> Performer and the Site each understands and agrees that any controversy or claim arising out of or relating to this Agreement shall be settled by binding arbitration before the Canadian Arbitration Association using one arbitrator in Vancouver British Columbia, and judgment upon the award rendered by the arbitrator may be entered in any court having jurisdiction. The prevailing party shall be entitled to reimbursement for costs and reasonable attorneys’ fees. The determination of the arbitrator in such proceeding shall be final, binding and non-appealable. In such event Performer and Site each shall be limited to the applicable remedy at law for direct damages (after Performer’s and Site’s good-faith efforts to mitigate such damages), if any, and shall not have the right to seek consequential or punitive damages, to modify, abrogate or rescind this Agreement, or to enjoin or restrain in any way the production, distribution or advertising of the Services.</p>
        <p>By my electronic signature on this Agreement, <strong>I hereby represent, covenant and warrant that I have the ability to grant all rights granted herein, and that there are no agreements or other legal impediments which make my acceptance of this Agreement invalid, unlawful or avoidable.</strong></p>
        <p>I agree that this Agreement is intended to be governed by the Electronic Signatures in Global and National Commerce Act (the “E-Sign Act”) and that by typing my name on the Site below, I intend it to stand in the place of and be equally as binding on me as if I had signed this document manually. I understand that I may retain a paper copy of this transaction for my personal records and that I have the right to remove my consent to using the E-Sign Act by emailing the Site at <a href="mailto:hello@vinegarstrokes.com">hello@vinegarstrokes.com</a>, at which time I will <span style={{textDecoration: 'underline'}}>immediately</span> cease my use of the Site; provided however that I understand and agree that the withdrawal of my consent shall apply only to future services provided by me and will not effect my previous unrestricted grant of rights to the Site, if any, with respect to services previously performed by me.</p>
        <table>
          <thead>
            <tr>
              <th>Performer:</th>
              <th>Site:</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Agreed by E-Signature</td>
              <td>vinegarstrokes.com</td>
            </tr>
            <tr>
              <td>Print: <strong>{realName}</strong></td>
            </tr>
            <tr>
              <td>Date: <strong>{currentDate}</strong></td>
              <td>Date: <strong>{currentDate}</strong></td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }

  render() {
    const { agreement, digitalSignature, error, errorMessage } = this.state;
    return (
      <div className="stepcontent">
        <div className="side">
          <h2>Step 2 - Performer Agreement</h2>
          <p>Please read the following agreement carefully.</p>
          <div className="agreement">
            {agreement}
          </div>
          <form onSubmit={this.agree}>
          <TextField
          required
          hintText="Type your name here"
          floatingLabelText="E-Signature"
          fullWidth={true}
          value={digitalSignature}
          onChange={this.handleChangeSign}/>
            {error && <span style={{color: 'red', fontSize: 14}} >{errorMessage}</span>}
            <button className="button agree" type="submit">
              I agree
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Agreement;