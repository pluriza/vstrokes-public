import React, { Component } from "react";
import { Router, Route, Link, Prompt } from 'react-router-dom';
import Footer from "../Footer/Footer";
import Step from "./Step";
import Person from "./Person";
import Agreement from "./Agreement";
import AgePhotos from "./AgePhotos";
import Summary from "./Summary";

const emptyPerson = {
  realName: '',
  email: '',
  dateOfBirth: '',
  digitalSignature: '',
  acceptedContract: false,
  pictures: []
};

class AgeVerification extends Component {

  state = {
    currentPerson: { ...emptyPerson },
    people: {}
  };

  componentWillReceiveProps(nextProps) {
    const { people } = nextProps;
    this.setState(prevState => ({
      people: { ...people, ...prevState.people }
    }))
  }

  componentDidMount() {
    const { loadVerification, history } = this.props;
    loadVerification().catch(() => {
      history.push('/');
    });
  }

  setCurrentPerson = (realName, email, dateOfBirth) => {
    if (Object.keys(this.state.people).findIndex(pEmail => pEmail === email) > -1) {
      return false;
    }
    this.setState(prevState => ({
      currentPerson: {
        ...prevState.currentPerson,
        realName, email, dateOfBirth
      }
    }));
    const { match, history } = this.props;
    history.push(`${match.url}/agreement`);
    return true;
  };

  setCurrentPersonAgreement = (digitalSignature) => {
    this.setState(prevState => ({
      currentPerson: {
        ...prevState.currentPerson,
        digitalSignature,
        acceptedContract: true
      }
    }));
    const { match, history } = this.props;
    history.push(`${match.url}/pictures`);
  };

  setCurrentPersonPictures = async pictures => {
    const {currentPerson} = this.state;
    this.props.sendVerification({ ...currentPerson, pictures }).then(() => {
      const { match, history } = this.props;
      history.push(`${match.url}/summary`);
      this.setState({ currentPerson: { ...emptyPerson } });
    });
  };

  render() {
    const { match, loading, location: { pathname } } = this.props;
    const { currentPerson, people } = this.state;
    const steps = [
      {
        url: `${match.url}/person`,
        image: <i className="fa fa-handshake-o" aria-hidden="true" />,
        name: 'Add Agreement',
        info: '',
        hidden: pathname !== match.url && pathname !== `${match.url}/summary`
      }/*, {
        url: `${match.url}/pictures`,
        image: <i className="fa fa-id-card" aria-hidden="true" />,
        name: 'Pictures',
        info: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'
      }*/, {
        url: `${match.url}/summary`,
        image: <i className="fa fa-check-square-o" aria-hidden="true" />,
        name: 'Completed Agreements',
        info: '',
        hidden: pathname !== match.url
      }
    ];

    let intro = <div className="stepcontent" style={{textAlign: 'justify'}}>
      To earn tips (strokes) from other Vinegar Strokes members you must first sign the agreement and complete age verification process.
      We require two government issued IDs (suchs as your Passport and Driver's Licence).
      If another performer(s) will be on camera with you, each of them will need to complete their own agreement on your account.
    </div>;
    return (
      <div>
        <Prompt
          when={loading}
          message={location => {
            if (location.pathname.startsWith("/verification")) {
              return true;
            }
            return `Are you sure you want to go to ${location.pathname}? You have not send your changes.`;
          }}
        />
        <Router history={this.props.history}>
          <div className="ageVerification">
            <div className="title"><Link to={match.url}>Age Verification</Link></div>
            <div className="separator" />
            <Route exact path={match.url} render={() => intro}/>
            <Route exact path={`${match.url}/person`}
              render={props => <Person {...props} {...currentPerson} set={this.setCurrentPerson} />}
            />
            <Route exact path={`${match.url}/agreement`}
              render={props => <Agreement {...props} {...currentPerson} set={this.setCurrentPersonAgreement} />}
            />
            <Route exact path={`${match.url}/pictures`}
              render={props => (
                <AgePhotos
                  {...props}
                  {...currentPerson}
                  loading={loading}
                  set={this.setCurrentPersonPictures}
                  remove={this.removeCurrentPersonPictures}
                />
              )}
            />
            <Route exact path={`${match.url}/summary`}
              render={props => <Summary people={people} />}
            />
            <div className="steps">
              {steps.filter(step => !step.hidden).map((step, i) => (
                <Step 
                  key={i}
                  url={step.url}
                  image={step.image}
                  name={step.name}
                  info={step.info}
                />
              ))}
            </div>
          </div>
        </Router>
        <Footer />
      </div>
    );
  }

}

export default AgeVerification;