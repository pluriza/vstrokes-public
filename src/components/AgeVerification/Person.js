import React, { Component } from "react";
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import moment from "moment";

class Agreement extends Component {

  constructor(props) {
    super(props);
    this.state = {
      realName: props.realName,
      email: props.email,
      dateOfBirth: props.dateOfBirth || null,
      error: false,
      errorMessage: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      realName: nextProps.realName,
      email: nextProps.email,
      dateOfBirth: nextProps.dateOfBirth || null,
    });
  }

  generateAgreement = event => {
    event.preventDefault();
    const { realName, email, dateOfBirth } = this.state;
    if (!realName || !email || !dateOfBirth) {
      this.setState({error: true, errorMessage: 'You must fill out your information.'});
      return;
    }
    const age = moment().diff(dateOfBirth,'years');
    if (age < 18) {
      this.setState({error: true, errorMessage: 'You must be over 18 to accept the agreement.'});
      return;
    }
    if (!this.props.set(realName.trim(), email.trim(), dateOfBirth)) {
      this.setState({
        error: true,
        errorMessage: `You've already added a performer with the email "${email}". Please use a diferent one.`
      });
    }
  };

  handleChangeName = (event, value) => this.handleChange("realName", value);
  handleChangeMail = (event, value) => this.handleChange("email", value);

  handleChange = (name, value) => this.setState({ [name]: value });

  onChangeDate = (event, date) => this.setState({ dateOfBirth: date, error: false, errorMessage: '' });

  render() {
    const { realName, email, dateOfBirth, error, errorMessage } = this.state;
    return (
      <div className="stepcontent side">
        <h2>Step 1 - Your Information</h2>
        <p>
          Below, enter your full name and birthdate as they are displayed on your government issued ID.
          <br />
          Please use an email where we can reach you.
        </p>
        <form id="person" onSubmit={this.generateAgreement}>
          <TextField
          required
          hintText="Put your full name"
          floatingLabelText="Real name"
          fullWidth={true}
          value={realName}
          onChange={this.handleChangeName}/>
          <DatePicker
          required
          fullWidth={true}
          autoOk={true}
          floatingLabelText="Date of birth"
          hintText="Birthday"
          openToYearSelection={true}
          value={dateOfBirth}
          onChange={this.onChangeDate}/>
          <TextField
          required
          hintText="Put your e-mail"
          floatingLabelText="E-mail"
          type="email"
          fullWidth={true}
          value={email}
          onChange={this.handleChangeMail}/>
          {error && <div style={{color: 'red', fontSize: 14, maxWidth: 280}} >{errorMessage}</div>}
          <button form="person" className="button" type="submit">
            Show agreement
          </button>
        </form>
      </div>
    );
  }
}

export default Agreement;