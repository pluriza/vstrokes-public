import React, {Component} from "react";

class AgePhotos extends Component {

  constructor(props) {
    super(props);
    this.state = {
      pictures: props.pictures,
      error: false,
      errorMessage: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    const {acceptedContract, history} = this.props;
    if (!acceptedContract) {
      history.replace('/verification');
      return;
    }
    this.setState({
      pictures: nextProps.pictures
    });
  }

  componentDidMount() {
    const {acceptedContract, history} = this.props;
    if (!acceptedContract) {
      history.replace('/verification');
      // return;
    }
  }

  handleImageChange = event => {
    const {files} = event.target;
    Object.keys(files).forEach(index => {
      let file = files[index];
      const reader = new FileReader();
      reader.onloadend = () => {
        file.preview = reader.result;
        this.setState(prevState => ({
          pictures: [
            ...prevState.pictures,
            file
          ],
          error: prevState.pictures.length < 1
        }));
      };
      reader.readAsDataURL(file);
    });
    event.target.value = null
  };

  finish = event => {
    event.preventDefault();
    if (!this.props.loading) {
      const {pictures} = this.state;
      if (pictures.length < 2) {
        this.setState({error: true, errorMessage: 'You must upload at least two pictures.'});
        return;
      }
      this.props.set(pictures);
    }
  };

  remove = index => event => this.setState(prevState => ({
    pictures: [
      ...prevState.pictures.slice(0, index),
      ...prevState.pictures.slice(index + 1)
    ]
  }));

  render() {
    const {pictures, error, errorMessage} = this.state;
    return (
      <div className="stepcontent">
        <div className="side">
          <h2>Step 3 - Upload Proof of Age Photos</h2>
          <button className='step'>
            <div className="stepImage">
              <i className="fa fa-user-o" aria-hidden="true"/>
              <i className="fa fa-id-card" aria-hidden="true"/>
            </div>
            <div className="stepName">IDENTIFICATION & FACE</div>
            <div className="stepInfo">
              You'll need to upload two separate photos;
              one of your government-issued ID, and one of you holding the same ID next to your face.
              Each photo should be clear and easily distinguishable.
              Please - no hats, glasses, or anything else that might make it difficult to recognize you.
            </div>
            <label className="stepButton" htmlFor="picture">Upload</label>
          </button>
        </div>
        <div className="agreements-list">
          <ol>
            {pictures.map((picture, i) => (
              <li key={i}>
                {picture.name} &nbsp;
                {!picture.id &&
                <i className="fa fa-times" aria-hidden="true" onClick={this.remove(i)}/>}
              </li>
            ))}
          </ol>
        </div>
        <input
          style={{display: 'none'}}
          multiple
          id="picture"
          type="file"
          onChange={this.handleImageChange}
        />
        {error && <div style={{color: 'red', fontSize: 16, textAlign: 'center', margin: 'inherit'}} >{errorMessage}</div>}
        <div className="buttons">
          <a className="next" onClick={this.finish}>{this.props.loading ? "Submiting..." : "Submit for Review"}</a>
        </div>
      </div>
    );
  }
}

export default AgePhotos;