import React from 'react';
import { NavLink as Link } from 'react-router-dom';

const Step = ({
  image,
  name,
  info,
  url
}) => {
  return (
    <Link to={url} className='step' activeClassName='active'>
      <div className="stepImage">{image}</div>
      <div className="stepName">{name}</div>
      <div className="stepInfo">{info}</div>
    </Link>
  );
};

export default Step;