import React, { Component } from "react";

class Summary extends Component {

  verifiedStatusIcon(status = 'pending') {
    return ({
      pending: 'clock-o',
      rejected: 'times',
      accepted: 'check',
    })[status];
  }

  verifiedStatusText(status = 'pending') {
    return ({
      pending: 'Awaiting Approval',
      rejected: 'Rejected',
      accepted: 'Approved',
    })[status];
  }

  render() {
    const {people} = this.props;
    return (
      <div className="stepcontent">
        <div className="side" style={{minHeight: 'auto'}}>
          <h2>Performer Agreements</h2>
          <div className="max-side">
            <table style={{width: '60%'}}>
              <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Date of Birth</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
                {Object.keys(people).map(email => {
                  const person = people[email];
                  return (
                    <tr key={email}>
                      <td>{person.realName}</td>
                      <td>{email}</td>
                      <td>{person.dateOfBirth.format("DD/MM/YYYY")}</td>
                      <td>
                        {this.verifiedStatusText(person.verifiedStatus)}
                        {person.verifiedStatus === 'rejected' && <span>{person.rejectionReason}</span>}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <div className="warningMessage">
          <div className="icon">
            <i className="fa fa-hourglass" aria-hidden="true"></i>
          </div>
          <div className="text">
            <div>
              Only the performers whose accounts have been approved shall be able to preform live on camera as per our terms of service.
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Summary;