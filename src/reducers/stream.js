const streams = (state = {}, action) => {
  const { type } = action;
  const { isStreamOnline } = state;

  switch (type) {
    case "TOGGLE_STREAM":
      return Object.assign({}, state, {
        isStreamOnline: !isStreamOnline
      });
    default:
      return state;
  }
};

export default streams;
