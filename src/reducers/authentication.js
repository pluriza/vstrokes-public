const authentication = (state = {loading: false, error: false}, action) => {
  const { type, payload } = action;

  switch (type) {
    case "CREATE_USER_PENDING":
      return {...state, loading: true};
    case "CREATE_USER_FULFILLED":
      return {
        ...state,
        userId: payload.result,
        loading: false,
        error: false,
        errorMessage: null
      };
    case "CREATE_USER_REJECTED":
      return {
        ...state,
        loading: false,
        error: true,
        errorMessage: payload.response.data.message
      };
    case "CONFIRM_USER_PENDING":
      return state;
    case "CONFIRM_USER_FULFILLED":
      return state;
    case "CREATE_SESSION_PENDING":
      return { ...state, loggingFail: false };
    case "CREATE_SESSION_FULFILLED":
      return Object.assign({}, state, {
        sessionId: payload.token,
        userId: payload.result,
        isLogged: true
      });
    case "CREATE_SESSION_REJECTED":
      const { response: { data } } = payload;
      let { message = 'Your username or password are incorrect' } = data || {};
      if (message === 'User not confirmed') {
        message = 'You must confirm your account before logging in. Please check the email we sent you in the signup process';
      }
      return { ...state, loggingFail: true, loginErrorMessage: message };
    case "CLEAR_LOGIN_FAIL":
      return { ...state, loggingFail: false };
    case "DESTROY_SESSION":
      return Object.assign({}, state, {
        sessionId: "",
        userId: "",
        isLogged: false
      });
    default:
      return state;
  }
};

export default authentication;
