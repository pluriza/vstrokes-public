const user = (state = {}, action) => {
  const {type, payload} = action;

  switch (type) {
    case "REQUEST_PASSWORD_RESET_PENDING":
      return state;
    case "REQUEST_PASSWORD_RESET_FULFILLED":
      return state;
    case "REQUEST_PASSWORD_RESET_REJECTED":
      return state;
    case "GET_USER_PROFILE_INFO_FULFILLED":
      return {
        ...state,
        profile: payload.profile,
        userId: payload.user.id,
        username: payload.user.username
      };
    case "SEND_USER_IMAGE_PROFILE":
      return {
        ...state,
        profileImageLoading: true
      };
    case "POST_USER_IMAGE_PROFILE":
      return {
        ...state,
        profileImage: payload.profileImage,
        profileImageLoading: false
      };
    case "SEND_USER_IMAGE_COVER":
      return {
        ...state,
        coverImageLoading: true
      };
    case "POST_USER_IMAGE_COVER":
      return {
        ...state,
        coverImage: payload.coverImage,
        coverImageLoading: false
      };
    case "GETTING_USER_IMAGE_PROFILE":
      return {
        ...state,
        profileImage: '',
      };
    case "GET_USER_IMAGE_PROFILE":
      return {
        ...state,
        profileImage: payload.profileImage
      };
    case "GETTING_USER_IMAGE_COVER":
      return {
        ...state,
        coverImage: '',
      };
    case "GET_USER_IMAGE_COVER":
      return {
        ...state,
        coverImage: payload.coverImage
      };
    case "GETTING_USER_OTHER_IMAGE_PROFILE":
      return {
        ...state,
        profileImage: '',
      };
    case "GET_USER_OTHER_IMAGE_PROFILE":
      return {
        ...state,
        profileImage: payload.profileImage
      };
    case "GETTING_USER_OTHER_IMAGE_COVER":
      return {
        ...state,
        coverImage: '',
      };
    case "GET_USER_OTHER_IMAGE_COVER":
      return {
        ...state,
        coverImage: payload.coverImage
      };
    case "POST_DESCRIPTION_ME":
      return {
        ...state,
        description: payload.description
      };
    default:
      return state;
  }
};

export default user;
