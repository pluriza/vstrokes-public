const modals = (state = {}, action) => {
  const { type, payload } = action;
  const {
    isTermsModalOpen,
    isLoginModalOpen,
    isResetPasswordModalOpen,
    isUploadPictureModalOpen,
    isGetStrokesModalOpen,
    isStrokeMeModalOpen,
    isDescriptionMeModalOpen,
    isCreateRoomModalOpen,
    isClosingRoomModalOpen,
    isAlbumsModalOpen,
    isPayInfoToken,
    isDetailAlbumModalOpen,
    isUploadPicturePostModalOpen,
    isGalleryModalOpen,    
  } = state;

  switch (type) {
    case 'TOGGLE_TERMS_MODAL':
      return Object.assign({}, state, {
        isTermsModalOpen: !isTermsModalOpen,
      });
    case 'TOGGLE_LOGIN_MODAL':
      return Object.assign({}, state, {
        isLoginModalOpen: !isLoginModalOpen,
      });
    case 'TOGGLE_RESET_PASSWORD_MODAL':
      return Object.assign({}, state, {
        isResetPasswordModalOpen: !isResetPasswordModalOpen,
      });
    case 'TOGGLE_UPLOAD_PICTURE_MODAL':
      return Object.assign({}, state, {
        isUploadPictureModalOpen: !isUploadPictureModalOpen,
      });
    case 'TOGGLE_GET_STROKES_MODAL':
      return Object.assign({}, state, {
        isGetStrokesModalOpen: !isGetStrokesModalOpen,
      });
    case 'TOGGLE_STROKE_ME_MODAL':
      return Object.assign({}, state, {
        isStrokeMeModalOpen: !isStrokeMeModalOpen,
      });
    case 'TOGGLE_DESCRIPTION_ME_MODAL':
      return Object.assign({}, state, {
        isDescriptionMeModalOpen: !isDescriptionMeModalOpen,
      });
    case 'TOGGLE_ROOM_CREATION_MODAL':
      return Object.assign({}, state, {
        isCreateRoomModalOpen: !isCreateRoomModalOpen,
      });
    case 'TOGGLE_ROOM_CLOSING_MODAL':
      return Object.assign({}, state, {
        isClosingRoomModalOpen: !isClosingRoomModalOpen,
      });
    case 'TOGGLE_ALBUMS_MODAL':
      return Object.assign({}, state, {
        isAlbumsModalOpen: !isAlbumsModalOpen,
      });
    case 'TOGGLE_PAY_INFO_TOKEN_MODAL':
      return Object.assign({}, state, {
        isPayInfoToken: !isPayInfoToken,
      });
    case 'TOGGLE_DETAIL_ALBUM_MODAL':
      return Object.assign({}, state, {
        isDetailAlbumModalOpen: !isDetailAlbumModalOpen,
      });
    case 'TOGGLE_UPLOAD_PICTURE_POST_MODAL':
      return Object.assign({}, state, {
        isUploadPicturePostModalOpen: !isUploadPicturePostModalOpen,
      });
    case 'TOGGLE_GALLERY_MODAL':
      return Object.assign({}, state, {
        isGalleryModalOpen: !isGalleryModalOpen,
        pictureId: payload
      });
    default:
      return state;
  }
};

export default modals;
