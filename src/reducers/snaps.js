import {
  GET_SNAPS_PENDING, GET_SNAPS_FULFILLED, GET_SNAPS_REJECTED,
  SET_FILTER, RESET_FILTERS,
} from '../actions/snaps';

const snaps = (state = { loading: false, rows: [], total: 0, limit: 1, filters: { page: 1 } }, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_SNAPS_PENDING:
      return {
        ...state,
        loading: true,
      };
    case GET_SNAPS_FULFILLED:
      return {
        ...state,
        loading: false,
        rows: payload.rows,
        total: payload.count,
        limit: payload.limit,
      };
    case GET_SNAPS_REJECTED:
      return {
        ...state,
        loading: false,
      };
    case SET_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          ...payload,
        },
      };
    case RESET_FILTERS:
      return {
        ...state,
        filters: {
          page: state.filters.page,
        },
      };
    default:
      return state;
  }
};

export default snaps;