import { merge } from 'ramda';

export default function entities(state = { loading: false }, action) {
  if (action.type === "GET_USER_IMAGE_PROFILE" || action.type === "POST_USER_IMAGE_PROFILE") {
    return {
      ...state,
      users: {
        ...state.users,
        [action.payload.userId]: {
          ...state.users[action.payload.userId],
          profileImage: action.payload.profileImage
        }
      }
    };
  }
  if (action.type === 'GET_PROFILE_PENDING') {
    return {
      ...state,
      loading: true,
    }
  }
  if (action.payload && action.payload.entities !== undefined) {
    return {
      ...merge(state, action.payload.entities),
      loading: false,
    };
  }
  return state;
}
