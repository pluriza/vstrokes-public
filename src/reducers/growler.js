const growler = (state = {}, action) => {
  const { type, text, growlerType } = action;
  switch (type) {
    case "GROWLER_SHOW":
      return Object.assign({}, state, {
        text,
        growlerType,
        status: "show"
      });
    case "GROWLER_HIDE":
      return Object.assign({}, state, {
        status: "hide"
      });
    case "GROWLER_HIDED":
      return Object.assign({}, state, {
        status: "hidden"
      });
    default:
      return state;
  }
};

export default growler;
