const post = (state = {}, action) => {
  const { type, payload } = action;

  switch (type) {
    case 'GET_POSTS_PENDING':
      return state;
    case 'GET_POSTS_FULFILLED':
      return Object.assign({}, state, {
        postIds: payload.result,
      });
    case 'GET_POSTS_REJECTED':
      return state;
    case 'GET_POSTS_USER':
      return Object.assign({}, state, {
        postsUser: payload.postsUser,
      });
    default:
      return state;
  }
};

export default post;
