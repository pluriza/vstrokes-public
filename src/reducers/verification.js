import { LOAD_VERIFICATION, SENDING_VERIFICATION, SENT_VERIFICATION } from '../actions/verification';

const verification = (state = {
  loading: false,
  people: {},
  pictures: [],
  ageVerified: false
}, { type, payload }) => {
  switch(type) {
    case LOAD_VERIFICATION:
      return { ...state, ...payload };
    case SENDING_VERIFICATION:
      return { ...state, loading: true };
    case SENT_VERIFICATION:
      const { person } = payload;
      return {
        ...state,
        loading: false,
        people: {
          ...state.people,
          [person.email]: person
        }
      };
    default:
      return state;
  }
};

export default verification;
