import { combineReducers } from "redux";
import growler from "./growler";
import authentication from "./authentication";
import albums from "./albums";
import post from "./post";
import modals from "./modals";
import user from "./user";
import stream from "./stream";
import entities from "./entities";
import token from "./token";
import chat from "./chat";
import room from "./room";
import rooms from "./rooms";
import tags from "./tags";
import verification from "./verification";
import snaps from "./snaps";

export default combineReducers({
  authentication,
  albums,
  post,
  modals,
  user,
  entities,
  growler,
  stream,
  token,
  chat,
  room,
  rooms,
  tags,
  verification,
  snaps,
});
