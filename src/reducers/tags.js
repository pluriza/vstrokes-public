import { combineReducers } from "redux";
import {
  GET_TRENDING_TAGS_PENDING, GET_TRENDING_TAGS_FULFILLED, GET_TRENDING_TAGS_REJECTED,
  GET_TAGS_PENDING, GET_TAGS_FULFILLED, GET_TAGS_REJECTED,
} from '../actions/tags';

const trending = (state = [], action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_TRENDING_TAGS_PENDING:
      return state;
    case GET_TRENDING_TAGS_FULFILLED:
      return [ ...payload ];
    case GET_TRENDING_TAGS_REJECTED:
      return state;
    default:
      return state;
  }
};

const all = (state = [], action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_TAGS_PENDING:
      return state;
    case GET_TAGS_FULFILLED:
      return [ ...payload ];
    case GET_TAGS_REJECTED:
      return state;
    default:
      return state;
  }
};

export default combineReducers({ trending, all });