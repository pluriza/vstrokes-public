import {
  LOAD_ROOMS, CLEARED_LOAD_ROOMS_TIMEOUT,
  GET_ROOMS_PENDING, GET_ROOMS_FULFILLED, GET_ROOMS_REJECTED,
  SET_FILTER, RESET_FILTERS,
} from '../actions/rooms';

const rooms = (state = { timeout: null, loading: false, data: [], total: 0, limit: 1, filters: { page: 1 } }, action) => {
  const { type, payload } = action;
  switch (type) {
    case LOAD_ROOMS:
      // window.clearTimeout(state.timeout);
      return {
        ...state,
        timeout: payload
      };
    case CLEARED_LOAD_ROOMS_TIMEOUT:
      return {
        ...state,
        timeout: null
      };
    case GET_ROOMS_PENDING:
      return {
        ...state,
        loading: true,
      };
    case GET_ROOMS_FULFILLED:
      return {
        ...state,
        loading: false,
        data: payload.users,
        total: payload.count,
        limit: payload.limit,
      };
    case GET_ROOMS_REJECTED:
      return {
        ...state,
        loading: false
      };
    case SET_FILTER:
      return {
        ...state,
        filters: {
          ...state.filters,
          ...payload,
        },
      };
    case RESET_FILTERS:
      return {
        ...state,
        filters: {
          page: state.filters.page,
        },
      };
    default:
      return state;
  }
};

export default rooms;