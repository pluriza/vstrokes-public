const post = (state = {}, action) => {
  const {type, payload} = action;

  switch (type) {
    case 'CREATE_ALBUM':
      return Object.assign({}, state, {
        created: payload.pending,
      });
    case 'REQUEST_ALBUMS':
      return Object.assign({}, state, {
        loading: true
      });
    case 'GET_ALBUMS':
      return Object.assign({}, state, {
        albums: payload.albums,
        loading: false
      });
    case 'CREATE_PICTURE_ALBUM':
      return state;
    case 'GET_ALBUM':
      return Object.assign({}, state, {
        albumActive: payload.album
      });
    case 'GET_PICTURES_ALBUM':
      return Object.assign({}, state, {
        albumActive: {
          pictures: payload.pictures
        }
      });
    case 'GETTING_PICTURES_ALL':
      return Object.assign({}, state, {
        picturesAll: [],
      });
    case 'GET_PICTURES_ALL':
      return Object.assign({}, state, {
        picturesAll: payload.picturesAll
      });
    default:
      return state;
  }
};

export default post;
