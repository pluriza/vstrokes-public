import {
  GET_WALLET_FULFILLED,// BUY_TIPS_FULFILLED,
  UPDATE_BUY_TOKENS_INFO_PENDING, UPDATE_BUY_TOKENS_INFO_FULFILLED,
  GET_PURCHASES_FULFILLED, CLEAR_PURCHASES,
  REQUEST_PAYMENT_PENDING, REQUEST_PAYMENT_FULFILLED, REQUEST_PAYMENT_REJECTED,
} from '../actions/token';

const token = (state = { buyInfo: {}, purchases: [], count: 0, withdrawable: 0 }, action) => {
  const {type, payload} = action;

  switch (type) {
    case 'SAVE_COUNT_STROKES':
      return Object.assign({}, state, {count: payload.count});
    case 'SAVE_TOTAL_STROKES_PRICES':
      return Object.assign({}, state, {total: payload.total});
    case 'GET_TOTAL_STROKES_PRICES':
      return state;
    /*case BUY_TIPS_FULFILLED:
      return {
        ...state,
        count: payload.tokenCount,
      };*/
    case GET_WALLET_FULFILLED:
      return {
        ...state,
        count: payload.tokenCount,
        withdrawable: payload.withdrawable,
      };
    case UPDATE_BUY_TOKENS_INFO_PENDING:
      return {
        ...state,
        buyInfo: {
          ...state.buyInfo,
          loading: true,
        },
      };
    case UPDATE_BUY_TOKENS_INFO_FULFILLED:
      return {
        ...state,
        buyInfo: {
          ...state.buyInfo,
          loading: false,
          ...payload,
        },
      };
    case GET_PURCHASES_FULFILLED:
      return {
        ...state,
        purchases: [...payload.data],
      };
    case CLEAR_PURCHASES:
      return {
        ...state,
        purchases: [],
      };
    case REQUEST_PAYMENT_PENDING:
      return {
        ...state,
        withdrawalPending: true,
        withdrawalMessage: null,
      };
    case REQUEST_PAYMENT_FULFILLED:
      return {
        ...state,
        withdrawalPending: false,
        withdrawalMessage: null,
      };
    case REQUEST_PAYMENT_REJECTED:
      return {
        ...state,
        withdrawalPending: false,
        withdrawalMessage: payload.response.data.message,
      };
    default:
      return state;
  }
};

export default token;
