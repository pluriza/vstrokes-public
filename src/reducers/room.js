import {
  CREATE_ROOM_PENDING,
  CREATE_ROOM_FULFILLED,
  CREATE_ROOM_REJECTED,
  GET_ROOM_PENDING,
  GET_ROOM_FULFILLED,
  GET_ROOM_REJECTED,
  DELETE_ROOM_PENDING,
  DELETE_ROOM_FULFILLED,
  DELETE_ROOM_REJECTED,
  RESTORE_ROOM,
  ADD_GOAL_FULFILLED,
  GET_GOAL_FULFILLED
} from '../actions/room';

const room = (state = {
  loading: false,
  active: false
}, action) => {
  const {type, payload} = action;
  switch (type) {
    case CREATE_ROOM_PENDING:
      return {
        ...state,
        loading: true,
        active: false
      };
    case GET_ROOM_PENDING:
      return {
        ...state,
        loading: true,
        active: false
      };
    case DELETE_ROOM_PENDING:
      return {
        ...state,
        loading: true,
        active: true
      };
    case CREATE_ROOM_FULFILLED:
      return {
        ...state,
        loading: false,
        active: true,
        ...payload
      };
    case GET_ROOM_FULFILLED:
      return {
        ...state,
        loading: false,
        active: true,
        ...payload,
        tags: payload.Tags,
        goals: payload.RoomGoals
      };
    case DELETE_ROOM_FULFILLED:
      return {loading: false, active: false};
    case CREATE_ROOM_REJECTED:
      return {
        ...state,
        loading: false,
        active: false,
        msg: "You already have a Room active."
      };
    case GET_ROOM_REJECTED:
      return {
        ...state,
        loading: false,
        active: false,
        msg: "You don't have a Room active."
      };
    case DELETE_ROOM_REJECTED:
      return {
        ...state,
        loading: false,
        active: true,
        msg: "Room cannot be deleted"
      };
    case RESTORE_ROOM:
      return {loading: false, active: false};
    case ADD_GOAL_FULFILLED:
      return {
        ...state,
        goals: [
          ...state.goals,
          payload.goal
        ]
      };
    case GET_GOAL_FULFILLED:
      return {
        ...state,
        goals: [
          ...payload.goals
        ]
      };
    default:
      return state;
  }
};

export default room;