const chat = (state = {}, action) => {
  const {type, payload} = action;

  switch (type) {
    case "GET_MESSAGES":
      return {id: payload.id, content: payload.content};
    case "CLEAR_MESSAGES":
      return {id: payload.id, content: []};
    case "SEND_TIPS_REJECTED":
      return {
        ...state,
        content: [
          ...state.content, {
            user: {
              image: '',
              name: ''
            },
            content: payload.response.data.message,
            error: true,
          }

        ]
      };
    case "SEND_TIPS_FULFILLED":
      return {
        ...state,
        content: [
          ...state.content, {
            user: {
              image: '',
              name: 'Bot'
            },
            content: payload.message,
            error: false,
          }
        ]
      };
    default:
      return state;
  }
};

export default chat;
