import http from "../libs/http";
import moment from "moment";

export const LOAD_VERIFICATION = 'LOAD_VERIFICATION';
export const SENDING_VERIFICATION = 'SENDING_VERIFICATION';
export const SENT_VERIFICATION = 'SENT_VERIFICATION';

const baseURL = process.env.REACT_APP_SERVER_URL;

const load = () => async (dispatch, getState) => {
  const { sessionId } = getState().authentication;
  const { data: { people: peopleData, ageVerified } } = await http.get(`/api/verification`, {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
  });
  const people = peopleData.reduce((total, person) => {
    person.dateOfBirth = moment(person.dateOfBirth);
    person.pictures = person.Pictures.map(picture => {
      picture.name = picture.title;
      picture.preview = picture.uri;
      return picture;
    });
    total[person.email] = person;
    return total;
  }, {});
  return dispatch({
    type: LOAD_VERIFICATION,
    payload: { people, ageVerified }
  });
};

const sendPicture = sessionId => async picture => {
  try {
    return await new Promise((resolve, reject) => {
      if (picture.id) {
        resolve({...picture});
      } else {
        const form = new FormData();
        form.append("image", picture);
        const oReq = new XMLHttpRequest();
        const endpoint = `${baseURL}/api/users/pictures?default_album=verification_images`;
        oReq.open("POST", endpoint, true);
        oReq.setRequestHeader("Authorization", `Bearer ${sessionId}`);
        oReq.onload = function(oEvent) {
          if (oReq.status === 200) {
            let res = JSON.parse(this.responseText);
            resolve(res);
          } else {
            reject(new Error(this.statusText));
          }
        };
        oReq.onerror = function () {
          reject(new Error('XMLHttpRequest Error: ' + this.statusText));
        };
        oReq.send(form);
      }
    });
  } catch (error) {
    // console.log("sendPicture.error", error);
  }
};

const sendPerson = sessionId => async person => {
  const { pictures, ...newPerson } = person;
  const storedPictures = await Promise.all(pictures.map(sendPicture(sessionId)));
  const picturesIds = storedPictures.map(picture => picture.id);
  newPerson.dateOfBirth = person.dateOfBirth;
  newPerson.pictures = picturesIds;
  let result;
  if (person.id) {
    result = await http.put(`/api/verification/${person.id}`, {
      optionalHeaders: {
        Authorization: `Bearer ${sessionId}`
      },
      data: newPerson,
    });
  } else {
    result = await http.post(`/api/verification`, {
      optionalHeaders: {
        Authorization: `Bearer ${sessionId}`
      },
      data: newPerson,
    });
  }
  const {data} = result;
  data.dateOfBirth = moment(data.dateOfBirth);
  data.pictures = data.Pictures.map(picture => {
    picture.name = picture.title;
    picture.preview = picture.uri;
    return picture;
  });
  return data;
};

const send = person => async (dispatch, getState) => {
  dispatch({ type: SENDING_VERIFICATION });
  const { sessionId } = getState().authentication;
  const storedPerson = await sendPerson(sessionId)(person);
  return dispatch({
    type: SENT_VERIFICATION,
    payload: { person: storedPerson }
  });
};
/*
const sendMultiplePeople = people => async (dispatch, getState) => {
  dispatch({ type: SENDING_VERIFICATION });
  const { sessionId } = getState().authentication;
  const storedPeople = await Promise.all(Object.keys(people).map(email => {
    const p = people[email];
    return sendPerson(sessionId)(p)
  }));
  const newPeople = storedPeople.reduce((total, person) => {
    total[person.email] = person;
    return total;
  }, {});
  return dispatch({
    type: SENT_VERIFICATION,
    payload: { people: newPeople }
  });
};
*/
export { load, send };