import { PENDING, FULFILLED, REJECTED } from 'redux-promise-middleware';
import http from "../libs/http";

const GET_TAGS = "GET_TAGS";
export const GET_TAGS_PENDING = GET_TAGS + "_" + PENDING;
export const GET_TAGS_FULFILLED = GET_TAGS + "_" + FULFILLED;
export const GET_TAGS_REJECTED = GET_TAGS + "_" + REJECTED;

const GET_TRENDING_TAGS = "GET_TRENDING_TAGS";
export const GET_TRENDING_TAGS_PENDING = GET_TRENDING_TAGS + "_" + PENDING;
export const GET_TRENDING_TAGS_FULFILLED = GET_TRENDING_TAGS + "_" + FULFILLED;
export const GET_TRENDING_TAGS_REJECTED = GET_TRENDING_TAGS + "_" + REJECTED;

const trendingTags = () => {
  return (dispatch, getState) => {
    const { sessionId } = getState().authentication;
    return dispatch({
      type: GET_TRENDING_TAGS,
      payload: {
        promise: http.get('/api/rooms/tags/trending', {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          }
        }).then(response => response.data)
      }
    });
  }
};

const tags = (search = '') => {
  let uri = '/api/rooms/tags';
  if (search.length > 0) uri += `?search=${encodeURI(search)}`;
  return (dispatch, getState) => {
    const { sessionId } = getState().authentication;
    return dispatch({
      type: GET_TAGS,
      payload: {
        promise: http.get(uri, {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          }
        }).then(response => response.data)
      }
    });
  }
};

export { trendingTags, tags };