const getMessages = (payload) => (dispatch, getState) => {
    return dispatch({
        type: 'GET_MESSAGES',
        payload: payload
    })
}

const clearMessages = ()=> (dispatch, getState) => {
    return dispatch({
        type: 'CLEAR_MESSAGES',
        payload: {}
    })
}

export {
    getMessages,
    clearMessages
}