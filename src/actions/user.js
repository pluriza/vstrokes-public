import http from "../libs/http";
import {prop, assoc} from "ramda";
import {normalize} from "normalizr";
import {usersSchema} from "../libs/schemas";
import {toggleResetPasswordModal} from "./modals";
import {showGrowler, hideTimeOutGrowler} from "./growler";

const requestResetPassword = email => {
  return (dispatch, getState) => {
    return dispatch({
      type: "REQUEST_RESET_PASSWORD",
      payload: {
        promise: http
          .post("/api/users/password/generateToken", {
            data: email
          })
          .then(() => {
            dispatch(toggleResetPasswordModal());
            dispatch(
              showGrowler(
                "Your request has been submitted, check you email.",
                "growler--success"
              )
            );
            const {growler} = getState();
            dispatch(hideTimeOutGrowler(growler));
          })
      }
    });
  };
};

const setNewPassword = passwordData => {
  const {password, confirmPassword, token} = passwordData;
  return (dispatch, getState) => {
    return dispatch({
      type: "SET_NEW_PASSWORD",
      payload: {
        promise: http
          .post(`/api/users/password/reset?token=${token}`, {
            data: {password, confirmPassword}
          })
          .then(() => {
            dispatch(
              showGrowler("Your password has been changed.", "growler--success")
            );
            const {growler} = getState();
            dispatch(hideTimeOutGrowler(growler));
          })
      }
    });
  };
};

const updateProfile = profileData => {
  return (dispatch, getState) => {
    const {sessionId} = getState().authentication;
    return dispatch({
      type: "UPDATE_PROFILE",
      payload: {
        promise: http
          .put("/api/users/me", {
            optionalHeaders: {
              Authorization: `Bearer ${sessionId}`
            },
            data: {profile: profileData}
          })
          .then(prop("data"))
      }
    });
  };
};

const getUserProfile = () => {
  return (dispatch, getState) => {
    const {sessionId} = getState().authentication;
    return dispatch({
      type: "GET_PROFILE",
      payload: {
        promise: http
          .get("/api/users/me", {
            optionalHeaders: {
              Authorization: `Bearer ${sessionId}`
            },
            transformResponse: [
              data =>
                assoc(
                  "profile",
                  data.profile,
                  normalize(data.user, usersSchema)
                )
            ]
          })
          .then(prop("data"))
      }
    });
  };
};

const getUserInfo = username => (dispatch, getState) => {
  const {sessionId} = getState().authentication;
  return dispatch({
    type: "GET_USER_PROFILE_INFO",
    payload: {
      promise: http
        .get(`/api/users/${username}`, {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          }
        })
        .then(prop("data"))
    }
  });
};

const postUserImageProfile = (image) => async (dispatch, getState) => {
  const {userId, sessionId} = getState().authentication;
  //const sessionId = `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6ImRlN2RmYzdmLTI3MmEtNGRhYi05MzY0LWRiZjcxY2YzNDViMiIsImV4cGlyYXRpb25EYXRlIjoiMjAxNy0wOC0yN1QyMjoxMDowMS44MzdaIn0.DM1jj3oAaaGSaM2iDavcUP-fk13KV3csRZD9gqAeDUw`;


  /* let form = new FormData();
  form.append("imagesd", image);
  form.append("prueba", 'prueba de body');
  let user_id = 'de7dfc7f-272a-4dab-9364-dbf71cf345b2';
  let album_id = 'a0832d7d-3181-4911-bdbd-e4b76dfdfbd6';
  var endpoint = `http://172.16.1.56:8080/api/users/${user_id}/albums/${album_id}/pictures`;

  const response = await fetch(`${endpoint}`,
  {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${sessionId}`
    },
    body: form
  });
  const data = await response.json();
  console.debug('type service Register', data);

  return dispatch({
    type: "POST_USER_IMAGE_PROFILE",
    payload: {
      profileImage: data
    }
  }) */

  dispatch({type: 'SEND_USER_IMAGE_PROFILE'});

  let form = new FormData();
  form.append("image", image);
  let oReq = new XMLHttpRequest();
  let endpoint = `${http.BASE_URL}/api/users/pictures?default_album=profile_images`;
  oReq.open("POST", endpoint, true);

  oReq.setRequestHeader("Authorization", `Bearer ${sessionId}`);
  oReq.onload = function () {
    if (oReq.status === 200) {
      let res = JSON.parse(oReq.responseText);
      console.log(res);
      return dispatch({
        type: "POST_USER_IMAGE_PROFILE",
        payload: {
          userId,
          profileImage: res.uri
        }
      })
    } else {
      return dispatch({
        type: "POST_USER_IMAGE_PROFILE",
        payload: {
          userId,
          profileImage: ''
        }
      })
    }
  };
  oReq.send(form);
};

const postUserImageCover = (image) => async (dispatch, getState) => {
  const {sessionId} = getState().authentication;
  dispatch({type: 'SEND_USER_IMAGE_COVER'});
  let form = new FormData();
  form.append("image", image);
  let oReq = new XMLHttpRequest();
  let endpoint = `${http.BASE_URL}/api/users/pictures?default_album=cover_images`;
  oReq.open("POST", endpoint, true);

  oReq.setRequestHeader("Authorization", `Bearer ${sessionId}`);
  oReq.onload = function () {
    if (oReq.status === 200) {
      let res = JSON.parse(oReq.responseText);
      console.log(res);
      return dispatch({
        type: "POST_USER_IMAGE_COVER",
        payload: {
          coverImage: res.uri
        }
      })
    } else {
      return dispatch({
        type: "POST_USER_IMAGE_COVER",
        payload: {
          coverImage: ''
        }
      })
    }
  };
  oReq.send(form);
};

const getUserImageProfile = () => async (dispatch, getState) => {
  await dispatch({ type: 'GETTING_USER_IMAGE_PROFILE' });
  const {userId, sessionId} = getState().authentication;
  let res = await http.get("/api/users/pictures?default_album=profile_images&current=true", {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
  });
  //const res = {uri: 'https://cdn.pixabay.com/photo/2016/03/28/12/35/cat-1285634_960_720.png'};
  if (res.data[0]) {
    res = res.data[0];
  } else {
    res = {uri: ''};
  }
  return dispatch({
    type: "GET_USER_IMAGE_PROFILE",
    payload: {
      userId,
      profileImage: res.uri
    }
  })
};

const getUserImageCover = () => async (dispatch, getState) => {
  await dispatch({ type: 'GETTING_USER_IMAGE_COVER' });
  const {sessionId} = getState().authentication;
  let res = await http.get("/api/users/pictures?default_album=cover_images&current=true", {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
  });
  //const res = {uri: 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg'};
  if (res.data[0]) {
    res = res.data[0];
  } else {
    res = {uri: ''};
  }
  return dispatch({
    type: "GET_USER_IMAGE_COVER",
    payload: {
      coverImage: res.uri
    }
  })
};

const getUserOtherImageProfile = (username) => async (dispatch, getState) => {
  await dispatch({ type: 'GETTING_USER_OTHER_IMAGE_PROFILE' });
  const {sessionId} = getState().authentication;
  let res = await http.get(`/api/users/${username}/pictures?default_album=profile_images&current=true`, {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
  });
  // const res = {uri: 'https://cdn.pixabay.com/photo/2016/03/28/12/35/cat-1285634_960_720.png'};
  if (res.data[0]) {
    res = res.data[0];
  } else {
    res = {uri: ''};
  }
  return dispatch({
    type: "GET_USER_OTHER_IMAGE_PROFILE",
    payload: {
      profileImage: res.uri
    }
  })
};

const getUserOtherImageCover = (username) => async (dispatch, getState) => {
  await dispatch({ type: 'GETTING_USER_OTHER_IMAGE_COVER' });
  const {sessionId} = getState().authentication;
  let res = await http.get(`/api/users/${username}/pictures?default_album=cover_images&current=true`, {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
  });
  // const res = {uri: 'https://cdn.pixabay.com/photo/2016/03/28/12/35/cat-1285634_960_720.png'};
  if (res.data[0]) {
    res = res.data[0];
  } else {
    res = {uri: ''};
  }
  return dispatch({
    type: "GET_USER_OTHER_IMAGE_COVER",
    payload: {
      coverImage: res.uri
    }
  })
};

const postDescriptionMe = (description) => async (dispatch) => {
  //const { sessionId, userId } = getState().authentication;
  return dispatch({
    type: "POST_DESCRIPTION_ME",
    payload: {
      description: description
    }
  })
};

const followUser = (FollowingId, history) => async (dispatch, getState) => {
  const {isLogged, sessionId} = getState().authentication;
  if (!isLogged) {
    history.push('signup');
    return false;
  }
  return await http.post(`/api/follows`, {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
    data: { FollowingId }
  });
};

const unfollowUser = (FollowingId) => async (dispatch, getState) => {
  const {sessionId} = getState().authentication;
  return await http.post(`/api/follows`, {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
    data: { FollowingId }
  });
};

export {
  requestResetPassword,
  setNewPassword,
  getUserProfile,
  updateProfile,
  getUserInfo,
  postUserImageProfile,
  postUserImageCover,
  postDescriptionMe,
  getUserImageProfile,
  getUserImageCover,
  getUserOtherImageProfile,
  getUserOtherImageCover,
  followUser,
  unfollowUser,
};
