let hideTimeOut;
let hiddenTimeOut;

const hideGrowler = () => {
  return dispatch => {
    dispatch({ type: "GROWLER_HIDE" });
    clearTimeout(hiddenTimeOut);
    clearTimeout(hideTimeOut);
    hiddenTimeOut = window.setTimeout(() => {
      dispatch({
        type: "GROWLER_HIDED"
      });
    }, 500);
  };
};

const hideTimeOutGrowler = growler => {
  return dispatch => {
    if (growler && growler.status === "show") {
      clearTimeout(hideTimeOut);
      hideTimeOut = window.setTimeout(() => {
        dispatch({
          type: "GROWLER_HIDE"
        });
      }, 3000);
    }
  };
};

const showGrowler = (text, growlerType) => {
  return {
    type: "GROWLER_SHOW",
    text,
    growlerType // growler--success or growler--error
  };
};

export { hideGrowler, hideTimeOutGrowler, showGrowler };
