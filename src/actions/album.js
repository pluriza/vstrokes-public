import http from "../libs/http";

const baseURL = process.env.REACT_APP_SERVER_URL;

const createAlbum = (album, pictures) => async (dispatch, getState) => {
  const {sessionId, userId} = getState().authentication;
  const {username} = getState().entities.users[userId];

  dispatch({
    type: 'CREATE_ALBUM',
    payload: {
      pending: true,
    }
  });

  const data = await http.post(`/api/users/${username}/albums`, {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
    data: {
      Album: {
        title: album.title,
        content: album.description
      }
    }
  });

  //let ok = send(pictures);
  function send(pictures) {
    let _data = pictures.map(async (picture, i) => {
      return await dispatch(createPictureAlbum(pictures[i], data.data.id));
    });

    console.log(_data);
    console.log(_data.length);
    console.log(pictures.length);
    return _data.length === pictures.length;
  }

  console.log('termino');

  if (send(pictures)) {
    console.log('hola bien');

    console.log('DISPATCH GET ALBUMS');
    dispatch(getAlbums(true));
    return dispatch({
      type: 'CREATE_ALBUM',
      payload: {
        data: data,
        pending: false,
      }
    })
  }
};

const getAlbums = (isMe) => async (dispatch, getState) => {
  dispatch({type: 'REQUEST_ALBUMS'});
  const {sessionId, userId} = getState().authentication;
  // const { username } = getState().entities.users[userId].username;
  // const { username } = getState().user.username;
  let username;

  if (isMe) {
    username = getState().entities.users[userId].username;
  } else {
    username = getState().user.username;
  }

  // if (getState().user.username === '') {
  //     username = getState().entities.users[userId].username;
  // } else {
  //     username = getState().user.username;
  // }

  const data = await http.get(`/api/users/${username}/albums`, {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
  });
  return dispatch({
    type: 'GET_ALBUMS',
    payload: {
      albums: data.data
    }
  })
};

const getAlbum = (id, isMe) => async (dispatch, getState) => {
  const {sessionId, userId} = getState().authentication;

  let username;

  if (isMe) {
    username = getState().entities.users[userId].username;
  } else {
    username = getState().user.username;
  }

  const {data} = await http.get(`/api/users/${username}/albums/${id}?images=true`, {
    optionalHeaders: {
      Authorization: `Bearer ${sessionId}`
    },
  });

  return dispatch({
    type: 'GET_ALBUM',
    payload: {
      album: data
    }
  })
};

const createPictureAlbum = (image, album_id) => async (dispatch, getState) => {
  return new Promise(
    function (resolve, reject) {

      const {sessionId} = getState().authentication;
      //const { username } = getState().entities.users[userId].username;

      let form = new FormData();
      form.append("image", image);
      let oReq = new XMLHttpRequest();
      let endpoint = `${baseURL}/api/users/pictures?album_id=${album_id}`;
      oReq.open("POST", endpoint, true);

      oReq.setRequestHeader("Authorization", `Bearer ${sessionId}`);
      oReq.onload = function () {
        if (oReq.status === 200) {
          let res = JSON.parse(oReq.responseText);
          console.log(res);
          dispatch({
            type: "CREATE_PICTURE_ALBUM",
            payload: res
          });
          resolve(res)
        } else {
          dispatch({
            type: "CREATE_PICTURE_ALBUM",
            payload: {
              payload: ''
            }
          });
          reject(new Error(this.statusText));
        }
      };
      oReq.onerror = function () {
        reject(new Error(
          'XMLHttpRequest Error: ' + this.statusText));
      };
      oReq.send(form);
    }
  )
};

const getPicturesAll = (isMe) => async (dispatch, getState) => {
  await dispatch({ type: 'GETTING_PICTURES_ALL' });
  const {sessionId, userId} = getState().authentication;
  // const { username } = getState().entities.users[userId].username;
  // const { username } = getState().user.username;
  let username;

  if (isMe) {
    username = getState().entities.users[userId].username;
  } else {
    username = getState().user.username;
  }

  // TODO: ENDPOINT NOT WORKING
  let data = {data: []};
  try {
    data = await http.get(`/api/users/${username}/pictures/all`, {
      optionalHeaders: {
        Authorization: `Bearer ${sessionId}`
      },
    });
    console.log(data.data);
  } catch (error) {
    console.log(error);
  }
  return dispatch({
    type: 'GET_PICTURES_ALL',
    payload: {
      picturesAll: data.data
    }
  })
};

export {
  createAlbum,
  getAlbums,
  createPictureAlbum,
  getAlbum,
  getPicturesAll,
};