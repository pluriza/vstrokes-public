import { prop, isEmpty } from "ramda";
import { normalize } from "normalizr";
import { postsSchema } from "../libs/schemas";
import http from "../libs/http";


const getPostsUser = (username) => async (dispatch, getState) =>{
  const { sessionId } = getState().authentication;
  const postsUser = await http
        .get(`/api/users/${username}/posts`, {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          }
        });
  return dispatch({
    type: "GET_POSTS_USER",
    payload: {
      postsUser: postsUser.data.posts
      // postsUser: [
      //   {
      //     GroupId: null,
      //     UserId: "c9d1938c-efeb-43b1-904d-a2532f6d9852",
      //     authorId: "c9d1938c-efeb-43b1-904d-a2532f6d9852",
      //     content: "jajajajj",
      //     createdAt: "2017-08-26T02:26:29.000Z",
      //     groupId: null,
      //     id:"98335d4c-ac9b-46e6-a285-c77bb149652e",
      //     updatedAt:"2017-08-26T02:26:29.000Z",
      //     url:null
      //   }
      // ]
    }
  });
};


const getPosts = (token , user) => {
  return {
    type: "GET_POSTS",
    payload: {
      promise: http
        .get(`/api/users/${user}/posts`, {
          transformResponse: [data => normalize(data.posts, postsSchema)],
          optionalHeaders: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(data => {
          const posts = prop("data")(data);
          if (isEmpty(posts.entities)) return { result: [], entities: { posts: {} } };
          return posts;
        })
    }
  };
};

const createPost = content => {
  return (dispatch, getState) => {
    const { userId, sessionId } = getState().authentication;
    const { username } = getState().entities.users[userId];
    return dispatch({
      type: "CREATE_POST",
      payload: {
        promise: http
          .post("/api/users/posts", {
            data: { userId, content },
            optionalHeaders: {
              Authorization: `Bearer ${sessionId}`
            }
          })
          .then(() => {
            return dispatch(getPosts(sessionId, username));
          })
      }
    });
  };
};

const deletePost = postId => {
  return (dispatch, getState) => {
    const { sessionId, userId} = getState().authentication;
    const { username } = getState().entities.users[userId];
    return dispatch({
      type: "DELETE_POST",
      payload: {
        promise: http
          .delete(`/api/users/posts/${postId}`, {
            optionalHeaders: {
              Authorization: `Bearer ${sessionId}`
            }
          })
          .then(() => {
            return dispatch(getPosts(sessionId, username));
          })
      }
    });
  };
};

const updatePost = (postId, content) => {
  return (dispatch, getState) => {
    const { sessionId, userId} = getState().authentication;
    const { username } = getState().entities.users[userId];
    return dispatch({
      type: "UPDATE_POST",
      payload: {
        promise: http
          .put(`/api/users/posts/${postId}`, {
            data: { content },
            optionalHeaders: {
              Authorization: `Bearer ${sessionId}`
            }
          })
          .then(() => {
            return dispatch(getPosts(sessionId, username));
          })
      }
    });
  };
};


//Comments
const createComment = (parent_id, content, username) => {
  console.log(username);
  return (dispatch, getState) => {
    const { sessionId, userId } = getState().authentication;
    const _usernameLogged = getState().entities.users[userId].username;
    return dispatch({
      type: "CREATE_COMMENT",
      payload: {
        promise: http
          .post(`/api/users/comments?parent_id=${parent_id}`, {
            data: {
              content: content 
            },
            optionalHeaders: {
              Authorization: `Bearer ${sessionId}`
            }
          })
          .then(() => {
            if (_usernameLogged === username) {
              return dispatch(getPosts(sessionId, _usernameLogged));
            }else {
              return dispatch(getPostsUser(username));
            }
          })
      }
    });
  };
};

const deleteComment = (parent_id, username, comment_id) => {
  return (dispatch, getState) => {
    const { sessionId, userId } = getState().authentication;
    const _usernameLogged = getState().entities.users[userId].username;

    console.log(parent_id)
    console.log(username)
    console.log(comment_id)

    return dispatch({
      type: "DELETE_COMMENT",
      payload: {
        promise: http
          .delete(`/api/users/comments/${comment_id}`, {
            data: {

            },
            optionalHeaders: {
              Authorization: `Bearer ${sessionId}`
            }
          })
          .then(() => {
            if (_usernameLogged === username) {
              return dispatch(getPosts(sessionId, _usernameLogged));
            }else {
              return dispatch(getPostsUser(username));
            }
          })
      }
    });
  }
}



export { getPosts, createPost, deletePost, updatePost, getPostsUser, createComment, deleteComment };
