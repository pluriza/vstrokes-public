import { PENDING, FULFILLED, REJECTED } from 'redux-promise-middleware';
import http from "../libs/http";
import { trendingTags } from "./tags";

const GET_ROOMS = "GET_ROOMS";
export const GET_ROOMS_PENDING = GET_ROOMS + "_" + PENDING;
export const GET_ROOMS_FULFILLED = GET_ROOMS + "_" + FULFILLED;
export const GET_ROOMS_REJECTED = GET_ROOMS + "_" + REJECTED;

export const LOAD_ROOMS = "LOAD_ROOMS";
export const CLEARED_LOAD_ROOMS_TIMEOUT = "CLEARED_LOAD_ROOMS_TIMEOUT";

export const SET_FILTER = "SET_FILTER";
export const RESET_FILTERS = "RESET_FILTERS";

const liveShows = (filters = { }) => {
  let uri = '/api/users/rooms/live';
  const params = [];
  Object.keys(filters).forEach(param => {
    if (filters[param] || filters[param] === 0) {
      params.push(`${param}=${encodeURIComponent(filters[param])}`);
    }
  });
  if (params.length > 0) uri += `?${params.join('&')}`;
  return (dispatch, getState) => {
    const { sessionId } = getState().authentication;
    return dispatch({
      type: GET_ROOMS,
      payload: {
        promise: http.get(uri, {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          }
        }).then(response => (response.data))
      }
    });
  }
};

const setFilter = (key, value) => {
  return async (dispatch) => {
    if (key === 'tag') {
      await dispatch({ type: SET_FILTER, payload: { tag: '' } });
    }
    await dispatch({ type: SET_FILTER, payload: { [key]: value } });
    return dispatch(loadRooms());
  };
};
const resetFilters = () => {
  return async (dispatch, getState) => {
    await dispatch({ type: RESET_FILTERS });
    return dispatch(loadRooms());
  };
};

const loadRooms = () => async (dispatch, getState) => {
  const { timeout, filters } = getState().rooms;
  window.clearTimeout(timeout);
  let delay = 5000;
  try {
    await Promise.all([
      dispatch(trendingTags()),
      dispatch(liveShows(filters)),
    ]);
  } catch (error) {
    delay = 7000;
  }
  return dispatch({
    type: LOAD_ROOMS,
    payload: setTimeout(() => dispatch(loadRooms()), delay),
  });
};

const clearTimeout = () => {
  return (dispatch, getState) => {
    const {timeout} = getState().rooms;
    window.clearTimeout(timeout);
    return dispatch({ type: CLEARED_LOAD_ROOMS_TIMEOUT });
  };
};

export { loadRooms, clearTimeout, setFilter, resetFilters };