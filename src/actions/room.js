import { PENDING, FULFILLED, REJECTED } from 'redux-promise-middleware';
import http from "../libs/http";
import {emitLiveChange, emitLiveEnd} from '../libs/socket';

const CREATE_ROOM = "CREATE_ROOM";
export const CREATE_ROOM_PENDING = CREATE_ROOM + "_" + PENDING;
export const CREATE_ROOM_FULFILLED = CREATE_ROOM + "_" + FULFILLED;
export const CREATE_ROOM_REJECTED = CREATE_ROOM + "_" + REJECTED;

const GET_ROOM = "GET_ROOM";
export const GET_ROOM_PENDING = GET_ROOM + "_" + PENDING;
export const GET_ROOM_FULFILLED = GET_ROOM + "_" + FULFILLED;
export const GET_ROOM_REJECTED = GET_ROOM + "_" + REJECTED;

const DELETE_ROOM = "DELETE_ROOM";
export const DELETE_ROOM_PENDING = DELETE_ROOM + "_" + PENDING;
export const DELETE_ROOM_FULFILLED = DELETE_ROOM + "_" + FULFILLED;
export const DELETE_ROOM_REJECTED = DELETE_ROOM + "_" + REJECTED;

export const RESTORE_ROOM = "RESTORE_ROOM";

const GET_GOAL = "GET_GOAL";
export const GET_GOAL_FULFILLED = GET_GOAL + "_" + FULFILLED;

const NEXT_GOAL = "NEXT_GOAL";
export const NEXT_GOAL_FULFILLED = NEXT_GOAL + "_" + FULFILLED;

const ADD_GOAL = "ADD_GOAL";
export const ADD_GOAL_PENDING = ADD_GOAL + "_" + PENDING;
export const ADD_GOAL_FULFILLED = ADD_GOAL + "_" + FULFILLED;
export const ADD_GOAL_REJECTED = ADD_GOAL + "_" + REJECTED;

const createRoom = data => {
  return (dispatch, getState) => {
    const { sessionId, userId } = getState().authentication;
    const { username } = getState().entities.users[userId];
    return dispatch({
      type: CREATE_ROOM,
      payload: {
        promise: http.post("/api/users/room", {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          },
          data
        }).then(response => {
          dispatch(getRoom(username));
          return ({ ...response.data, ...data });
        })
      }
    });
  }
};

const getRoom = username => {
  return (dispatch, getState) => {
    const { sessionId } = getState().authentication;
    return dispatch({
      type: GET_ROOM,
      payload: {
        promise: http.get(`/api/users/room/${username}`, {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          }
        }).then(response => ({ ...response.data }))
      }
    });
  }
};

const addGoal = (roomId, description, tokens) => {
  return (dispatch, getState) => {
    const { sessionId } = getState().authentication;
    return dispatch({
      type: ADD_GOAL,
      payload: {
        promise: http.post(`/api/rooms/goals/add`, {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          },
          data: {
            roomId,
            description,
            tokens
          }
        }).then(response => ({ ...response.data }))
      }
    });
  }
};

const nextGoal = (roomId, currentGoalId, nextGoalId) => {
  return (dispatch, getState) => {
    const {authentication: {sessionId, userId}, entities: {users}} = getState();
    const {username} = users[userId];
    return dispatch({
      type: NEXT_GOAL,
      payload: {
        promise: http.post(`/api/rooms/goals/next`, {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          },
          data: {
            roomId,
            currentGoalId,
            nextGoalId
          }
        }).then(() => {
          emitLiveChange(username, roomId);
          return dispatch(getGoals(roomId));
        })
      }
    });
  }
};

const getGoals = (roomId) => {
  return (dispatch, getState) => {
    const { sessionId } = getState().authentication;
    let uri = '/api/rooms/goals';
    if (roomId) uri += `?roomId=${roomId}`;
    return dispatch({
      type: GET_GOAL,
      payload: {
        promise: http.get(uri, {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          }
        }).then(response => ({ ...response.data }))
      }
    });
  }
};

const restoreRoomData = () => ({
  type: RESTORE_ROOM
});

const deleteRoom = () => {
  return (dispatch, getState) => {
    const {authentication: {sessionId, userId}, entities: {users}} = getState();
    const {username} = users[userId];
    return dispatch({
      type: DELETE_ROOM,
      payload: {
        promise: http.delete('/api/users/room/', {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          }
        }).then(response => {
          emitLiveEnd(username);
          return { ...response.data };
        })
      }
    });
  }
};

const updateRoomThumbnail = async (sessionId, roomId, blob) => {
  console.log('updateRoomThumbnail', sessionId, roomId, blob);
  const data = new FormData();
  data.append('image', blob);
  try {
    const resultPicture = await http.post(`/api/users/pictures?default_album=room_images_${roomId}`, {
      data,
      optionalHeaders: {
        Authorization: `Bearer ${sessionId}`,
        'Content-Type': 'multipart/form-data',
      },
    });
    const picture = resultPicture.data;
    console.log('updateRoomThumbnail', picture);
    const resultRoom = await http.put('/api/users/room', {
      optionalHeaders: {
        Authorization: `Bearer ${sessionId}`,
      },
      data: {
        thumbnail: picture.uri
      },
    });
    const room = resultRoom.data;
    console.log('updateRoomThumbnail', room);
  } catch (error) {
    console.log('updateRoomThumbnail', error);
  }
};

export { createRoom, getRoom, restoreRoomData, deleteRoom, addGoal, nextGoal, getGoals, updateRoomThumbnail };