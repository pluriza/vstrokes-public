const toggleTermsModal = () => ({ type: 'TOGGLE_TERMS_MODAL' })

const toggleLoginModal = () => ({ type: 'TOGGLE_LOGIN_MODAL' })

const toggleResetPasswordModal = () => ({ type: 'TOGGLE_RESET_PASSWORD_MODAL' })

const toggleUploadPictureModal = () => ({ type: 'TOGGLE_UPLOAD_PICTURE_MODAL' })

const toggleGetStrokesModal = () => ({ type: 'TOGGLE_GET_STROKES_MODAL' })

const toggleStrokeMeModal = () => ({ type: 'TOGGLE_STROKE_ME_MODAL' })

const toggleDescriptionMeModal = () => ({ type: 'TOGGLE_DESCRIPTION_ME_MODAL' })

const toggleRoomCreationModal = () => ({ type: 'TOGGLE_ROOM_CREATION_MODAL' })

const toggleRoomClosingModal = () => ({ type: 'TOGGLE_ROOM_CLOSING_MODAL' })

const toggleAlbumsModal = () => ({ type: 'TOGGLE_ALBUMS_MODAL' })

const togglePayInfoToken = () => ({ type: 'TOGGLE_PAY_INFO_TOKEN_MODAL' })

const toggleDetailAlbumModal = () => ({ type: 'TOGGLE_DETAIL_ALBUM_MODAL' })

const toggleUploadPicturePostModal = () => ({ type: 'TOGGLE_UPLOAD_PICTURE_POST_MODAL' })

const toggleGalleryModal = pictureId => ({ type: 'TOGGLE_GALLERY_MODAL', payload: pictureId });

export {
  toggleTermsModal, toggleLoginModal, toggleResetPasswordModal, toggleUploadPictureModal,
  toggleGetStrokesModal, toggleStrokeMeModal, toggleDescriptionMeModal,
  toggleRoomCreationModal, toggleRoomClosingModal, toggleGalleryModal,
  toggleAlbumsModal, togglePayInfoToken, toggleDetailAlbumModal, toggleUploadPicturePostModal
};
