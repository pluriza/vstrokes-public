import { PENDING, FULFILLED, REJECTED } from 'redux-promise-middleware';
import http from "../libs/http";

const GET_SNAPS = "GET_SNAPS";
export const GET_SNAPS_PENDING = GET_SNAPS + "_" + PENDING;
export const GET_SNAPS_FULFILLED = GET_SNAPS + "_" + FULFILLED;
export const GET_SNAPS_REJECTED = GET_SNAPS + "_" + REJECTED;

export const SET_FILTER = "SET_SNAPS_FILTER";
export const RESET_FILTERS = "RESET_SNAPS_FILTERS";

const snaps = (filters = {}) => {
  let uri = '/api/users/snaps';
  const params = [];
  Object.keys(filters).forEach(param => {
    if (filters[param] || filters[param] === 0) {
      params.push(`${param}=${encodeURIComponent(filters[param])}`);
    }
  });
  if (params.length > 0) uri += `?${params.join('&')}`;
  return (dispatch, getState) => {
    const { sessionId } = getState().authentication;
    return dispatch({
      type: GET_SNAPS,
      payload: {
        promise: http.get(uri, {
          optionalHeaders: {
            Authorization: `Bearer ${sessionId}`
          }
        }).then(response => response.data)
      }
    });
  }
};


const setFilter = (key, value) => {
  return async (dispatch, getState) => {
    await dispatch({ type: SET_FILTER, payload: { [key]: value } });
    const { snaps: { filters } } = getState();
    return dispatch(snaps(filters));
  };
};

const resetFilters = () => {
  return async (dispatch, getState) => {
    await dispatch({ type: RESET_FILTERS });
    const { snaps: { filters } } = getState();
    return dispatch(snaps(filters));
  };
};

export { snaps, setFilter, resetFilters };