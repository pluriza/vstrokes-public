import http from "../libs/http";
import {PENDING, FULFILLED, REJECTED} from 'redux-promise-middleware';
import {toggleLoginModal} from './modals';
import {getGoals} from './room';
import {emitToChatProfile, emitLiveChange} from '../libs/socket';

const SEND_TIPS = "SEND_TIPS";
export const SEND_TIPS_PENDING = SEND_TIPS + "_" + PENDING;
export const SEND_TIPS_FULFILLED = SEND_TIPS + "_" + FULFILLED;
export const SEND_TIPS_REJECTED = SEND_TIPS + "_" + REJECTED;
/*
const BUY_TIPS = "BUY_TIPS";
export const BUY_TIPS_PENDING = BUY_TIPS + "_" + PENDING;
export const BUY_TIPS_FULFILLED = BUY_TIPS + "_" + FULFILLED;
export const BUY_TIPS_REJECTED = BUY_TIPS + "_" + REJECTED;
*/
const GET_WALLET = "GET_WALLET";
export const GET_WALLET_PENDING = GET_WALLET + "_" + PENDING;
export const GET_WALLET_FULFILLED = GET_WALLET + "_" + FULFILLED;
export const GET_WALLET_REJECTED = GET_WALLET + "_" + REJECTED;

const UPDATE_BUY_TOKENS_INFO = 'UPDATE_BUY_TOKENS_INFO';
export const UPDATE_BUY_TOKENS_INFO_PENDING = UPDATE_BUY_TOKENS_INFO + "_" + PENDING;
export const UPDATE_BUY_TOKENS_INFO_FULFILLED = UPDATE_BUY_TOKENS_INFO + "_" + FULFILLED;
export const UPDATE_BUY_TOKENS_INFO_REJECTED = UPDATE_BUY_TOKENS_INFO + "_" + REJECTED;

const GET_PURCHASES = "GET_PURCHASES";
export const GET_PURCHASES_PENDING = GET_PURCHASES + "_" + PENDING;
export const GET_PURCHASES_FULFILLED = GET_PURCHASES + "_" + FULFILLED;
export const GET_PURCHASES_REJECTED = GET_PURCHASES + "_" + REJECTED;

export const CLEAR_PURCHASES = "CLEAR_PURCHASES";

const REQUEST_PAYMENT = "REQUEST_PAYMENT";
export const REQUEST_PAYMENT_PENDING = REQUEST_PAYMENT + "_" + PENDING;
export const REQUEST_PAYMENT_FULFILLED = REQUEST_PAYMENT + "_" + FULFILLED;
export const REQUEST_PAYMENT_REJECTED = REQUEST_PAYMENT + "_" + REJECTED;

const sendTips = (room, user, roomId, goalId, tips) => {
  return (dispatch, getState) => {
    const {sessionId, isLogged} = getState().authentication;
    if (!isLogged) {
      return dispatch(toggleLoginModal());
    }
    return dispatch({
      type: SEND_TIPS,
      payload: {
        promise: http.post(`/api/rooms/tip`, {
          optionalHeaders: {Authorization: `Bearer ${sessionId}`},
          data: {roomId, goalId, tips},
        }).then(response => {
          dispatch(getGoals(roomId));
          emitToChatProfile({
            room,
            content: {
              user: {name: '', image: ''},
              content: `${user} just tipped ${tips} tokens`,
            },
          });
          emitLiveChange(room, roomId);
          return {
            ...response.data
          };
        })
      }
    });
  }
};
/*
const buyTokens = (tokenCount, value, paymentData) => {
  return (dispatch, getState) => {
    const {sessionId} = getState().authentication;
    return dispatch({
      type: BUY_TIPS,
      payload: {
        promise: http.post(`/api/tokens/buy`, {
          optionalHeaders: {Authorization: `Bearer ${sessionId}`},
          data: {tokenCount, value, paymentData}
        }).then(response => ({...response.data})),
      },
    });
  }
};
*/
const getTokens = () => {
  return (dispatch, getState) => {
    const {sessionId} = getState().authentication;
    return dispatch({
      type: GET_WALLET,
      payload: {
        promise: http.get(`/api/wallet`, {
          optionalHeaders: {Authorization: `Bearer ${sessionId}`}
        }).then(response => ({...response.data})),
      },
    });
  }
};

const saveCountStrokes = count => {
  return dispatch => {
    return dispatch({
      type: "SAVE_COUNT_STROKES", payload: {
        count
      }
    });
  };
};

const saveTotalStrokes = total => {
  return dispatch => {
    return dispatch({
      type: "SAVE_TOTAL_STROKES_PRICES", payload: {
        total
      }
    });
  };
};

const saveBuyTokensInfo = (count, total) => {
  return (dispatch, getState) => {
    const {sessionId} = getState().authentication;
    return dispatch({
      type: UPDATE_BUY_TOKENS_INFO,
      payload: {
        promise: http.post(`/api/tokens/generate`, {
          optionalHeaders: {Authorization: `Bearer ${sessionId}`},
          data: {price: Number(total).toFixed(2), count},
        }).then(response => ({
          ...response.data,
          count,
          total,
        })),
      },
    });
  };
};

const getPurchases = () => {
  return (dispatch, getState) => {
    const { sessionId } = getState().authentication;
    return dispatch({
      type: GET_PURCHASES,
      payload: {
        promise: http.get('api/tokens/payments', {
          optionalHeaders: {Authorization: `Bearer ${sessionId}`},
        }).then(response => response.data),
      }
    })
  };
};

const clearPurchases = { type: CLEAR_PURCHASES };

const requestPayment = (tokenCount) => {
  return (dispatch, getState) => {
    const { sessionId } = getState().authentication;
    return dispatch({
      type: REQUEST_PAYMENT,
      payload: {
        promise: http.post('/api/tokens/withdrawal', {
          optionalHeaders: { Authorization: `Bearer ${sessionId}` },
          data: { tokenCount },
        }),
      },
    });
  };
};

export {
  saveCountStrokes, saveTotalStrokes, sendTips, getTokens,
  saveBuyTokensInfo, getPurchases, clearPurchases, requestPayment,
};
