import { prop, assoc } from "ramda";
import { normalize } from "normalizr";
import http from "../libs/http";
import { usersSchema } from "../libs/schemas";
import history from "../libs/history";
import { getPosts } from "./post";
import { showGrowler, hideTimeOutGrowler } from "./growler";

const createUser = userData => {
  return {
    type: "CREATE_USER",
    payload: {
      promise: http
        .post("/api/users", {
          data: userData,
        })
        .then(response => {
          history.push("/confirm");
          return normalize(prop("data")(response), usersSchema);
        })
    }
  };
};

const confirmUser = confirmationToken => {
  return (dispatch, getState) => {
    return dispatch({
      type: "CONFIRM_USER",
      payload: {
        promise: http
          .get(`/api/users/confirm_account?token=${confirmationToken}`, {})
          .then(data => {
            const { username } = data.data;
            const password = localStorage.getItem("vspassword");
            return dispatch(createSession({ username, password }));
          })
          .then(() => {
            localStorage.removeItem("vspassword");
            history.push("/profile");
            dispatch(
              showGrowler(
                "Your account has been confirmed.",
                "growler--success"
              )
            );
            const { growler } = getState();
            dispatch(hideTimeOutGrowler(growler));
          })
      }
    });
  };
};

const createSession = userData => {
  return dispatch => {
    return dispatch({
      type: "CREATE_SESSION",
      payload: {
        promise: http
          .post("/api/users/login", {
            data: userData
          })
          .then(response => {
            const data = assoc('token', response.data.token, normalize(response.data.user, usersSchema));
            const user = data.entities.users[data.result] ;
            dispatch(getPosts(data.token, user.username));
            return data;
          })
      }
    });
  };
};

const destroySession = () => {
  return { type: "DESTROY_SESSION" };
};

const clearLoginFail = { type: "CLEAR_LOGIN_FAIL" };

export { createUser, confirmUser, createSession, destroySession, clearLoginFail };
