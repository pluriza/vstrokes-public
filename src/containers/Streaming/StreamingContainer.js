import { connect } from "react-redux";
import Streaming from "../../components/Streaming/Streaming";
import { getRoom, getGoals } from "../../actions/room";
import { sendTips } from "../../actions/token";

const mapStateToProps = state => {
  const { authentication: { userId }, entities: { users }, chat, room } = state;
  const user = users[userId];
  const username = user ? user.username : "";
  return { chat, room, username };
};


// const mapStateToProps = state => {
//   const { authentication, entities, user: { profile, profileImage, coverImage, description }, post: {postsUser} } = state;
//   const { users } = entities;
//   const { isLogged, userId } = authentication;
//   const username = users[userId] !== undefined ? users[userId].username : "";
//   const {isStrokeMeModalOpen} = state.modals;
//   const _userId = state.user.userId;
//   return { isLogged, username, profile, isStrokeMeModalOpen, profileImage, coverImage, description, _userId, postsUser };
// };


const mapDispatchToProps = dispatch => {
  return {
    getRoom: username => dispatch(getRoom(username)),
    getGoals: roomId => dispatch(getGoals(roomId)),
    sendTips: (room, user) => (roomId, goalId, tips) => dispatch(sendTips(room, user, roomId, goalId, tips)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Streaming);
