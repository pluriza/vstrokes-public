import {connect} from "react-redux";
import PayInfoTokens from "../../components/Tokens/PayInfoTokens";
import {togglePayInfoToken, toggleGetStrokesModal} from "../../actions/modals";
// import {buyTokens} from "../../actions/token";

const mapStateToProps = ({modals, token}) => {
  const {isPayInfoToken} = modals;
  const {count, total, loading, digest, token: vsToken} = token.buyInfo;
  return {count, total, loading, digest, vsToken, isPayInfoToken};
};

const mapDispatchToProps = dispatch => {
  return {
    togglePayInfoToken: () => dispatch(togglePayInfoToken()),
    toggleGetStrokesModal: () => dispatch(toggleGetStrokesModal()),
    // buyTokens: (tokenCount, value, paymentData) => dispatch(buyTokens(tokenCount, value, paymentData))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PayInfoTokens);