import {connect} from "react-redux";
import Tokens from "../../components/Tokens/Tokens";
import { getTokens, getPurchases, clearPurchases, requestPayment } from "../../actions/token";

const mapStateToProps = state => {
  const {
    count, total, purchases, withdrawable, withdrawalPending, withdrawalMessage,
  } = state.token;
  return {
    count, total, purchases, withdrawable, withdrawalPending, withdrawalMessage,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getTokens: () => dispatch(getTokens()),
    getPurchases: () => dispatch(getPurchases()),
    clearPurchases: () => dispatch(clearPurchases),
    requestPayment: tokens => dispatch(requestPayment(tokens)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tokens);
