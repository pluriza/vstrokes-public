import { connect } from "react-redux";
import GetTokens from "../../components/Tokens/GetTokens";
import { toggleGetStrokesModal, togglePayInfoToken } from "../../actions/modals";
import { saveBuyTokensInfo } from "../../actions/token";

const mapStateToProps = state => {
  const { isGetStrokesModalOpen, isPayInfoToken } = state.modals;
  return { isGetStrokesModalOpen, isPayInfoToken };
};

const mapDispatchToProps = dispatch => {
  return {
    togglePayInfoToken: () => dispatch(togglePayInfoToken()),
    toggleGetStrokesModal: () => dispatch(toggleGetStrokesModal()),
    saveBuyTokensInfo: (count, total) => dispatch(saveBuyTokensInfo(count, total)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GetTokens);