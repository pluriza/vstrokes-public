import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import Header from "../../components/Header/Header";
import { toggleGetStrokesModal } from "../../actions/modals";
import { setFilter, resetFilters } from "../../actions/rooms";

const mapStateToProps = state => {
  const { isLogged } = state.authentication;
  return {
    isLogged,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleGetStrokesModal: () => dispatch(toggleGetStrokesModal()),
    setFilter: (key, value) => dispatch(setFilter(key, value)),
    resetFilters: () => dispatch(resetFilters()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
