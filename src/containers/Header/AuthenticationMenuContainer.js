import { connect } from "react-redux";
import AuthenticationMenu from "../../components/Header/AuthenticationMenu";
import { toggleLoginModal } from "../../actions/modals";

const mapDispatchToProps = dispatch => {
  return {
    toggleLoginModal: () => dispatch(toggleLoginModal())
  };
};

export default connect(null, mapDispatchToProps)(AuthenticationMenu);
