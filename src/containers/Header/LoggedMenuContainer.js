import { connect } from "react-redux";
import LoggedMenu from "../../components/Header/LoggedMenu";
import profile from "../../assets/images/woman_profile_icon.png";

const mapStateToProps = state => {
  const { entities: { users }, authentication: { userId } } = state;
  const user = users[userId];
  const { username = "", profileImage } = user;
  const userImage = profileImage || profile;
  return { username, profileImage: userImage };
};

export default connect(mapStateToProps)(LoggedMenu);
