import { connect } from "react-redux";
import LoggedMenuOptions from "../../components/Header/LoggedMenuOptions";
import { destroySession } from "../../actions/authentication";
import history from "../../libs/history";

const mapDispatchToProps = dispatch => {
  return {
    destroySession: () => {
      dispatch(destroySession());
      history.push("/");
    }
  };
};

export default connect(null, mapDispatchToProps)(LoggedMenuOptions);
