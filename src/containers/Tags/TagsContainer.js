import { connect } from 'react-redux';
import { tags } from '../../actions/tags';
import Tags from '../../components/Tags/Tags';

const mapStateToProps = ({ tags: { all } }) => ({
  tags: all
});

const mapDispatchToProps = dispatch => ({
  getTags: search => dispatch(tags(search))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tags);