import { connect } from 'react-redux';
import { snaps, setFilter, resetFilters } from '../../actions/snaps';
import Snaps from '../../components/Snaps/Snaps';

const mapStateToProps = ({ snaps }) => snaps;

const mapDispatchToProps = dispatch => ({
  getSnaps: filters => dispatch(snaps(filters)),
  updatePage: page => dispatch(setFilter('page', page)),
  setFilter: (key, value) => dispatch(setFilter(key, value)),
  resetFilters: () => dispatch(resetFilters()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Snaps);