import {connect} from "react-redux";
import {createUser} from "../../actions/authentication";
import Form from "../../components/SignUp/Form";
import { toggleLoginModal } from "../../actions/modals";

const mapStateToProps = ({authentication: { loading, error, errorMessage }}) => ({
  loading, error, errorMessage
});

const mapDispatchToProps = dispatch => {
  return {
    createUser: payload => dispatch(createUser(payload)),
    toggleLoginModal: () => dispatch(toggleLoginModal())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
