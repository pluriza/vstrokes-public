import { connect } from "react-redux";
import StrokeMe from "../../../components/Profile/StrokeMe/StrokeMe";
import { toggleStrokeMeModal, toggleGetStrokesModal } from "../../../actions/modals";
import { saveCountStrokes } from "../../../actions/token";

const mapStateToProps = state => {
  const { isStrokeMeModalOpen, isGetStrokesModalOpen } = state.modals;
  const { count, total } = state.token;
  return { isStrokeMeModalOpen, count, total, isGetStrokesModalOpen };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleStrokeMeModal: () => dispatch(toggleStrokeMeModal()),
    toggleGetStrokesModal: () => dispatch(toggleGetStrokesModal()),
    saveCountStrokes: (count) => dispatch(saveCountStrokes(count)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StrokeMe);