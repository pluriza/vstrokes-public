import { connect } from "react-redux";
import { updatePost } from "../../../actions/post";
import EditForm from "../../../components/Profile/Post/EditForm";

const mapDispatchToProps = dispatch => {
  return {
    updatePost: (id, content) => dispatch(updatePost(id, content))
  };
};

export default connect(null, mapDispatchToProps)(EditForm);
