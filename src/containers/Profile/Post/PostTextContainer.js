import { connect } from "react-redux";
import { deletePost } from "../../../actions/post";
import PostText from "../../../components/Profile/Post/PostText";

const mapDispatchToProps = dispatch => {
  return {
    deletePost: payload => dispatch(deletePost(payload))
  };
};

export default connect(null, mapDispatchToProps)(PostText);
