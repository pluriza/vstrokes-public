import { connect } from "react-redux";
import { createPost } from "../../../actions/post";
import Form from "../../../components/Profile/Post/Form";
import { toggleUploadPicturePostModal } from "../../../actions/modals";

const mapDispatchToProps = dispatch => {
  return {
    createPost: payload => dispatch(createPost(payload)),
    toggleUploadPicturePostModal: () => dispatch(toggleUploadPicturePostModal()),
  };
};

export default connect(null, mapDispatchToProps)(Form);
