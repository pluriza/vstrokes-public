import { connect } from "react-redux";
import { confirmUser } from "../../actions/authentication";
import {
  getUserInfo,
  getUserProfile,
  postDescriptionMe,
  updateProfile,
  getUserImageProfile,
  getUserImageCover,
  getUserOtherImageProfile,
  getUserOtherImageCover,
  followUser,
  unfollowUser,
} from "../../actions/user";
import { getRoom, restoreRoomData } from "../../actions/room";
import { toggleStrokeMeModal, toggleGalleryModal } from "../../actions/modals";
import Profile from "../../components/Profile/Profile";


import { getPostsUser } from "../../actions/post";
import { getPicturesAll } from "../../actions/album";

const mapStateToProps = state => {
  const {
    authentication: { isLogged, userId },
    entities: { users, loading },
    user: { userId: _userId, profile, profileImage, profileImageLoading, coverImage, coverImageLoading, description },
    post: { postsUser },
    albums: { picturesAll },
    modals: { isStrokeMeModalOpen },
    chat
  } = state;
  const currentUser = users[userId];
  const username = currentUser ? currentUser.username : "";
  return {
    isLogged, username, profile, isStrokeMeModalOpen, description, _userId,
    profileImage, profileImageLoading, coverImage, coverImageLoading,
    postsUser, picturesAll, chat, loadingEntities: loading,
  };
};

const mapDispatchToProps = dispatch => ({
  confirmUser: confirmationtoken => dispatch(confirmUser(confirmationtoken)),
  getUserInfo: username => dispatch(getUserInfo(username)),
  getUserProfile: () => dispatch(getUserProfile()),
  toggleStrokeMeModal: () => dispatch(toggleStrokeMeModal()),
  getPostsUser: (username) => dispatch(getPostsUser(username)),
  postDescriptionMe: (description) => dispatch(postDescriptionMe(description)),
  updateProfile: payload => dispatch(updateProfile(payload)),
  getRoom: username => dispatch(getRoom(username)),
  restoreRoomData: () => dispatch(restoreRoomData()),
  getUserImageProfile: () => dispatch(getUserImageProfile()),
  getUserImageCover: () => dispatch(getUserImageCover()),
  getUserOtherImageProfile: (username) => dispatch(getUserOtherImageProfile(username)),
  getUserOtherImageCover: (username) => dispatch(getUserOtherImageCover(username)),
  getPicturesAll: (isMe) => dispatch(getPicturesAll(isMe)),
  toggleGalleryModal: pictureId => dispatch(toggleGalleryModal(pictureId)),
  onFollowClick: (FollowingId, history) => dispatch(followUser(FollowingId, history)),
  onUnfollowClick: (FollowingId) => dispatch(unfollowUser(FollowingId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
