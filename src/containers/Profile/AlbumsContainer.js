import { connect } from "react-redux";
import { toggleAlbumsModal, toggleDetailAlbumModal } from "../../actions/modals";
import Albums from "../../components/Profile/Albums";
import { createAlbum, getAlbums, createPictureAlbum, getAlbum } from '../../actions/album';


const mapStateToProps = state => {
  const { isAlbumsModalOpen, isDetailAlbumModalOpen } = state.modals;
  const {albums, created, albumActive, loading } = state.albums;
  return { isAlbumsModalOpen, albums, created, loading, isDetailAlbumModalOpen, albumActive };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleDetailAlbumModal: () => dispatch(toggleDetailAlbumModal()),
    toggleAlbumsModal: () => dispatch(toggleAlbumsModal()),
    createAlbum: (album, pictures) => dispatch(createAlbum(album, pictures)),
    getAlbums: (isMe) => dispatch(getAlbums(isMe)),
    getAlbum: (id, isMe) => dispatch(getAlbum(id, isMe)),
    createPictureAlbum: (picture, album_id) => dispatch(createPictureAlbum(picture, album_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Albums);
