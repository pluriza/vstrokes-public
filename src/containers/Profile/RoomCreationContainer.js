import { connect } from "react-redux";
import RoomCreation from "../../components/Profile/RoomCreation/RoomCreation";
import { toggleRoomCreationModal } from "../../actions/modals";
import { createRoom } from "../../actions/room";

const mapStateToProps = state => {
  const { modals, room } = state;
  const { isCreateRoomModalOpen } = modals;
  const { loading, active, broadcastKey } = room;
  return { isCreateRoomModalOpen, isCreatingRoom: loading, activeRoom: active, broadcastKey };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleRoomCreationModal: () => dispatch(toggleRoomCreationModal()),
    createRoom: data => dispatch(createRoom(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomCreation);