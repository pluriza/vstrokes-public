import { connect } from "react-redux";
import { createAlbum } from "../../../actions/album";
import { createPicture } from "../../../actions/picture";
import { toggleUploadPictureModal } from "../../../actions/modals";
import UploadPicture from "../../../components/Profile/UploadPicture/UploadPicture";

import { postUserImageProfile, postUserImageCover } from "../../../actions/user";

const mapStateToProps = state => {
  const { isUploadPictureModalOpen } = state.modals;
  const { profileImage } = state.user;
  return { isUploadPictureModalOpen, profileImage };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleUploadPictureModal: () => dispatch(toggleUploadPictureModal()),
    createAlbum: () => dispatch(createAlbum()),
    createPicture: pictureData => dispatch(createPicture(pictureData)),
    postUserImageProfile: (image) => dispatch(postUserImageProfile(image)),
    postUserImageCover: (image) => dispatch(postUserImageCover(image)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UploadPicture);
