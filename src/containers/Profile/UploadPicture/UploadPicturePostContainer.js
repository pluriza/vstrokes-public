import { connect } from "react-redux";
import { toggleUploadPicturePostModal } from "../../../actions/modals";
import UploadPicturePost from "../../../components/Profile/UploadPicture/UploadPicturePost";

const mapStateToProps = state => {
  const { isUploadPicturePostModalOpen } = state.modals;
  const { profileImage } = state.user;
  return { isUploadPicturePostModalOpen, profileImage };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleUploadPicturePostModal: () => dispatch(toggleUploadPicturePostModal()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UploadPicturePost);
