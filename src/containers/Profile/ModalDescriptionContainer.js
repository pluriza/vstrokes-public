import { connect } from "react-redux";
import ModalDescription from "../../components/Profile/DescribeYou/ModalDescription";
import { toggleDescriptionMeModal } from "../../actions/modals";
import { postDescriptionMe, updateProfile } from "../../actions/user";

const mapStateToProps = state => {
  const { isDescriptionMeModalOpen } = state.modals;
  const { description, profile } = state.user;
  return { isDescriptionMeModalOpen, description, profile };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleDescriptionMeModal: () => dispatch(toggleDescriptionMeModal()),
    postDescriptionMe: (description) => dispatch(postDescriptionMe(description)),
    updateProfile: payload => dispatch(updateProfile(payload))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalDescription);