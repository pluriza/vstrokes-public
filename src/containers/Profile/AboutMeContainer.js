import { connect } from "react-redux";
import AboutMe from "../../components/Profile/AboutMe";

const mapStateToProps = state => {
  const { user: { profile } } = state;
  return { profile };
};

export default connect(mapStateToProps)(AboutMe);
