import { connect } from "react-redux";
import { updateProfile } from "../../../actions/user";
import EditContactForm from "../../../components/Profile/EditProfile/EditContactForm";

const mapDispatchToProps = dispatch => {
  return {
    updateProfile: payload => dispatch(updateProfile(payload))
  };
};

export default connect(null, mapDispatchToProps)(EditContactForm);
