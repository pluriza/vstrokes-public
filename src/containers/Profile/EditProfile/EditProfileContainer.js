import { connect } from "react-redux";
import { getUserProfile } from "../../../actions/user";
import EditProfile from "../../../components/Profile/EditProfile/EditProfile";

import { updateProfile } from "../../../actions/user";

const mapStateToProps = state => {
  const {
    authentication: { userId },
    entities: { users },
    user: { profile, profileImage, coverImage, description }
  } = state;
  const { username, email } = users[userId];
  return { username, profile, profileImage, coverImage, description, email };
};

const mapDispatchToProps = dispatch => ({
  getUserProfile: () => dispatch(getUserProfile()),
  updateProfile: payload => dispatch(updateProfile(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
