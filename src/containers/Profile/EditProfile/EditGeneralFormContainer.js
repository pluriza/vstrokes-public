import { connect } from "react-redux";
import { updateProfile } from "../../../actions/user";
import EditGeneralForm from "../../../components/Profile/EditProfile/EditGeneralForm";

const mapDispatchToProps = dispatch => {
  return {
    updateProfile: payload => dispatch(updateProfile(payload))
  };
};

export default connect(null, mapDispatchToProps)(EditGeneralForm);
