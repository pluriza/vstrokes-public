import { connect } from "react-redux";
import { updateProfile } from "../../../actions/user";
import EditDetailsForm from "../../../components/Profile/EditProfile/EditDetailsForm";

const mapDispatchToProps = dispatch => {
  return {
    updateProfile: payload => dispatch(updateProfile(payload))
  };
};

export default connect(null, mapDispatchToProps)(EditDetailsForm);
