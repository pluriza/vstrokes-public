import {connect} from "react-redux";
import {values, sort, compose} from "ramda";
import Feed from "../../components/Profile/Feed";
import {addGoal, nextGoal, getGoals, deleteRoom, updateRoomThumbnail} from "../../actions/room";
import {sendTips} from "../../actions/token";

const mapStateToProps = (state, ownProps) => {
  const {isOtherProfile} = ownProps;

  const {
    entities: {
      posts,
      users
    },
    authentication: {
      userId, sessionId
    },
    user: {
      username: otherUserUsername
    },
    post: {
      postsUser
    },
    room
  } = state;

  //is other  const _userId = state.user.userId;

  const isStreamOnline = room.active || false;

  const sortedPostsArray = compose(sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt)), values)(posts);

  const scroll = postsUser.length > 0 || sortedPostsArray.length > 0;
  const user = users[userId];
  const preUsername = user ? user.username : "";
  let ageVerified = false;
  if (!isOtherProfile) {
    ageVerified = user.ageVerified;
  }
  const username = isOtherProfile
    ? otherUserUsername
    : preUsername;

  return {
    username,
    sortedPostsArray,
    isStreamOnline,
    postsUser,
    room,
    scroll,
    broadcastKey: !isOtherProfile && user.broadcastKey,
    currentUsername: preUsername,
    sessionId,
    ageVerified,
  };
};

const mapDispatchToProps = dispatch => ({
  addGoal: (roomId, description, tokens) => dispatch(addGoal(roomId, description, tokens)),
  nextGoal: (roomId, currentGoalId, nextGoalId) => dispatch(nextGoal(roomId, currentGoalId, nextGoalId)),
  getGoals: roomId => dispatch(getGoals(roomId)),
  sendTips: (room, user) => (roomId, goalId, tips) => dispatch(sendTips(room, user, roomId, goalId, tips)),
  deleteRoom: () => dispatch(deleteRoom()),
  updateRoomThumbnail: sessionId => roomId => blob => updateRoomThumbnail(sessionId, roomId, blob),
});

export default connect(mapStateToProps, mapDispatchToProps)(Feed);
