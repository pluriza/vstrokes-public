import { connect } from "react-redux";
import RoomCreation from "../../components/Profile/RoomClosing";
import { toggleRoomClosingModal } from "../../actions/modals";
import { deleteRoom } from "../../actions/room";

const mapStateToProps = state => {
  const { modals } = state;
  const { isClosingRoomModalOpen } = modals;
  return { isClosingRoomModalOpen };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleRoomClosingModal: () => dispatch(toggleRoomClosingModal()),
    deleteRoom: () => {
      dispatch(deleteRoom());
      dispatch(toggleRoomClosingModal());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomCreation);