import { connect } from "react-redux";
import Tools from "../../components/Profile/Tools/Tools";
import { toggleRoomCreationModal, toggleRoomClosingModal } from "../../actions/modals";

const mapDispatchToProps = dispatch => {
  return {
    openRoomCreationModal: () => dispatch(toggleRoomCreationModal()),
    toggleRoomClosingModal: () => dispatch(toggleRoomClosingModal()),
  };
};

export default connect(null, mapDispatchToProps)(Tools);
