import { connect } from "react-redux";
import HeaderP from "../../components/Profile/HeaderP";
import { toggleUploadPictureModal, toggleDescriptionMeModal  } from "../../actions/modals";

const mapDispatchToProps = dispatch => {
  return {
    toggleUploadPictureModal: () => dispatch(toggleUploadPictureModal()),
    toggleDescriptionMeModal: () => dispatch(toggleDescriptionMeModal())
  };
};

export default connect(null, mapDispatchToProps)(HeaderP);
