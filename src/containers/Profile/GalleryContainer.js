import { connect } from "react-redux";
import Gallery from "../../components/Profile/Gallery/Gallery";
import { toggleGalleryModal } from "../../actions/modals";

const mapStateToProps = state => {
  const { isGalleryModalOpen, pictureId } = state.modals;
  return { isGalleryModalOpen, pictureId };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleGalleryModal: (pictureId) => dispatch(toggleGalleryModal(pictureId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);