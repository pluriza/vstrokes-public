import { connect } from "react-redux";
import Chat from "../components/Chat/Chat";

const mapStateToProps = state => {
  const { authentication, entities, user: { profile } } = state;
  const { users } = entities;
  const { isLogged, userId } = authentication;
  const username = users[userId] !== undefined ? users[userId].username : "";
  return { isLogged, username, profile };
};

export default connect(mapStateToProps)(Chat);
