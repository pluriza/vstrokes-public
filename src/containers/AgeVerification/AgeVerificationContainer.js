import { connect } from "react-redux";
import { load, send } from "../../actions/verification";
import AgeVerification from "../../components/AgeVerification/AgeVerification";

const mapStateToProps = ({ verification }) => ({ ...verification });

const mapDispatchToProps = dispatch => {
  return {
    loadVerification: () => dispatch(load()),
    sendVerification: people => dispatch(send(people)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AgeVerification);
