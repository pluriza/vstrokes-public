import { connect } from "react-redux";
import SetNewPassword from "../../components/ResetPassword/SetNewPassword";
import { setNewPassword } from "../../actions/user";

const mapDispatchToProps = dispatch => {
  return {
    setNewPassword: passwordData => dispatch(setNewPassword(passwordData))
  };
};

export default connect(null, mapDispatchToProps)(SetNewPassword);
