import { connect } from "react-redux";
import ResetPassword from "../../components/ResetPassword/ResetPassword";
import { toggleResetPasswordModal } from "../../actions/modals";

const mapStateToProps = state => {
  const { isResetPasswordModalOpen } = state.modals;
  return { isResetPasswordModalOpen };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleResetPasswordModal: () => dispatch(toggleResetPasswordModal())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
