import { connect } from "react-redux";
import { toggleResetPasswordModal } from "../../actions/modals";
import { requestResetPassword } from "../../actions/user";
import Form from "../../components/ResetPassword/Form";

const mapDispatchToProps = dispatch => {
  return {
    requestResetPassword: payload => dispatch(requestResetPassword(payload)),
    toggleResetPasswordModal: () => dispatch(toggleResetPasswordModal())
  };
};

export default connect(null, mapDispatchToProps)(Form);
