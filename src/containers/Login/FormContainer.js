import { connect } from "react-redux";
import { createSession } from "../../actions/authentication";
import { toggleLoginModal } from "../../actions/modals";
import Form from "../../components/Login/Form";

const mapStateToProps = state => {
  const { authentication: { loggingFail, loginErrorMessage } } = state;
  return { loggingFail, loginErrorMessage };
};
const mapDispatchToProps = dispatch => {
  return {
    createSession: payload => dispatch(createSession(payload)),
    toggleLoginModal: () => dispatch(toggleLoginModal())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
