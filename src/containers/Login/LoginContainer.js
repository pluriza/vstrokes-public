import { connect } from "react-redux";
import Login from "../../components/Login/Login";
import { clearLoginFail } from "../../actions/authentication";
import {
  toggleLoginModal,
  toggleResetPasswordModal
} from "../../actions/modals";

const mapStateToProps = state => {
  const { isLoginModalOpen } = state.modals;
  return { isLoginModalOpen };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleLoginModal: () => {
      dispatch(toggleLoginModal());
      dispatch(clearLoginFail);
    },
    toggleResetPasswordModal: () => dispatch(toggleResetPasswordModal())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
