import { connect } from "react-redux";
import Chat2 from "../components/Chat/Chat2";
import { getMessages, clearMessages } from '../actions/chat';
import img1 from '../assets/images/woman_profile_icon.png';

const mapStateToProps = state => {
  const {
    authentication: { isLogged, userId },
    entities: { users },
    user: { profile },
    room: { active }
  } = state;
  const user = users[userId] || {};
  const { username = "", profileImage } = user;
  const userImage = profileImage || img1;
  const isStreamOnline = active || false;
  return { isLogged, username, userImage, profile, userId, isStreamOnline };
};

const mapDispatchToProps = dispatch => ({
  getMessages: (payload) => dispatch(getMessages(payload)),
  clearMessages: () => dispatch(clearMessages()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat2);
