import { connect } from "react-redux";
import { hideGrowler, showGrowler } from "../../actions/growler";
import Growler from "../../components/Growler/Growler";

const mapStateToProps = state => {
  const { growler } = state;
  return { growler };
};

const mapDispatchToProps = dispatch => {
  return {
    hideGrowler: () => dispatch(hideGrowler()),
    showGrowler: (text, type) => dispatch(showGrowler(text, type))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Growler);
