import { connect } from "react-redux";
import Home from "../components/Home";
import { loadRooms, clearTimeout, setFilter, resetFilters } from "../actions/rooms";
import { tags } from "../actions/tags";

const mapStateToProps = ({ rooms, tags }) => ({
  streamsLoading: rooms.loading,
  streams: rooms.data,
  streamsCount: rooms.total,
  streamsLimit: rooms.limit,
  filters: rooms.filters,
  trendingTags: tags.trending,
  allTags: tags.all
});

const mapDispatchToProps = dispatch => ({
  getLiveShows: () => dispatch(loadRooms()),
  clearTimeout: () => dispatch(clearTimeout()),
  setFilter: (key, value) => dispatch(setFilter(key, value)),
  resetFilters: () => dispatch(resetFilters()),
  getTags: search => dispatch(tags(search)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
