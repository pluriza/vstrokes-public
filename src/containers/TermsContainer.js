import { connect } from 'react-redux';
import Terms from '../components/InicialTerms';
import { toggleTermsModal } from '../actions/modals';

const mapStateToProps = state => {
  const { isTermsModalOpen } = state.modals;
  return { isTermsModalOpen };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleTermsModal: () => {
      dispatch(toggleTermsModal());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Terms);
