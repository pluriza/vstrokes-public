# VStrokes User Interface

### Basic setup

To get base application up and running, make sure you have a recent version of [Node.js](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/) installed locally, then execute following commands in your terminal:

```bash
yarn
yarn start
```

That's it! Now you should be able to run the development server locally on port 3000.

To build a production version of the application, you have to run `yarn build` and then server the output with a http server.

## Code style

Follow [Airbnb style guide](https://github.com/airbnb/javascript). [ESLint](http://eslint.org/) together with [Airbnb base config](https://www.npmjs.com/package/eslint-config-airbnb-base) is set-up to lint your code.

## More Information

This project was created based using the boilerplate:
[create-react-app](https://github.com/facebookincubator/create-react-app)

## License

Copyright (c) 2017 [vstrokes.com](http://vstrokes.com)
